/*
MySQL Data Transfer
Source Host: localhost
Source Database: database
Target Host: localhost
Target Database: database
Date: 9/19/2013 9:29:35 AM
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for server_message
-- ----------------------------
DROP TABLE IF EXISTS `server_message`;
CREATE TABLE `server_message` (
  `server_message` varchar(230) NOT NULL DEFAULT 'TEST MESSAGE',
  `id` int(1) unsigned NOT NULL DEFAULT '1',
  `lang_name` varchar(200) NOT NULL DEFAULT 'noname',
  PRIMARY KEY (`server_message`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records
-- ----------------------------
INSERT INTO `server_message` VALUES ('テストメッセージ', '1', 'Japanese');
INSERT INTO `server_message` VALUES ('TEST MESSAGE', '2', 'English');
INSERT INTO `server_message` VALUES ('TEST MESSAGE', '3', 'Englsih Japanese');
INSERT INTO `server_message` VALUES ('TEST MESSAGE', '4', 'German');
INSERT INTO `server_message` VALUES ('TEST MESSAGE', '5', 'German Japanese');
INSERT INTO `server_message` VALUES ('TEST MESSAGE', '6', 'English German');
INSERT INTO `server_message` VALUES ('TEST MESSAGE', '8', 'French ');
INSERT INTO `server_message` VALUES ('TEST MESSAGE', '9', 'French Japanese ');
INSERT INTO `server_message` VALUES ('TEST MESSAGE', '10', 'English French ');
INSERT INTO `server_message` VALUES ('TEST MESSAGE', '11', 'Englsih French Japanese');
INSERT INTO `server_message` VALUES ('TEST MESSAGE', '12', 'French Greman');
INSERT INTO `server_message` VALUES ('TEST MESSAGE', '13', 'French German Japanese ');
INSERT INTO `server_message` VALUES ('TEST MESSAGE', '14', 'Englsih French German');
INSERT INTO `server_message` VALUES ('TEST MESSAGE', '15', 'Englsih French German Japanese');
INSERT INTO `server_message` VALUES ('TEST MESSAGE', '16', 'Other');
INSERT INTO `server_message` VALUES ('テストメッセージ', '17', 'Japanese Other');
INSERT INTO `server_message` VALUES ('TEST MESSAGE', '18', 'Englsih Other');
INSERT INTO `server_message` VALUES ('TEST MESSAGE', '20', 'German Other');
INSERT INTO `server_message` VALUES ('TEST MESSAGE', '21', 'German Japanese Other');
INSERT INTO `server_message` VALUES ('TEST MESSAGE', '23', 'English German Japanese Other');
INSERT INTO `server_message` VALUES ('TEST MESSAGE', '24', 'French Other');
INSERT INTO `server_message` VALUES ('TEST MESSAGE', '26', 'Englsih French Other');
INSERT INTO `server_message` VALUES ('TEST MESSAGE', '27', 'English French Japanese Other');
INSERT INTO `server_message` VALUES ('TEST MESSAGE', '28', 'French German Other');
INSERT INTO `server_message` VALUES ('TEST MESSAGE', '29', 'French German Japanese Other');
INSERT INTO `server_message` VALUES ('TEST MESSAGE', '30', 'English French German Other');
INSERT INTO `server_message` VALUES ('TEST MESSAGE', '31', 'English French German Japanese Other');
