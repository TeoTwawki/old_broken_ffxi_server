
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


DROP TABLE IF EXISTS `item_latents`;
CREATE TABLE IF NOT EXISTS `item_latents` (
  `itemId` smallint(5) unsigned NOT NULL,
  `modId` smallint(5) unsigned NOT NULL,
  `value` smallint(5) NOT NULL DEFAULT '0',
  `latentId` smallint(5) NOT NULL,
  `latentParam` smallint(5) NOT NULL,
  PRIMARY KEY (`itemId`,`modId`, `value`, `latentId`, `latentParam`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=13 PACK_KEYS=1;




-- INSERT INTO `item_latents` VALUES (itemID, modId, modValue, latentId, latentParam);
-- Uggalepih Pendant 
INSERT INTO `item_latents` VALUES (13145, 28, 8, 4, 51);
-- Carapace Gauntlets
INSERT INTO `item_latents` VALUES (14008, 23, 8, 0, 75);
INSERT INTO `item_latents` VALUES (14008, 1, 16, 0, 75);
-- Carapace Gauntlets +1
INSERT INTO `item_latents` VALUES (14009, 23, 9, 0, 75);
INSERT INTO `item_latents` VALUES (14009, 1, 17, 0, 75);
INSERT INTO `item_latents` VALUES (15209, 8, 3, 46, 75);
INSERT INTO `item_latents` VALUES (15345, 384, 3, 46, 75);
INSERT INTO `item_latents` VALUES (15406, 31, 4, 46, 75);
-- Berserker's Torque
INSERT INTO `item_latents` VALUES (15530, 368, 10, 10, 50);
-- Bedivere's Hose
INSERT INTO `item_latents` VALUES (16355, 25, 25, 0, 25);
INSERT INTO `item_latents` VALUES (16355, 23, 25, 0, 25);
INSERT INTO `item_latents` VALUES (16426, 25, 2, 0, 92);
INSERT INTO `item_latents` VALUES (16426, 25, 2, 0, 82);
INSERT INTO `item_latents` VALUES (16426, 25, 2, 0, 73);
INSERT INTO `item_latents` VALUES (16426, 25, 2, 0, 64);
INSERT INTO `item_latents` VALUES (16426, 25, 2, 0, 55);
INSERT INTO `item_latents` VALUES (16426, 25, 2, 0, 46);
INSERT INTO `item_latents` VALUES (16426, 25, 2, 0, 37);
INSERT INTO `item_latents` VALUES (16426, 25, 2, 0, 28);
INSERT INTO `item_latents` VALUES (16426, 25, 2, 0, 19);
INSERT INTO `item_latents` VALUES (16426, 25, 2, 0, 10);
INSERT INTO `item_latents` VALUES (16426, 25, 2, 0, 1);
INSERT INTO `item_latents` VALUES (17983, 366, 1, 0, 92);
INSERT INTO `item_latents` VALUES (17983, 366, 1, 0, 82);
INSERT INTO `item_latents` VALUES (17983, 366, 1, 0, 73);
INSERT INTO `item_latents` VALUES (17983, 366, 1, 0, 64);
INSERT INTO `item_latents` VALUES (17983, 366, 1, 0, 55);
INSERT INTO `item_latents` VALUES (17983, 366, 1, 0, 46);
INSERT INTO `item_latents` VALUES (17983, 366, 1, 0, 37);
INSERT INTO `item_latents` VALUES (17983, 366, 1, 0, 28);
INSERT INTO `item_latents` VALUES (17983, 366, 1, 0, 19);
INSERT INTO `item_latents` VALUES (17983, 366, 1, 0, 10);
INSERT INTO `item_latents` VALUES (17983, 366, 1, 0, 1);
-- Amakura
INSERT INTO `item_latents` VALUES (18445, 73, 10, 0, 50);
INSERT INTO `item_latents` VALUES (16911, 366, 1, 0, 92);
INSERT INTO `item_latents` VALUES (16911, 366, 1, 0, 82);
INSERT INTO `item_latents` VALUES (16911, 366, 1, 0, 73);
INSERT INTO `item_latents` VALUES (16911, 366, 1, 0, 64);
INSERT INTO `item_latents` VALUES (16911, 366, 1, 0, 55);
INSERT INTO `item_latents` VALUES (16911, 366, 1, 0, 46);
INSERT INTO `item_latents` VALUES (16911, 366, 1, 0, 37);
INSERT INTO `item_latents` VALUES (16911, 366, 1, 0, 28);
INSERT INTO `item_latents` VALUES (16911, 366, 1, 0, 19);
INSERT INTO `item_latents` VALUES (16911, 366, 1, 0, 10);
INSERT INTO `item_latents` VALUES (16911, 366, 1, 0, 1);
-- Carapace Helm
INSERT INTO `item_latents` VALUES (13878, 23, 10, 0, 50);
INSERT INTO `item_latents` VALUES (13878, 1, 23, 0, 50);
-- Carapace Helm +1
INSERT INTO `item_latents` VALUES (13879, 23, 11, 0, 50);
INSERT INTO `item_latents` VALUES (13879, 1, 24, 0, 50);
-- Carapace Breastplate
INSERT INTO `item_latents` VALUES (13789, 23, 12, 0, 25);
INSERT INTO `item_latents` VALUES (13789, 1, 44, 0, 25);
-- Carapace Breastplate +1
INSERT INTO `item_latents` VALUES (13790, 23, 13, 0, 25);
INSERT INTO `item_latents` VALUES (13790, 1, 45, 0, 25);
-- Hercules' Ring
INSERT INTO `item_latents` VALUES (14659, 370, 3, 0, 50);
INSERT INTO `item_latents` VALUES (14659, 369, 1, 0, 50);
-- Muscle Belt
INSERT INTO `item_latents` VALUES (13185, 370, 1, 0, 50);
INSERT INTO `item_latents` VALUES (13185, 291, 1, 0, 50);
-- Muscle Belt +1
INSERT INTO `item_latents` VALUES (13279, 370, 2, 0, 50);
INSERT INTO `item_latents` VALUES (13279, 291, 2, 0, 50);
-- Horrent Mace
INSERT INTO `item_latents` VALUES (17471, 25, 10, 0, 25);
INSERT INTO `item_latents` VALUES (17471, 23, 10, 0, 25);
-- Kerykeion
INSERT INTO `item_latents` VALUES (18859, 370, 3, 0, 50);
-- Rasetsu Jinpachi
INSERT INTO `item_latents` VALUES (13925, 291, 1, 0, 25);
-- Rasetsu Samue
INSERT INTO `item_latents` VALUES (14376, 291, 1, 0, 25);
-- Rasetsu Tekko
INSERT INTO `item_latents` VALUES (14819, 291, 1, 0, 25);
-- Rasetsu Hakama
INSERT INTO `item_latents` VALUES (14299, 291, 1, 0, 25);
-- Rasetsu Sune-Ate
INSERT INTO `item_latents` VALUES (14178, 291, 1, 0, 25);
-- Unicorn Cap
INSERT INTO `item_latents` VALUES (15209, 8, 4, 46, 75);
-- Unicorn Cap +1
INSERT INTO `item_latents` VALUES (15210, 8, 5, 1, 75);
-- Unicorn Harness
INSERT INTO `item_latents` VALUES (14448, 10, 6, 46, 71);
-- Unicorn Harness +1
INSERT INTO `item_latents` VALUES (14449, 10, 7, 1, 71);
-- Unicorn Mittens
INSERT INTO `item_latents` VALUES (14055, 23, 7, 46, 75);
-- Unicorn Mittens +1
INSERT INTO `item_latents` VALUES (14056, 23, 8, 1, 75);
-- Unicorn Subligar
INSERT INTO `item_latents` VALUES (15406, 68, 3, 46, 75);
-- Unicorn Subligar +1
INSERT INTO `item_latents` VALUES (15407, 68, 4, 1, 75);
-- Unicorn Leggings
INSERT INTO `item_latents` VALUES (15345, 167, 3, 46, 75);
-- Unicorn Leggings +1
INSERT INTO `item_latents` VALUES (15346, 167, 4, 1, 75);
-- Zareehkl Jambiya
INSERT INTO `item_latents` VALUES (19108, 366, 5, 0, 75);
-- Zareehkl Scythe
INSERT INTO `item_latents` VALUES (18949, 302, 1, 0, 25);
-- Soldier's Ring
INSERT INTO `item_latents` VALUES (13286, 288, 2, 2, 75);
-- Soldier's Earring
INSERT INTO `item_latents` VALUES (13419, 63, 20, 2, 25);
-- Kampfer Ring
INSERT INTO `item_latents` VALUES (13287, 291, 2, 2, 75);
-- Kampfer Earring
INSERT INTO `item_latents` VALUES (13420, 291, 5, 2, 25);
-- Medicine Earring
INSERT INTO `item_latents` VALUES (13421, 160, -30, 2, 25);
-- Sorcerer's Earring
INSERT INTO `item_latents` VALUES (13422, 160, -30, 2, 25);
-- Fencer's Earring
INSERT INTO `item_latents` VALUES (13423, 163, -77, 2, 25);
-- Rogue's Earring
INSERT INTO `item_latents` VALUES (13424, 68, 15, 2, 25);
-- Rogue's Ring
INSERT INTO `item_latents` VALUES (13291, 298, 3, 2, 75);
-- Guardian Earring
INSERT INTO `item_latents` VALUES (13425, 168, 30, 2, 25);
-- Slayer's Earring
INSERT INTO `item_latents` VALUES (13426, 161, -20, 2, 25);
-- Tamer's Earring
INSERT INTO `item_latents` VALUES (13427, 304, 5, 2, 25);
-- Minstrel's Earring
INSERT INTO `item_latents` VALUES (13428, 161, -30, 2, 25);
-- Tracker's Ring
INSERT INTO `item_latents` VALUES (13296, 27, 2, 2, 75);
-- Tracker's Earring
INSERT INTO `item_latents` VALUES (13429, 161, -30, 2, 25);
-- Ronin Ring
INSERT INTO `item_latents` VALUES (13297, 25, 5, 2, 75);
-- Ronin Earring
INSERT INTO `item_latents` VALUES (13430, 243, 20, 2, 25);
-- Shinobi Ring
INSERT INTO `item_latents` VALUES (13298, 167, 4, 2, 75);
-- Shinobi Earring
INSERT INTO `item_latents` VALUES (13431, 167, 20, 2, 25);
-- Drake Earring
INSERT INTO `item_latents` VALUES (13432, 288, 5, 2, 25);
-- Conjurer's Ring
INSERT INTO `item_latents` VALUES (13300, 346, 1, 2, 75);
-- Wagh Baghnakhs
INSERT INTO `item_latents` VALUES (18358, 366, 5, 6, 100);
INSERT INTO `item_latents` VALUES (18358, 25, 5, 6, 100);
INSERT INTO `item_latents` VALUES (18358, 23, 14, 6, 100);
-- Blau Dolch
INSERT INTO `item_latents` VALUES (18015, 366, 7, 6, 100);
INSERT INTO `item_latents` VALUES (18015, 25, 5, 6, 100);
INSERT INTO `item_latents` VALUES (18015, 23, 16, 6, 100);
-- Maneater
INSERT INTO `item_latents` VALUES (17946, 366, 6, 6, 100);
INSERT INTO `item_latents` VALUES (17946, 25, 5, 6, 100);
INSERT INTO `item_latents` VALUES (17946, 23, 18, 6, 100);
-- Stone-splitter
INSERT INTO `item_latents` VALUES (18099, 366, 5, 6, 100);
INSERT INTO `item_latents` VALUES (18099, 25, 5, 6, 100);
INSERT INTO `item_latents` VALUES (18099, 23, 24, 6, 100);
-- Onimaru
INSERT INTO `item_latents` VALUES (16976, 366, 6, 6, 100);
INSERT INTO `item_latents` VALUES (16976, 25, 5, 6, 100);
INSERT INTO `item_latents` VALUES (16976, 23, 18, 6, 100);
-- Perdu Hanger
INSERT INTO `item_latents` VALUES (17741, 366, 6, 6, 100);
INSERT INTO `item_latents` VALUES (17741, 25, 5, 6, 100);
INSERT INTO `item_latents` VALUES (17741, 23, 15, 6, 100);
-- Perdu Sickle
INSERT INTO `item_latents` VALUES (18943, 366, 5, 6, 100);
INSERT INTO `item_latents` VALUES (18943, 25, 5, 6, 100);
INSERT INTO `item_latents` VALUES (18943, 23, 14, 6, 100);
-- Perdu Wand
INSERT INTO `item_latents` VALUES (18850, 366, 5, 6, 100);
INSERT INTO `item_latents` VALUES (18850, 25, 5, 6, 100);
INSERT INTO `item_latents` VALUES (18850, 23, 15, 6, 100);
-- Perdu Blade
INSERT INTO `item_latents` VALUES (18425, 366, 5, 6, 100);
INSERT INTO `item_latents` VALUES (18425, 25, 5, 6, 100);
INSERT INTO `item_latents` VALUES (18425, 23, 10, 6, 100);
-- Perdu Staff
INSERT INTO `item_latents` VALUES (18588, 366, 6, 6, 100);
INSERT INTO `item_latents` VALUES (18588, 25, 5, 6, 100);
INSERT INTO `item_latents` VALUES (18588, 23, 15, 6, 100);
-- Perdu Sword
INSERT INTO `item_latents` VALUES (16602, 366, 4, 6, 100);
INSERT INTO `item_latents` VALUES (16602, 25, 5, 6, 100);
INSERT INTO `item_latents` VALUES (16602, 23, 12, 6, 100);
-- Perdu Voulge
INSERT INTO `item_latents` VALUES (18491, 366, 5, 6, 100);
INSERT INTO `item_latents` VALUES (18491, 25, 5, 6, 100);
INSERT INTO `item_latents` VALUES (18491, 23, 10, 6, 100);

INSERT INTO `item_latents` VALUES (15188, 288, 2, 7, 100);
INSERT INTO `item_latents` VALUES (15187, 288, 3, 7, 100);
INSERT INTO `item_latents` VALUES (14876, 23, 10, 7, 100);
INSERT INTO `item_latents` VALUES (14878, 23, 12, 7, 100);
INSERT INTO `item_latents` VALUES (15392, 24, 7, 7, 100);
INSERT INTO `item_latents` VALUES (15394, 24, 8, 7, 100);
INSERT INTO `item_latents` VALUES (11312, 8, 5, 7, 100);
-- Bard Justacorps
INSERT INTO `item_latents` VALUES (15096, 8, 8, 10, 0);
INSERT INTO `item_latents` VALUES (15096, 9, 8, 10, 0);
INSERT INTO `item_latents` VALUES (15096, 10, 8, 10, 0);
INSERT INTO `item_latents` VALUES (15096, 11, 8, 10, 0);
INSERT INTO `item_latents` VALUES (15096, 12, -8, 10, 0);
INSERT INTO `item_latents` VALUES (15096, 13, -8, 10, 0);
INSERT INTO `item_latents` VALUES (15096, 14, -8, 10, 0);
-- Bard Justacorps +1
INSERT INTO `item_latents` VALUES (14509, 8, 8, 10, 0);
INSERT INTO `item_latents` VALUES (14509, 9, 8, 10, 0);
INSERT INTO `item_latents` VALUES (14509, 10, 8, 10, 0);
INSERT INTO `item_latents` VALUES (14509, 11, 8, 10, 0);
INSERT INTO `item_latents` VALUES (15126, 8, -8, 11, 0);
INSERT INTO `item_latents` VALUES (15126, 9, -8, 11, 0);
INSERT INTO `item_latents` VALUES (15126, 10, -8, 11, 0);
INSERT INTO `item_latents` VALUES (15126, 11, -8, 11, 0);
INSERT INTO `item_latents` VALUES (15126, 12, 8, 11, 0);
INSERT INTO `item_latents` VALUES (15126, 13, 8, 11, 0);
INSERT INTO `item_latents` VALUES (15126, 14, 8, 11, 0);
INSERT INTO `item_latents` VALUES (15589, 12, 8, 11, 0);
INSERT INTO `item_latents` VALUES (15589, 13, 8, 11, 0);
INSERT INTO `item_latents` VALUES (15589, 14, 8, 11, 0);
INSERT INTO `item_latents` VALUES (10719, 12, 10, 11, 0);
INSERT INTO `item_latents` VALUES (10719, 13, 10, 11, 0);
INSERT INTO `item_latents` VALUES (10719, 14, 10, 11, 0);
INSERT INTO `item_latents` VALUES (15530, 368, 1, 10, 50);
INSERT INTO `item_latents` VALUES (15174, 25, 12, 10, 50);
INSERT INTO `item_latents` VALUES (15174, 167, 4, 10, 49);
-- Mistilteinn
INSERT INTO `item_latents` VALUES (17073, 369, 1, 44, 3);
INSERT INTO `item_latents` VALUES (15532, 370, 1, 12, 3);
-- Avis
INSERT INTO `item_latents` VALUES (19120, 25, 6, 13, 368);
INSERT INTO `item_latents` VALUES (19120, 25, 6, 13, 369);
INSERT INTO `item_latents` VALUES (19120, 25, 6, 13, 370);
-- Chaotic Earring
INSERT INTO `item_latents` VALUES (15983, 23, 7, 13, 75);
-- Haten Earring
INSERT INTO `item_latents` VALUES (15981, 73, 2, 13, 117);
-- Poppet Katars
INSERT INTO `item_latents` VALUES (18768, 23, 12, 9, 4);
-- Priest's Earring
INSERT INTO `item_latents` VALUES (15982, 1, 8, 13, 74);
-- Ryumon
INSERT INTO `item_latents` VALUES (18422, 62, 1, 13, 66);
INSERT INTO `item_latents` VALUES (18422, 62, 1, 13, 444);
INSERT INTO `item_latents` VALUES (18422, 62, 1, 13, 445);
INSERT INTO `item_latents` VALUES (18422, 62, 1, 13, 446);
-- INSERT INTO `item_latents` VALUES (18422, 62, 1, 13, 66); -- Besieged
-- INSERT INTO `item_latents` VALUES (18422, 62, 1, 13, 444); --
-- INSERT INTO `item_latents` VALUES (18422, 62, 1, 13, 445); --
-- INSERT INTO `item_latents` VALUES (18422, 62, 1, 13, 446); --
-- Enhancing Sword
INSERT INTO `item_latents` VALUES (16605, 25, 8, 13, 94);
INSERT INTO `item_latents` VALUES (16605, 23, 16, 13, 94);
INSERT INTO `item_latents` VALUES (16605, 25, 8, 13, 95);
INSERT INTO `item_latents` VALUES (16605, 23, 16, 13, 95);
INSERT INTO `item_latents` VALUES (16605, 25, 8, 13, 96);
INSERT INTO `item_latents` VALUES (16605, 23, 16, 13, 96);
INSERT INTO `item_latents` VALUES (16605, 25, 8, 13, 97);
INSERT INTO `item_latents` VALUES (16605, 23, 16, 13, 97);
INSERT INTO `item_latents` VALUES (16605, 25, 8, 13, 98);
INSERT INTO `item_latents` VALUES (16605, 23, 16, 13, 98);
INSERT INTO `item_latents` VALUES (16605, 25, 8, 13, 99);
INSERT INTO `item_latents` VALUES (16605, 23, 16, 13, 99);
INSERT INTO `item_latents` VALUES (16605, 25, 8, 13, 277);
INSERT INTO `item_latents` VALUES (16605, 23, 16, 13, 277);
INSERT INTO `item_latents` VALUES (16605, 25, 8, 13, 278);
INSERT INTO `item_latents` VALUES (16605, 23, 16, 13, 278);
INSERT INTO `item_latents` VALUES (16605, 25, 8, 13, 279);
INSERT INTO `item_latents` VALUES (16605, 23, 16, 13, 279);
INSERT INTO `item_latents` VALUES (16605, 25, 8, 13, 280);
INSERT INTO `item_latents` VALUES (16605, 23, 16, 13, 280);
INSERT INTO `item_latents` VALUES (16605, 25, 8, 13, 281);
INSERT INTO `item_latents` VALUES (16605, 23, 16, 13, 281);
INSERT INTO `item_latents` VALUES (16605, 25, 8, 13, 282);
INSERT INTO `item_latents` VALUES (16605, 23, 16, 13, 282);
-- Scogan's Knuckles
INSERT INTO `item_latents` VALUES (18741, 23, 10, 9, 4);
INSERT INTO `item_latents` VALUES (17624, 165, 7, 13, 3);
INSERT INTO `item_latents` VALUES (13693, 370, 1, 13, 2);
INSERT INTO `item_latents` VALUES (13693, 370, 1, 13, 19);
INSERT INTO `item_latents` VALUES (13693, 369, 1, 13, 2);
INSERT INTO `item_latents` VALUES (13693, 369, 1, 13, 19);
INSERT INTO `item_latents` VALUES (13416, 68, 15, 13, 5);
INSERT INTO `item_latents` VALUES (13400, 26, 5, 13, 9);
INSERT INTO `item_latents` VALUES (13400, 26, 5, 13, 20);
INSERT INTO `item_latents` VALUES (13248, 167, 8, 13, 4);
INSERT INTO `item_latents` VALUES (17831, 369, 1, 13, 2);
INSERT INTO `item_latents` VALUES (17831, 370, 1, 13, 19);
-- Nightmare Gloves
INSERT INTO `item_latents` VALUES (14946, 346, 1, 13, 2);
INSERT INTO `item_latents` VALUES (14946, 346, 1, 13, 19);
INSERT INTO `item_latents` VALUES (13143, 368, 25, 13, 2);
INSERT INTO `item_latents` VALUES (13143, 368, 25, 13, 19);
INSERT INTO `item_latents` VALUES (15328, 370, 2, 13, 11);
INSERT INTO `item_latents` VALUES (13655, 161, -20, 13, 7);
INSERT INTO `item_latents` VALUES (12621, 370, 2, 13, 3);
INSERT INTO `item_latents` VALUES (12589, 370, 2, 13, 3);
INSERT INTO `item_latents` VALUES (12751, 71, 4, 13, 6);
INSERT INTO `item_latents` VALUES (12717, 71, 5, 13, 6);
INSERT INTO `item_latents` VALUES (13846, 369, 1, 13, 4);
INSERT INTO `item_latents` VALUES (12461, 369, 1, 13, 4);
INSERT INTO `item_latents` VALUES (16238, 369, 1, 13, 3);
INSERT INTO `item_latents` VALUES (14954, 5, 35, 14, 0);
INSERT INTO `item_latents` VALUES (14954, 168, -5, 14, 0);
INSERT INTO `item_latents` VALUES (14954, 71, 1, 14, 0);
-- Sirius Axe
INSERT INTO `item_latents` VALUES (17952, 160, 2, 15, 2);
INSERT INTO `item_latents` VALUES (17952, 160, 2, 15, 3);
INSERT INTO `item_latents` VALUES (17952, 160, 2, 15, 4);
INSERT INTO `item_latents` VALUES (17952, 160, 2, 15, 5);
INSERT INTO `item_latents` VALUES (17952, 160, 2, 15, 6);
-- Company Sword
INSERT INTO `item_latents` VALUES (17662, 366, 2, 15, 2);
INSERT INTO `item_latents` VALUES (17662, 366, 2, 15, 3);
INSERT INTO `item_latents` VALUES (17662, 366, 2, 15, 4);
INSERT INTO `item_latents` VALUES (17662, 366, 2, 15, 5);
INSERT INTO `item_latents` VALUES (17662, 366, 2, 15, 6);
-- Company Fleuret
INSERT INTO `item_latents` VALUES (17720, 366, 1, 15, 2);
INSERT INTO `item_latents` VALUES (17720, 366, 1, 15, 3);
INSERT INTO `item_latents` VALUES (17720, 366, 1, 15, 4);
INSERT INTO `item_latents` VALUES (17720, 366, 1, 15, 5);
INSERT INTO `item_latents` VALUES (17720, 366, 1, 15, 6);
-- Garuda's Sickle
INSERT INTO `item_latents` VALUES (18063, 25, 13, 21, 13);
-- Ifrit's Bow
INSERT INTO `item_latents` VALUES (17192, 165, 3, 21, 10);
-- Titan's Baselard
INSERT INTO `item_latents` VALUES (18021, 366, 5, 21, 11);
-- Lyft Sainti
INSERT INTO `item_latents` VALUES (18771, 23, 1, 16, 2);
INSERT INTO `item_latents` VALUES (18771, 23, 1, 16, 3);
INSERT INTO `item_latents` VALUES (18771, 23, 1, 16, 4);
INSERT INTO `item_latents` VALUES (18771, 23, 1, 16, 5);
-- Lyft Jambiya
INSERT INTO `item_latents` VALUES (19125, 9, 1, 16, 2);
INSERT INTO `item_latents` VALUES (19125, 9, 1, 16, 3);
INSERT INTO `item_latents` VALUES (19125, 9, 1, 16, 4);
INSERT INTO `item_latents` VALUES (19125, 9, 1, 16, 5);
-- Lyft Scimitar
INSERT INTO `item_latents` VALUES (17766, 10, 2, 16, 2);
INSERT INTO `item_latents` VALUES (17766, 10, 2, 16, 3);
INSERT INTO `item_latents` VALUES (17766, 10, 2, 16, 4);
INSERT INTO `item_latents` VALUES (17766, 10, 2, 16, 5);
-- Lyft Claymore
INSERT INTO `item_latents` VALUES (19161, 8, 1, 16, 2);
INSERT INTO `item_latents` VALUES (19161, 8, 1, 16, 3);
INSERT INTO `item_latents` VALUES (19161, 8, 1, 16, 4);
INSERT INTO `item_latents` VALUES (19161, 8, 1, 16, 5);
-- Lyft Tabar
INSERT INTO `item_latents` VALUES (17970, 9, 1, 16, 2);
INSERT INTO `item_latents` VALUES (17970, 23, 2, 16, 2);
INSERT INTO `item_latents` VALUES (17970, 9, 1, 16, 3);
INSERT INTO `item_latents` VALUES (17970, 23, 2, 16, 3);
INSERT INTO `item_latents` VALUES (17970, 9, 1, 16, 4);
INSERT INTO `item_latents` VALUES (17970, 23, 2, 16, 4);
INSERT INTO `item_latents` VALUES (17970, 9, 1, 16, 5);
INSERT INTO `item_latents` VALUES (17970, 23, 2, 16, 5);
-- Lyft Voulge
INSERT INTO `item_latents` VALUES (18508, 8, 1, 16, 2);
INSERT INTO `item_latents` VALUES (18508, 25, 2, 16, 2);
INSERT INTO `item_latents` VALUES (18508, 8, 1, 16, 3);
INSERT INTO `item_latents` VALUES (18508, 25, 2, 16, 3);
INSERT INTO `item_latents` VALUES (18508, 8, 1, 16, 4);
INSERT INTO `item_latents` VALUES (18508, 25, 2, 16, 4);
INSERT INTO `item_latents` VALUES (18508, 8, 1, 16, 5);
INSERT INTO `item_latents` VALUES (18508, 25, 2, 16, 5);
-- Lyft Scythe
INSERT INTO `item_latents` VALUES (18958, 8, 1, 16, 2);
INSERT INTO `item_latents` VALUES (18958, 8, 1, 16, 3);
INSERT INTO `item_latents` VALUES (18958, 8, 1, 16, 4);
INSERT INTO `item_latents` VALUES (18958, 8, 1, 16, 5);
-- Lyft Lance
INSERT INTO `item_latents` VALUES (19306, 8, 1, 16, 2);
INSERT INTO `item_latents` VALUES (19306, 23, 2, 16, 2);
INSERT INTO `item_latents` VALUES (19306, 8, 1, 16, 3);
INSERT INTO `item_latents` VALUES (19306, 23, 2, 16, 3);
INSERT INTO `item_latents` VALUES (19306, 8, 1, 16, 4);
INSERT INTO `item_latents` VALUES (19306, 23, 2, 16, 4);
INSERT INTO `item_latents` VALUES (19306, 8, 1, 16, 5);
INSERT INTO `item_latents` VALUES (19306, 23, 2, 16, 5);
-- Musanto
INSERT INTO `item_latents` VALUES (19279, 9, 1, 16, 2);
INSERT INTO `item_latents` VALUES (19279, 9, 1, 16, 3);
INSERT INTO `item_latents` VALUES (19279, 9, 1, 16, 4);
INSERT INTO `item_latents` VALUES (19279, 9, 1, 16, 5);
-- Carbuncle Mitts
INSERT INTO `item_latents` VALUES (14062, 346, 4, 9, 8);
-- Karura Hachigane
INSERT INTO `item_latents` VALUES (16154, 346, 2, 9, 13);
INSERT INTO `item_latents` VALUES (16154, 314, 5, 9, 13);
-- INSERT INTO `item_latents` VALUES (16154, ?, 5, 9, 13); Pet Def Bonus%
-- Duende Cotehardie
INSERT INTO `item_latents` VALUES (14401, 346, 2, 9, 7);
-- Nimbus Doublet
INSERT INTO `item_latents` VALUES (14410, 346, 2, 9, 6);

INSERT INTO `item_latents` VALUES (17509, 366, 13, 47, 0);
INSERT INTO `item_latents` VALUES (17509, 165, 6, 47, 0);
-- Heart Snatcher
INSERT INTO `item_latents` VALUES (18005, 366, 15, 47, 0);
INSERT INTO `item_latents` VALUES (18005, 165, 6, 47, 0);
-- Subduer
INSERT INTO `item_latents` VALUES (18378, 366, 13, 47, 0);
INSERT INTO `item_latents` VALUES (18378, 165, 6, 47, 0);
-- Dissector
INSERT INTO `item_latents` VALUES (17699, 366, 13, 47, 0);
INSERT INTO `item_latents` VALUES (17699, 165, 6, 47, 0);
-- Morgenstern
INSERT INTO `item_latents` VALUES (17451, 366, 13, 47, 0);
INSERT INTO `item_latents` VALUES (17451, 165, 6, 47, 0);
-- Michishiba
INSERT INTO `item_latents` VALUES (17827, 366, 13, 47, 0);
INSERT INTO `item_latents` VALUES (17827, 165, 6, 47, 0);
-- Senjuinrikio
INSERT INTO `item_latents` VALUES (17793, 366, 13, 47, 0);
INSERT INTO `item_latents` VALUES (17793, 165, 6, 47, 0);
-- Thyrusstab
INSERT INTO `item_latents` VALUES (17589, 366, 13, 47, 0);
INSERT INTO `item_latents` VALUES (17589, 165, 6, 47, 0);
-- Gravedigger
INSERT INTO `item_latents` VALUES (18053, 366, 13, 47, 0);
INSERT INTO `item_latents` VALUES (18053, 165, 6, 47, 0);
-- Gondo-Shizunori
INSERT INTO `item_latents` VALUES (18097, 366, 13, 47, 0);
INSERT INTO `item_latents` VALUES (18097, 165, 6, 47, 0);
-- Rampager
INSERT INTO `item_latents` VALUES (18217, 366, 13, 47, 0);
INSERT INTO `item_latents` VALUES (18217, 165, 6, 47, 0);
-- Retributor
INSERT INTO `item_latents` VALUES (17944, 366, 13, 47, 0);
INSERT INTO `item_latents` VALUES (17944, 165, 6, 47, 0);
-- Coffinmaker
INSERT INTO `item_latents` VALUES (17275, 376, 13, 47, 0);
-- Bow of Trials
INSERT INTO `item_latents` VALUES (18144, 2, 20, 47, 0);
INSERT INTO `item_latents` VALUES (18144, 17, 10, 47, 0);
INSERT INTO `item_latents` VALUES (18144, 19, 10, 47, 0);
-- Pick of Trials
INSERT INTO `item_latents` VALUES (17933, 2, 20, 47, 0);
INSERT INTO `item_latents` VALUES (17933, 17, 10, 47, 0);
INSERT INTO `item_latents` VALUES (17933, 19, 10, 47, 0);
-- Club of Trials
INSERT INTO `item_latents` VALUES (17456, 2, 10, 47, 0);
INSERT INTO `item_latents` VALUES (17456, 5, 10, 47, 0);
INSERT INTO `item_latents` VALUES (17456, 18, 10, 47, 0);
INSERT INTO `item_latents` VALUES (17456, 20, 10, 47, 0);
-- Dagger of Trials
INSERT INTO `item_latents` VALUES (17616, 2, 20, 47, 0);
INSERT INTO `item_latents` VALUES (17616, 16, 10, 47, 0);
INSERT INTO `item_latents` VALUES (17616, 18, 10, 47, 0);
-- Axe of Trials
INSERT INTO `item_latents` VALUES (16735, 2, 20, 47, 0);
INSERT INTO `item_latents` VALUES (16735, 18, 10, 47, 0);
INSERT INTO `item_latents` VALUES (16735, 20, 10, 47, 0);
-- Tachi of Trials
INSERT INTO `item_latents` VALUES (17815, 2, 20, 47, 0);
INSERT INTO `item_latents` VALUES (17815, 16, 10, 47, 0);
INSERT INTO `item_latents` VALUES (17815, 22, 10, 47, 0);
-- Sword of Trials
INSERT INTO `item_latents` VALUES (16952, 2, 20, 47, 0);
INSERT INTO `item_latents` VALUES (16952, 16, 10, 47, 0);
INSERT INTO `item_latents` VALUES (16952, 18, 10, 47, 0);
-- Knuckles of Trials
INSERT INTO `item_latents` VALUES (17507, 2, 20, 47, 0);
INSERT INTO `item_latents` VALUES (17507, 18, 10, 47, 0);
INSERT INTO `item_latents` VALUES (17507, 20, 10, 47, 0);
-- Kodachi of Trials
INSERT INTO `item_latents` VALUES (17773, 2, 20, 47, 0);
INSERT INTO `item_latents` VALUES (17773, 15, 10, 47, 0);
INSERT INTO `item_latents` VALUES (17773, 21, 10, 47, 0);
-- Gun of Trials
INSERT INTO `item_latents` VALUES (18146, 2, 20, 47, 0);
INSERT INTO `item_latents` VALUES (18146, 18, 10, 47, 0);
INSERT INTO `item_latents` VALUES (18146, 20, 10, 47, 0);
-- Spear of Trials
INSERT INTO `item_latents` VALUES (16892, 2, 20, 47, 0);
INSERT INTO `item_latents` VALUES (16892, 20, 10, 47, 0);
INSERT INTO `item_latents` VALUES (16892, 22, 10, 47, 0);
-- Scythe of Trials
INSERT INTO `item_latents` VALUES (16793, 2, 20, 47, 0);
INSERT INTO `item_latents` VALUES (16793, 19, 10, 47, 0);
INSERT INTO `item_latents` VALUES (16793, 21, 10, 47, 0);
-- Pole of Trials
INSERT INTO `item_latents` VALUES (17527, 2, 10, 47, 0);
INSERT INTO `item_latents` VALUES (17527, 5, 10, 47, 0);
INSERT INTO `item_latents` VALUES (17527, 15, 10, 47, 0);
INSERT INTO `item_latents` VALUES (17527, 21, 10, 47, 0);
-- Sapara of Trials
INSERT INTO `item_latents` VALUES (17654, 2, 20, 47, 0);
INSERT INTO `item_latents` VALUES (17654, 15, 10, 47, 0);
INSERT INTO `item_latents` VALUES (17654, 17, 10, 47, 0);
-- Mercenary Earring
INSERT INTO `item_latents` VALUES (13435, 10, 2, 8, 1);
-- Mercenary Mantle 
INSERT INTO `item_latents` VALUES (13659, 27, 1, 8, 1); 
-- Mercenary's Targe
INSERT INTO `item_latents` VALUES (12389, 2, 5, 8, 1); 
INSERT INTO `item_latents` VALUES (12389, 5, 10, 8, 1); 
-- Axe Belt
INSERT INTO `item_latents` VALUES (15271, 85, 5, 8, 1);
-- Wrestler's Aspis
INSERT INTO `item_latents` VALUES (12390, 12, 2, 8, 2);
-- Wrestler's Earring
INSERT INTO `item_latents` VALUES (13436, 2, 30, 8, 2);
-- Wrestler's Mantle
INSERT INTO `item_latents` VALUES (13660, 173, 10, 8, 2);
-- Cestus Belt
INSERT INTO `item_latents` VALUES (15272, 80, 5, 8, 2); 
-- Dominion Ring
INSERT INTO `item_latents` VALUES (15784, 5, 30, 8, 3); 
-- Healer's Earring
INSERT INTO `item_latents` VALUES (13437, 27, -1, 8, 3); 
-- Healer's Shield
INSERT INTO `item_latents` VALUES (12391, 9, 2, 8, 3); 
-- Healer's Mantle
INSERT INTO `item_latents` VALUES (13661, 16, 1, 8, 3); 
-- Mace Belt
INSERT INTO `item_latents` VALUES (15273, 90, 5, 8, 3); 
-- Wizard's Earring
INSERT INTO `item_latents` VALUES (13438, 115, 5, 8, 4); 
-- Wizard's Mantle
INSERT INTO `item_latents` VALUES (13662, 71, 1, 8, 4); 
-- Wizard's Shield
INSERT INTO `item_latents` VALUES (12392, 8, 2, 8, 4); 
-- Staff Belt
INSERT INTO `item_latents` VALUES (15274, 91, 5, 8, 4); 
-- Templar Hammer
INSERT INTO `item_latents` VALUES (18390, 28, 3, 8, 4); 
-- Warlock's Earring
INSERT INTO `item_latents` VALUES (13439, 71, 1, 8, 5); 
-- Warlock's Mantle
INSERT INTO `item_latents` VALUES (13663, 170, 2, 8, 5); 
-- Warlock's Shield
INSERT INTO `item_latents` VALUES (12393, 2, 7, 8, 5); 
INSERT INTO `item_latents` VALUES (12393, 5, 7, 8, 5); 
-- Rapier Belt
INSERT INTO `item_latents` VALUES (15275, 82, 5, 8, 5); 
-- Pilferer's Aspis
INSERT INTO `item_latents` VALUES (12394, 109, 5, 8, 6); 
-- Pilferer's Earring
INSERT INTO `item_latents` VALUES (14729, 9, 2, 8, 6); 
-- Pilferer's Mantle
INSERT INTO `item_latents` VALUES (13664, 68, 4, 8, 6); 
-- Dagger Belt
INSERT INTO `item_latents` VALUES (15276, 81, 5, 8, 6); 
-- Esquire's Earring
INSERT INTO `item_latents` VALUES (14730, 1, 5, 8, 7); 
-- Esquire's Mantle
INSERT INTO `item_latents` VALUES (13665, 231, 4, 8, 7); 
-- Varlet's Targe
INSERT INTO `item_latents` VALUES (12395, 11, 2, 8, 7); 
-- Shield Belt
INSERT INTO `item_latents` VALUES (15277, 109, 5, 8, 7); 
-- Killer Earring
INSERT INTO `item_latents` VALUES (14731, 23, 5, 8, 8); 
-- Killer Mantle
INSERT INTO `item_latents` VALUES (13666, 116, 5, 8, 8); 
-- Killer Targe
INSERT INTO `item_latents` VALUES (12396, 14, 2, 8, 8); 
INSERT INTO `item_latents` VALUES (12396, 5, 5, 8, 8); 
-- Scythe Belt
INSERT INTO `item_latents` VALUES (15278, 86, 5, 8, 8); 
-- Trimmer's Aspis
INSERT INTO `item_latents` VALUES (12397, 13, 2, 8, 9); 
-- Trimmer's Earring
INSERT INTO `item_latents` VALUES (14732, 25, 5, 8, 9); 
-- Trimmer's Mantle
INSERT INTO `item_latents` VALUES (13667, 250, 5, 8, 9); 
-- Pick Belt
INSERT INTO `item_latents` VALUES (15279, 84, 5, 8, 9); 
-- Singer's Earring
INSERT INTO `item_latents` VALUES (14733, 68, 5, 8, 10); 
-- Singer's Mantle
INSERT INTO `item_latents` VALUES (13668, 244, 5, 8, 10); 
-- Singer's Shield
INSERT INTO `item_latents` VALUES (12398, 110, 5, 8, 10); 
-- Song Belt
INSERT INTO `item_latents` VALUES (15280, 119, 5, 8, 10); 
-- Beater's Aspis
INSERT INTO `item_latents` VALUES (12399, 68, 2, 8, 11); 
-- Beater's Earring
INSERT INTO `item_latents` VALUES (14734, 26, 3, 8, 11); 
-- Beater's Mantle
INSERT INTO `item_latents` VALUES (13669, 359, 5, 8, 11); 
-- Gun Belt
INSERT INTO `item_latents` VALUES (15281, 105, 5, 8, 11); 
-- Ashigaru Earring
INSERT INTO `item_latents` VALUES (14735, 110, 5, 8, 12); 
-- Ashigaru Mantle
INSERT INTO `item_latents` VALUES (13670, 243, 5, 8, 12); 
-- Ashigaru Targe
INSERT INTO `item_latents` VALUES (12400, 106, 5, 8, 12); 
-- Katana Obi
INSERT INTO `item_latents` VALUES (15282, 73, 1, 8, 12); 
-- Genin Aspis
INSERT INTO `item_latents` VALUES (12401, 23, 5, 8, 13); 
-- Genin Earring
INSERT INTO `item_latents` VALUES (14736, 11, 4, 8, 13); 
-- Genin Mantle
INSERT INTO `item_latents` VALUES (13671, 247, 5, 8, 13); 
-- Sarashi
INSERT INTO `item_latents` VALUES (15283, 259, 1, 8, 13); 
-- Lance Belt
INSERT INTO `item_latents` VALUES (15284, 87, 5, 8, 14); 
-- Wyvern Earring
INSERT INTO `item_latents` VALUES (14737, 167, 5, 8, 14); 
-- Wyvern Mantle
INSERT INTO `item_latents` VALUES (13672, 23, 6, 8, 14);
-- Wyvern Targe
INSERT INTO `item_latents` VALUES (12402, 167, 1, 8, 14);
-- Magician's Earring
INSERT INTO `item_latents` VALUES (14738, 5, 30, 8, 15); 
-- Magician's Mantle
INSERT INTO `item_latents` VALUES (13673, 117, 5, 8, 15); 
-- Magician's Shield
INSERT INTO `item_latents` VALUES (12403, 2, 10, 8, 15); 
INSERT INTO `item_latents` VALUES (12403, 5, 5, 8, 15); 
-- Avatar Belt
INSERT INTO `item_latents` VALUES (15285, 346, 2, 8, 15); 
-- Immortal's Cape
INSERT INTO `item_latents` VALUES (16217, 240, 5, 8, 16); 
-- Immortal's Earring
INSERT INTO `item_latents` VALUES (15975, 122, 5, 8, 16); 
-- Pirate's Cape
INSERT INTO `item_latents` VALUES (16218, 242, 5, 8, 17); 
-- Pirate's Earring
INSERT INTO `item_latents` VALUES (15976, 24, 5, 8, 17); 
-- Busker's Cape
INSERT INTO `item_latents` VALUES (16219, 107, 5, 8, 18); 
-- Busker's Earring
INSERT INTO `item_latents` VALUES (15977, 27, -2, 8, 18); 
-- Thunder Belt 
-- INSERT INTO `item_latents` VALUES (11759, ?, ?, ?, ?); -- Increase WS Accuracy & Damage
-- Soil Belt
-- INSERT INTO `item_latents` VALUES (11758, ?, ?, ?, ?); -- Increase WS Accuracy & Damage
-- Snow Belt
-- INSERT INTO `item_latents` VALUES (11756, ?, ?, ?, ?); -- Increase WS Accuracy & Damage
-- Shadow Belt
-- INSERT INTO `item_latents` VALUES (11762, ?, ?, ?, ?); -- Increase WS Accuracy & Damage
-- Light Belt
-- INSERT INTO `item_latents` VALUES (11761, ?, ?, ?, ?); -- Increase WS Accuracy & Damage
-- Flame Belt
-- INSERT INTO `item_latents` VALUES (11755, ?, ?, ?, ?); -- Increase WS Accuracy & Damage
-- Breeze Belt
-- INSERT INTO `item_latents` VALUES (11757, ?, ?, ?, ?); -- Increase WS Accuracy & Damage
-- Aqua Belt
-- INSERT INTO `item_latents` VALUES (11760, ?, ?, ?, ?); -- Increase WS Accuracy & Damage
-- Zeta Sash
INSERT INTO `item_latents` VALUES (15901, 165, 5, 23, 73); 
INSERT INTO `item_latents` VALUES (15901, 165, 5, 23, 74); 
INSERT INTO `item_latents` VALUES (15901, 165, 5, 23, 75); 
INSERT INTO `item_latents` VALUES (15901, 165, 5, 23, 76); 
-- Theta Sash
INSERT INTO `item_latents` VALUES (15904, 115, 3, 23, 73); 
INSERT INTO `item_latents` VALUES (15904, 115, 3, 23, 74); 
INSERT INTO `item_latents` VALUES (15904, 115, 3, 23, 75); 
INSERT INTO `item_latents` VALUES (15904, 115, 3, 23, 76);
INSERT INTO `item_latents` VALUES (15904, 116, 3, 23, 73); 
INSERT INTO `item_latents` VALUES (15904, 116, 3, 23, 74); 
INSERT INTO `item_latents` VALUES (15904, 116, 3, 23, 75); 
INSERT INTO `item_latents` VALUES (15904, 116, 3, 23, 76);
-- Nu Sash
INSERT INTO `item_latents` VALUES (15900, 289, 5, 23, 73); 
INSERT INTO `item_latents` VALUES (15900, 289, 5, 23, 74); 
INSERT INTO `item_latents` VALUES (15900, 289, 5, 23, 75); 
INSERT INTO `item_latents` VALUES (15900, 289, 5, 23, 76);
-- Lambda Sash
INSERT INTO `item_latents` VALUES (15902, 120, 3, 23, 73); 
INSERT INTO `item_latents` VALUES (15902, 120, 3, 23, 74); 
INSERT INTO `item_latents` VALUES (15902, 120, 3, 23, 75); 
INSERT INTO `item_latents` VALUES (15902, 120, 3, 23, 76);
INSERT INTO `item_latents` VALUES (15902, 374, 3, 23, 73); 
INSERT INTO `item_latents` VALUES (15902, 374, 3, 23, 74); 
INSERT INTO `item_latents` VALUES (15902, 374, 3, 23, 75); 
INSERT INTO `item_latents` VALUES (15902, 374, 3, 23, 76);
-- Ksi Sash
INSERT INTO `item_latents` VALUES (15903, 28, 2, 23, 73); 
INSERT INTO `item_latents` VALUES (15903, 28, 2, 23, 74); 
INSERT INTO `item_latents` VALUES (15903, 28, 2, 23, 75); 
INSERT INTO `item_latents` VALUES (15903, 28, 2, 23, 76);
INSERT INTO `item_latents` VALUES (15903, 122, 3, 23, 73); 
INSERT INTO `item_latents` VALUES (15903, 122, 3, 23, 74); 
INSERT INTO `item_latents` VALUES (15903, 122, 3, 23, 75); 
INSERT INTO `item_latents` VALUES (15903, 122, 3, 23, 76);
-- Fatality Belt
INSERT INTO `item_latents` VALUES (15955, 421, 2, 13, 44);
INSERT INTO `item_latents` VALUES (15955, 421, 2, 13, 48);
INSERT INTO `item_latents` VALUES (15955, 421, 2, 13, 49);
INSERT INTO `item_latents` VALUES (15955, 421, 2, 13, 50);
INSERT INTO `item_latents` VALUES (15955, 421, 2, 13, 51);
-- INSERT INTO `item_latents` VALUES (15955, 421, 2, 13, ??); -- critical hit damage 2% under status familiar (bst)
INSERT INTO `item_latents` VALUES (15955, 421, 2, 13, 52);
INSERT INTO `item_latents` VALUES (15955, 421, 2, 13, 53);
INSERT INTO `item_latents` VALUES (15955, 421, 2, 13, 54);
-- INSERT INTO `item_latents` VALUES (15955, 421, 2, 13, ??); -- critical hit damage 2% under status mijin gakure (nin)
INSERT INTO `item_latents` VALUES (15955, 421, 2, 13, 126);
INSERT INTO `item_latents` VALUES (15955, 421, 2, 13, 163);
-- INSERT INTO `item_latents` VALUES (15955, 421, 2, 13, ??); -- critical hit damage 2% under status wild card (cor)
INSERT INTO `item_latents` VALUES (15955, 421, 2, 13, 376);
-- Lycopodium Sash
INSERT INTO `item_latents` VALUES (15928, 370, 3, 26, 0);
-- Suirin Obi
INSERT INTO `item_latents` VALUES (15440, 3, 10, 36, 0);
INSERT INTO `item_latents` VALUES (15440, 37, 10, 52, 3);
-- INSERT INTO `item_latents` VALUES (15440, 37, 15, ?, ?); -- 25% magic matching single weather
-- Rairin Obi
INSERT INTO `item_latents` VALUES (15439, 36, 10, 36, 0);
INSERT INTO `item_latents` VALUES (15439, 36, 10, 52, 6);
-- INSERT INTO `item_latents` VALUES (15439, 36, 15, ?, ?); -- 25% magic matching single weather
-- Korin Obi
INSERT INTO `item_latents` VALUES (15441, 38, 10, 36, 0);
INSERT INTO `item_latents` VALUES (1544, 38, 10, 52, 7);
-- INSERT INTO `item_latents` VALUES (15441, 38, 15, ?, ?); -- 25% magic matching single weather
-- Karin Obi
INSERT INTO `item_latents` VALUES (15435, 3, 10, 36, 0);
INSERT INTO `item_latents` VALUES (15435, 32, 10, 52, 1);
-- INSERT INTO `item_latents` VALUES (15435, 32, 15, ?, ?); -- 25% magic matching single weather
-- Hyorin Obi
INSERT INTO `item_latents` VALUES (15436, 33, 10, 36, 0);
INSERT INTO `item_latents` VALUES (15436, 33, 10, 52, 5);
-- INSERT INTO `item_latents` VALUES (15436, 33, 15, ?, ?); -- 25% magic matching single weather
-- Furin Obi
INSERT INTO `item_latents` VALUES (15437, 34, 10, 36, 0);
INSERT INTO `item_latents` VALUES (15437, 34, 10, 52, 4);
-- INSERT INTO `item_latents` VALUES (15437, 34, 15, ?, ?); -- 25% magic matching single weather
-- Dorin Obi
INSERT INTO `item_latents` VALUES (15438, 35, 10, 36, 0);
INSERT INTO `item_latents` VALUES (15438, 35, 10, 52, 2);
-- INSERT INTO `item_latents` VALUES (15438, 35, 15, ?, ?); -- 25% magic matching single weather
-- Anrin Obi
INSERT INTO `item_latents` VALUES (15443, 39, 10, 36, 0);
INSERT INTO `item_latents` VALUES (15442, 39, 10, 52, 8);
-- INSERT INTO `item_latents` VALUES (15442, 39, 15, ?, ?); -- 25% magic matching single weather
-- Sothic Rope
INSERT INTO `item_latents` VALUES (15915, 5, 20, 65, 0);
INSERT INTO `item_latents` VALUES (15915, 12, 6, 65, 0);
INSERT INTO `item_latents` VALUES (15915, 13, 6, 65, 0);
-- Mandraguard
INSERT INTO `item_latents` VALUES (10807, 370, 1, 26, 0);
-- Riot Shield
-- INSERT INTO `item_latents` VALUES (16174, 1, 71, ?, ?); -- wear no other armor
-- Ancile
INSERT INTO `item_latents` VALUES (15069, 385, 1, 23, 39);
INSERT INTO `item_latents` VALUES (15069, 385, 1, 23, 40);
INSERT INTO `item_latents` VALUES (15069, 385, 1, 23, 41);
INSERT INTO `item_latents` VALUES (15069, 385, 1, 23, 42);
INSERT INTO `item_latents` VALUES (15069, 385, 1, 23, 134);
INSERT INTO `item_latents` VALUES (15069, 385, 1, 23, 135);
INSERT INTO `item_latents` VALUES (15069, 385, 1, 23, 185);
INSERT INTO `item_latents` VALUES (15069, 385, 1, 23, 186);
INSERT INTO `item_latents` VALUES (15069, 385, 1, 23, 187);
INSERT INTO `item_latents` VALUES (15069, 385, 1, 23, 188);
-- Bulwark
INSERT INTO `item_latents` VALUES (15067, 1, 22, 23, 39);
INSERT INTO `item_latents` VALUES (15067, 1, 22, 23, 40);
INSERT INTO `item_latents` VALUES (15067, 1, 22, 23, 41);
INSERT INTO `item_latents` VALUES (15067, 1, 22, 23, 42);
INSERT INTO `item_latents` VALUES (15067, 1, 22, 23, 134);
INSERT INTO `item_latents` VALUES (15067, 1, 22, 23, 135);
INSERT INTO `item_latents` VALUES (15067, 1, 22, 23, 185);
INSERT INTO `item_latents` VALUES (15067, 1, 22, 23, 186);
INSERT INTO `item_latents` VALUES (15067, 1, 22, 23, 187);
INSERT INTO `item_latents` VALUES (15067, 1, 22, 23, 188);
-- Lavalier
INSERT INTO `item_latents` VALUES (10961, 60, -40, 13, 14);
INSERT INTO `item_latents` VALUES (10961, 61, -40, 13, 14);
-- Lavalier +1
INSERT INTO `item_latents` VALUES (10962, 60, -50, 13, 14);
INSERT INTO `item_latents` VALUES (10962, 61, -50, 13, 14);
INSERT INTO `item_latents` VALUES (10962, 169, -20, 13, 14);
-- Rancor Collar
-- INSERT INTO `item_latents` VALUES (10931, 160, 10, ?, ?); Dependant on Tonberrys Grudge
-- INSERT INTO `item_latents` VALUES (10931, 165, 5, ?, ?);
-- Chrysopoeia Torque
INSERT INTO `item_latents` VALUES (11621, 370, 1, 7, 100);
INSERT INTO `item_latents` VALUES (11621, 368, -1, 7, 100);
-- Rho Necklace
INSERT INTO `item_latents` VALUES (15903, 1, 20, 23, 73); 
INSERT INTO `item_latents` VALUES (15903, 1, 20, 23, 74); 
INSERT INTO `item_latents` VALUES (15903, 1, 20, 23, 75); 
INSERT INTO `item_latents` VALUES (15903, 1, 20, 23, 76);
INSERT INTO `item_latents` VALUES (15903, 117, 3, 23, 73); 
INSERT INTO `item_latents` VALUES (15903, 117, 3, 23, 74); 
INSERT INTO `item_latents` VALUES (15903, 117, 3, 23, 75); 
INSERT INTO `item_latents` VALUES (15903, 117, 3, 23, 76);
-- Phi Necklace
INSERT INTO `item_latents` VALUES (15538, 112, 3, 23, 73); 
INSERT INTO `item_latents` VALUES (15538, 112, 3, 23, 74); 
INSERT INTO `item_latents` VALUES (15538, 112, 3, 23, 75); 
INSERT INTO `item_latents` VALUES (15538, 112, 3, 23, 76);
INSERT INTO `item_latents` VALUES (15538, 114, 3, 23, 73); 
INSERT INTO `item_latents` VALUES (15538, 114, 3, 23, 74); 
INSERT INTO `item_latents` VALUES (15538, 114, 3, 23, 75); 
INSERT INTO `item_latents` VALUES (15538, 114, 3, 23, 76);
-- Mu Necklace
INSERT INTO `item_latents` VALUES (15534, 112, 3, 23, 73); 
INSERT INTO `item_latents` VALUES (15534, 112, 3, 23, 74); 
INSERT INTO `item_latents` VALUES (15534, 112, 3, 23, 75); 
INSERT INTO `item_latents` VALUES (15534, 112, 3, 23, 76);
INSERT INTO `item_latents` VALUES (15534, 114, 3, 23, 73); 
INSERT INTO `item_latents` VALUES (15534, 114, 3, 23, 74); 
INSERT INTO `item_latents` VALUES (15534, 114, 3, 23, 75); 
INSERT INTO `item_latents` VALUES (15534, 114, 3, 23, 76);
-- Kappa Necklace
INSERT INTO `item_latents` VALUES (15537, 305, 1, 23, 73); 
INSERT INTO `item_latents` VALUES (15537, 305, 1, 23, 74); 
INSERT INTO `item_latents` VALUES (15537, 305, 1, 23, 75); 
INSERT INTO `item_latents` VALUES (15537, 305, 1, 23, 76);
-- Halting Stole
INSERT INTO `item_latents` VALUES (16306, 25, 4, 13, 73);
-- Colossuss Torque
INSERT INTO `item_latents` VALUES (11590, 112, 10, 36, 0); 
INSERT INTO `item_latents` VALUES (11590, 113, 10, 36, 0);
-- Chi Necklace
INSERT INTO `item_latents` VALUES (15535, 296, 2, 23, 73);
INSERT INTO `item_latents` VALUES (15535, 296, 2, 23, 74); 
INSERT INTO `item_latents` VALUES (15535, 296, 2, 23, 75); 
INSERT INTO `item_latents` VALUES (15535, 296, 2, 23, 76);
-- Aesir Torque
INSERT INTO `item_latents` VALUES (11589, 115, 10, 36, 0);
INSERT INTO `item_latents` VALUES (11589, 116, 10, 36, 0);
-- Aces Locket
INSERT INTO `item_latents` VALUES (11529, 305, 5, 25, 0);
-- Uggalepih Pendant
INSERT INTO `item_latents` VALUES (13145, 28, 8, 4, 50);
-- Fenrirs Stone
INSERT INTO `item_latents` VALUES (18165, 2, 30, 26, 0);
INSERT INTO `item_latents` VALUES (18165, 68, 10, 26, 1);
-- Diaboloss Torque
INSERT INTO `item_latents` VALUES (15516, 23, 8, 52, 8);
INSERT INTO `item_latents` VALUES (15516, 26, -8, 52, 8);
-- Fenrir's Torque
INSERT INTO `item_latents` VALUES (13138, 5, 30, 26, 0);
INSERT INTO `item_latents` VALUES (13138, 27, -3, 26, 1);
-- Iga Zukin +2
INSERT INTO `item_latents` VALUES (11076, 302, 5, 13, 421);
-- Iga Zukin +1
INSERT INTO `item_latents` VALUES (11176, 288, 3, 13, 421);
-- Coven Hat
INSERT INTO `item_latents` VALUES (16076, 3, 3, 42, 0);
INSERT INTO `item_latents` VALUES (16076, 6, 3, 41, 1);
-- Destrier Beret
INSERT INTO `item_latents` VALUES (11811, 169, 12, 50, 31);
INSERT INTO `item_latents` VALUES (11811, 369, 1, 50, 31);
INSERT INTO `item_latents` VALUES (11811, 370, 1, 50, 31);
INSERT INTO `item_latents` VALUES (11811, 613, 1, 50, 31);
-- Horror Head
INSERT INTO `item_latents` VALUES (13917, 27, -50, 66, 0);
-- Horror Head II
INSERT INTO `item_latents` VALUES (15177, 27, -50, 66, 1);
-- Pyracmon Cap
INSERT INTO `item_latents` VALUES (10447, 369, 1, 66, 0);
-- Snow Bunny Hat
INSERT INTO `item_latents` VALUES (11490, 169, 20, 13, 127);
-- Snow Bunny Hat +1
INSERT INTO `item_latents` VALUES (11491, 169, 20, 13, 127);
-- Mekira-oto +1
INSERT INTO `item_latents` VALUES (10869, 32, 10, 28, 0);
INSERT INTO `item_latents` VALUES (10869, 33, 10, 34, 0);
INSERT INTO `item_latents` VALUES (10869, 34, 10, 31, 0);
INSERT INTO `item_latents` VALUES (10869, 35, 10, 29, 0);
INSERT INTO `item_latents` VALUES (10869, 36, 10, 35, 0);
INSERT INTO `item_latents` VALUES (10869, 37, 10, 30, 0);
INSERT INTO `item_latents` VALUES (10869, 38, 10, 36, 0);
INSERT INTO `item_latents` VALUES (10869, 39, 10, 32, 0);
-- Mekira-oto
INSERT INTO `item_latents` VALUES (10866, 32, 10, 28, 0);
INSERT INTO `item_latents` VALUES (10866, 33, 10, 34, 0);
INSERT INTO `item_latents` VALUES (10866, 34, 10, 31, 0);
INSERT INTO `item_latents` VALUES (10866, 35, 10, 29, 0);
INSERT INTO `item_latents` VALUES (10866, 36, 10, 35, 0);
INSERT INTO `item_latents` VALUES (10866, 37, 10, 30, 0);
INSERT INTO `item_latents` VALUES (10866, 38, 10, 36, 0);
INSERT INTO `item_latents` VALUES (10866, 39, 10, 32, 0);
-- Koga Hatsuburi +2
INSERT INTO `item_latents` VALUES (10662, 110, 12, 26, 2);
-- Cocoon Band
INSERT INTO `item_latents` VALUES (11823, 1, 35, 13, 13);
-- Suijin Kabuto
INSERT INTO `item_latents` VALUES (13852, 370, 1, 30, 0);
-- Rasetsu Jinpachi
INSERT INTO `item_latents` VALUES (13852, 291, 1, 0, 25);
-- Kawahori Kabuto
INSERT INTO `item_latents` VALUES (16071, 165, 5, 13, 5);
INSERT INTO `item_latents` VALUES (16071, 48, 5, 13, 5);
-- Koga Hatsuburi
INSERT INTO `item_latents` VALUES (15084, 110, 10, 26, 1);
-- Koga Hatsuburi +1
INSERT INTO `item_latents` VALUES (15257, 110, 12, 26, 2);
-- Stout Bonnet
-- INSERT INTO `item_latents` VALUES (16105, 360, 6, ?, ?); -- Vs Beasts
-- Militant Knuckles
INSERT INTO `item_latents` VALUES (18261, 366, 15, 48, 0);
INSERT INTO `item_latents` VALUES (18261, 59, 7, 48, 0);
-- Dynamis Knuckles
INSERT INTO `item_latents` VALUES (18262, 366, 2, 48, 0);
INSERT INTO `item_latents` VALUES (18262, 59, 9, 48, 0);
-- Malefic Dagger
INSERT INTO `item_latents` VALUES (18267, 366, 22, 48, 0);
INSERT INTO `item_latents` VALUES (18267, 39, 7, 48, 0);
-- Dynamis Dagger
INSERT INTO `item_latents` VALUES (18268, 366, 3, 48, 0);
INSERT INTO `item_latents` VALUES (18268, 39, 9, 48, 0);
-- Glyphic Sword
INSERT INTO `item_latents` VALUES (18273, 366, 39, 48, 0);
INSERT INTO `item_latents` VALUES (18273, 58, 7, 48, 0);
-- Dynamis Blade
INSERT INTO `item_latents` VALUES (18274, 366, 8, 48, 0);
INSERT INTO `item_latents` VALUES (18274, 58, 9, 48, 0);
-- Gilded Blade
INSERT INTO `item_latents` VALUES (18279, 366, 79, 48, 0);
INSERT INTO `item_latents` VALUES (18279, 56, 7, 48, 0);
-- Dynamis Blade
INSERT INTO `item_latents` VALUES (18280, 366, 5, 48, 0);
INSERT INTO `item_latents` VALUES (18280, 56, 9, 48, 0);
-- Leonine Axe
INSERT INTO `item_latents` VALUES (18285, 366, 25, 48, 0);
INSERT INTO `item_latents` VALUES (18285, 60, 7, 48, 0);
-- Dynamis Axe
INSERT INTO `item_latents` VALUES (18286, 366, 4, 48, 0);
INSERT INTO `item_latents` VALUES (18286, 60, 9, 48, 0);
-- Agonal Bhuj
INSERT INTO `item_latents` VALUES (18291, 366, 86, 48, 0);
INSERT INTO `item_latents` VALUES (18291, 60, 7, 48, 0);
-- Dynamis Bhuj
INSERT INTO `item_latents` VALUES (18292, 366, 4, 48, 0);
INSERT INTO `item_latents` VALUES (18292, 60, 9, 48, 0);
-- Hotspur Lance
INSERT INTO `item_latents` VALUES (18297, 366, 81, 48, 0);
INSERT INTO `item_latents` VALUES (18297, 57, 7, 48, 0);
-- Dynamis Lance
INSERT INTO `item_latents` VALUES (18298, 366, 2, 48, 0);
INSERT INTO `item_latents` VALUES (18298, 57, 9, 48, 0);
INSERT INTO `item_latents` VALUES (18303, 366, 81, 48, 0);
INSERT INTO `item_latents` VALUES (18303, 55, 7, 48, 0);
INSERT INTO `item_latents` VALUES (18304, 366, 3, 48, 0);
INSERT INTO `item_latents` VALUES (18304, 55, 9, 48, 0);
-- Mimizuku
INSERT INTO `item_latents` VALUES (18309, 366, 32, 48, 0);
INSERT INTO `item_latents` VALUES (18309, 61, 7, 48, 0);
-- Rogetsu
INSERT INTO `item_latents` VALUES (18310, 366, 7, 48, 0);
INSERT INTO `item_latents` VALUES (18310, 61, 9, 48, 0);
-- Hayatemaru
INSERT INTO `item_latents` VALUES (18315, 366, 75, 48, 0);
INSERT INTO `item_latents` VALUES (18315, 56, 7, 48, 0);
-- Oboromaru
INSERT INTO `item_latents` VALUES (18316, 366, 3, 48, 0);
INSERT INTO `item_latents` VALUES (18316, 56, 9, 48, 0);
-- Battering Maul
INSERT INTO `item_latents` VALUES (18321, 366, 28, 48, 0);
INSERT INTO `item_latents` VALUES (18321, 58, 7, 48, 0);
-- Dynamis Maul
INSERT INTO `item_latents` VALUES (18322, 366, 5, 48, 0);
INSERT INTO `item_latents` VALUES (18322, 58, 9, 48, 0);
-- Sage's Staff
INSERT INTO `item_latents` VALUES (18327, 366, 54, 48, 0);
INSERT INTO `item_latents` VALUES (18327, 54, 7, 48, 0);
-- Dynamis Staff
INSERT INTO `item_latents` VALUES (18328, 366, 3, 48, 0);
INSERT INTO `item_latents` VALUES (18328, 54, 9, 48, 0);
-- Marksman Gun
INSERT INTO `item_latents` VALUES (18333, 366, 38, 48, 0);
INSERT INTO `item_latents` VALUES (18333, 54, 7, 48, 0);
-- Dynamis Gun
INSERT INTO `item_latents` VALUES (18334, 366, 3, 48, 0);
INSERT INTO `item_latents` VALUES (18334, 54, 9, 48, 0);
-- Pyrrhic Horn
INSERT INTO `item_latents` VALUES (18339, 14, 1, 48, 0);
INSERT INTO `item_latents` VALUES (18339, 55, 7, 48, 0);
-- Dynamis Horn
INSERT INTO `item_latents` VALUES (18340, 14, 2, 48, 0);
INSERT INTO `item_latents` VALUES (18340, 55, 9, 48, 0);
-- Millennium Horn
INSERT INTO `item_latents` VALUES (18341, 14, 3, 48, 0);
INSERT INTO `item_latents` VALUES (18341, 436, 2, 48, 0);
-- Wolver Bow
INSERT INTO `item_latents` VALUES (18345, 376, 67, 48, 0);
INSERT INTO `item_latents` VALUES (18345, 59, 7, 48, 0);
-- Dynamis Bow
INSERT INTO `item_latents` VALUES (18346, 376, 3, 48, 0);
INSERT INTO `item_latents` VALUES (18346, 59, 9, 48, 0);
INSERT INTO `item_latents` VALUES (15067, 1, 21, 48, 0);
INSERT INTO `item_latents` VALUES (15068, 1, 6, 48, 0);
INSERT INTO `item_latents` VALUES (15069, 385, 200, 48, 0);
-- Shadow Mantle
INSERT INTO `item_latents` VALUES (13658, 10, 20, 32, 0);
-- Rancorous Mantle
-- INSERT INTO `item_latents` VALUES (10991, 165, 5, ?, ?); -- Tonyberrys Grudge Crit Rate +5%
-- INSERT INTO `item_latents` VALUES (10991, 160, 10, ?, ?); -- Tonyberrys Grudge DMG Taken +%10
-- Twilight Cape
INSERT INTO `item_latents` VALUES (16259, 32, 5, 28, 0);
INSERT INTO `item_latents` VALUES (16259, 33, 5, 34, 0);
INSERT INTO `item_latents` VALUES (16259, 34, 5, 31, 0);
INSERT INTO `item_latents` VALUES (16259, 35, 5, 29, 0);
INSERT INTO `item_latents` VALUES (16259, 36, 5, 35, 0);
INSERT INTO `item_latents` VALUES (16259, 37, 5, 30, 0);
INSERT INTO `item_latents` VALUES (16259, 38, 5, 36, 0);
INSERT INTO `item_latents` VALUES (16259, 39, 5, 32, 0);
INSERT INTO `item_latents` VALUES (16259, 32, 5, 52, 1);
INSERT INTO `item_latents` VALUES (16259, 33, 5, 52, 5);
INSERT INTO `item_latents` VALUES (16259, 34, 5, 52, 4);
INSERT INTO `item_latents` VALUES (16259, 35, 5, 52, 2);
INSERT INTO `item_latents` VALUES (16259, 36, 5, 52, 6);
INSERT INTO `item_latents` VALUES (16259, 37, 5, 52, 3);
INSERT INTO `item_latents` VALUES (16259, 38, 5, 52, 7);
INSERT INTO `item_latents` VALUES (16259, 39, 5, 52, 8);
-- Bond Cape
-- INSERT INTO `item_latents` VALUES (11576, 30, ?, ?, ?); -- Magical Acc + ? Latent unknown
-- Archon Cape
INSERT INTO `item_latents` VALUES (10975, 23, 13, 58, 8);
INSERT INTO `item_latents` VALUES (10975, 25, 13, 52, 8);
-- Colossuss Mantle
INSERT INTO `item_latents` VALUES (11547, 163, -3, 36, 0);
-- Aesir Mantle
INSERT INTO `item_latents` VALUES (11546, 288, 2, 32, 0);
-- Mercenary's Mantle
-- INSERT INTO `item_latents` VALUES (16222, 384, 31, ?, ?); -- Besieged Haste +3%
-- Fenrir's Cape
INSERT INTO `item_latents` VALUES (13572, 1, 10, 26, 0);
INSERT INTO `item_latents` VALUES (13572, 27, 3, 26, 1);
-- Viator Cape
-- INSERT INTO `item_latents` VALUES (16246, 2, 15, ?, ?); -- Campaign
-- INSERT INTO `item_latents` VALUES (16246, 3, 5, ?, ?); -- Campaign
-- INSERT INTO `item_latents` VALUES (16246, 5, 15, ?, ?); -- Campaign
-- INSERT INTO `item_latents` VALUES (16246, 6, 5, ?, ?); -- Campaign
-- INSERT INTO `item_latents` VALUES (16246, 72, 2, ?, ?); -- Campaign
-- Arrestor Mantle
-- INSERT INTO `item_latents` VALUES (16258, 114, 20, ?, ?); -- Campaign
-- Variable Mantle
INSERT INTO `item_latents` VALUES (13680, 1, 1, 51, 30);
INSERT INTO `item_latents` VALUES (13680, 1, 1, 51, 40);
INSERT INTO `item_latents` VALUES (13680, 1, 1, 51, 50);
INSERT INTO `item_latents` VALUES (13680, 1, 1, 51, 60);
INSERT INTO `item_latents` VALUES (13680, 1, 1, 51, 70);
INSERT INTO `item_latents` VALUES (13680, 1, 1, 51, 80);
INSERT INTO `item_latents` VALUES (13680, 1, 1, 51, 90);
-- Variable Cape
INSERT INTO `item_latents` VALUES (13681, 1, 1, 51, 30);
INSERT INTO `item_latents` VALUES (13681, 1, 1, 51, 40);
INSERT INTO `item_latents` VALUES (13681, 1, 1, 51, 50);
INSERT INTO `item_latents` VALUES (13681, 1, 1, 51, 60);
INSERT INTO `item_latents` VALUES (13681, 1, 1, 51, 70);
INSERT INTO `item_latents` VALUES (13681, 1, 1, 51, 80);
INSERT INTO `item_latents` VALUES (13681, 1, 1, 51, 90);
INSERT INTO `item_latents` VALUES (13681, 5, 2, 51, 25);
INSERT INTO `item_latents` VALUES (13681, 5, 2, 51, 30);
INSERT INTO `item_latents` VALUES (13681, 5, 2, 51, 35);
INSERT INTO `item_latents` VALUES (13681, 5, 2, 51, 40);
INSERT INTO `item_latents` VALUES (13681, 5, 2, 51, 45);
INSERT INTO `item_latents` VALUES (13681, 5, 2, 51, 50);
INSERT INTO `item_latents` VALUES (13681, 5, 2, 51, 55);
INSERT INTO `item_latents` VALUES (13681, 5, 2, 51, 60);
INSERT INTO `item_latents` VALUES (13681, 5, 2, 51, 65);
INSERT INTO `item_latents` VALUES (13681, 5, 2, 51, 70);
INSERT INTO `item_latents` VALUES (13681, 5, 2, 51, 75);
INSERT INTO `item_latents` VALUES (13681, 5, 2, 51, 80);
INSERT INTO `item_latents` VALUES (13681, 5, 2, 51, 85);
INSERT INTO `item_latents` VALUES (13681, 5, 2, 51, 90);
INSERT INTO `item_latents` VALUES (13681, 5, 2, 51, 95);
-- Variable Ring
-- INSERT INTO `item_latents` VALUES (14653, 5, ?, ?, ?); -- Garrison Variable MP with Lvl ??
-- INSERT INTO `item_latents` VALUES (14653, 71, ?, ?, ?); -- Garrison Variable MP while healing with Lvl ??
-- Resentment Cape
INSERT INTO `item_latents` VALUES (15468, 163, -13, 53, 1);
-- Casaba Melon Tank
INSERT INTO `item_latents` VALUES (16251, 71, 3, 52, 1);
-- Fourth Division Mantle
-- INSERT INTO `item_latents` VALUES (11545, 368, 1, ?, ?); -- Campaign Regain 1 TP/ 3 Tic
-- Saviesa Pearl
-- INSERT INTO `item_latents` VALUES (11045, 30, 5, ?, ?); -- Legion
-- Puissant Pearl
-- INSERT INTO `item_latents` VALUES (11050, 161, -2, ?, ?); -- Legion
-- Ousek Pearl
-- INSERT INTO `item_latents` VALUES (11046, 289, 10, ?, ?); -- Legion
-- Myrddin Pearl
-- INSERT INTO `item_latents` VALUES (11049, 71, 10, ?, ?); -- Legion
-- Cytherea Pearl
-- INSERT INTO `item_latents` VALUES (11048, 27, -20, ?, ?); -- Legion
-- Corybant Pearl
-- INSERT INTO `item_latents` VALUES (11044, 375, 10, ?, ?); -- Legion
-- Belatz Pearl
-- INSERT INTO `item_latents` VALUES (11047, 384, 21, ?, ?); -- Legion
-- Flock Earring
-- INSERT INTO `item_latents` VALUES (11727, 68, ?, ?, ?); -- Bonus to evasion latent ?
-- Sigma Earring
INSERT INTO `item_latents` VALUES (15985, 385, 10, 23, 73); 
INSERT INTO `item_latents` VALUES (15985, 385, 10, 23, 74); 
INSERT INTO `item_latents` VALUES (15985, 385, 10, 23, 75); 
INSERT INTO `item_latents` VALUES (15985, 385, 10, 23, 76);
INSERT INTO `item_latents` VALUES (15985, 433, 10, 23, 73);
INSERT INTO `item_latents` VALUES (15985, 433, 10, 23, 74); 
INSERT INTO `item_latents` VALUES (15985, 433, 10, 23, 75); 
INSERT INTO `item_latents` VALUES (15985, 433, 10, 23, 76);
-- Gamma Earring
INSERT INTO `item_latents` VALUES (15989, 371, 1, 23, 73); 
INSERT INTO `item_latents` VALUES (15989, 371, 1, 23, 74); 
INSERT INTO `item_latents` VALUES (15989, 371, 1, 23, 75); 
INSERT INTO `item_latents` VALUES (15989, 371, 1, 23, 76);
-- Eta Earring
INSERT INTO `item_latents` VALUES (15986, 362, 2, 23, 73);
INSERT INTO `item_latents` VALUES (15986, 362, 2, 23, 74); 
INSERT INTO `item_latents` VALUES (15986, 362, 2, 23, 75); 
INSERT INTO `item_latents` VALUES (15986, 362, 2, 23, 76);
-- Epsilon Earring
INSERT INTO `item_latents` VALUES (15987, 228, 2, 23, 73);
INSERT INTO `item_latents` VALUES (15987, 228, 2, 23, 74); 
INSERT INTO `item_latents` VALUES (15987, 228, 2, 23, 75); 
INSERT INTO `item_latents` VALUES (15987, 228, 2, 23, 76);
INSERT INTO `item_latents` VALUES (15987, 229, 2, 23, 73);
INSERT INTO `item_latents` VALUES (15987, 229, 2, 23, 74); 
INSERT INTO `item_latents` VALUES (15987, 229, 2, 23, 75); 
INSERT INTO `item_latents` VALUES (15987, 229, 2, 23, 76);
-- Colossuss Earring
INSERT INTO `item_latents` VALUES (16058, 161, -2, 52, 7);
-- Beta Earring
INSERT INTO `item_latents` VALUES (15987, 384, 10, 23, 73);
INSERT INTO `item_latents` VALUES (15987, 384, 10, 23, 74); 
INSERT INTO `item_latents` VALUES (15987, 384, 10, 23, 75); 
INSERT INTO `item_latents` VALUES (15987, 384, 10, 23, 76);
-- Aesir Ear Pendant
INSERT INTO `item_latents` VALUES (16057, 566, 6, 52, 8);
-- Lunette Ring
INSERT INTO `item_latents` VALUES (10766, 29, 7, 12, 2);
-- Maquette Ring
-- INSERT INTO `item_latents` VALUES (10788, 28, 5, ?, ?); -- Legion
-- INSERT INTO `item_latents` VALUES (10788, 369, 2, ?, ?); -- Legion
-- Veneficium Ring
-- INSERT INTO `item_latents` VALUES (10783, 30, 4, ?, ?); -- Legion
-- INSERT INTO `item_latents` VALUES (10783, 410, 2, ?, ?); -- Legion Occasionally quickens spellcasting 2%
-- Ambuscade Ring
-- INSERT INTO `item_latents` VALUES (10782, 25, 8, ?, ?); -- Legion
-- INSERT INTO `item_latents` VALUES (10782, 288, 2, ?, ?); -- Legion
-- Oneiros Ring
INSERT INTO `item_latents` VALUES (11671, 302, 2, 44, 100);
-- Flock Ring
-- INSERT INTO `item_latents` VALUES (11676, 26, ?, ?, ?); -- Bonus RAcc ? unknown latent
-- Rollers Ring
-- INSERT INTO `item_latents` VALUES (11667, 368, 1, ?, ?); -- Roll of 11
-- INSERT INTO `item_latents` VALUES (11667, 369, 1, ?, ?); -- Roll of 11 
-- Miseria Ring
INSERT INTO `item_latents` VALUES (11652, 13, 6, 5, 51);
INSERT INTO `item_latents` VALUES (11652, 30, 3, 5, 51);
-- Zodiac Ring
INSERT INTO `item_latents` VALUES (15858, 32, 5, 28, 0);
INSERT INTO `item_latents` VALUES (15858, 33, 5, 34, 0);
INSERT INTO `item_latents` VALUES (15858, 34, 5, 31, 0);
INSERT INTO `item_latents` VALUES (15858, 35, 5, 29, 0);
INSERT INTO `item_latents` VALUES (15858, 36, 5, 35, 0);
INSERT INTO `item_latents` VALUES (15858, 37, 5, 30, 0);
INSERT INTO `item_latents` VALUES (15858, 38, 5, 36, 0);
INSERT INTO `item_latents` VALUES (15858, 39, 5, 32, 0);
-- Tau Ring
INSERT INTO `item_latents` VALUES (15795, 288, 2, 23, 73);
INSERT INTO `item_latents` VALUES (15795, 288, 2, 23, 74); 
INSERT INTO `item_latents` VALUES (15795, 288, 2, 23, 75); 
INSERT INTO `item_latents` VALUES (15795, 288, 2, 23, 76);
-- Shadow Ring
INSERT INTO `item_latents` VALUES (14646, 29, 10, 32, 0);
-- Psi Ring
INSERT INTO `item_latents` VALUES (15796, 73, 5, 23, 73);
INSERT INTO `item_latents` VALUES (15796, 73, 5, 23, 74); 
INSERT INTO `item_latents` VALUES (15796, 73, 5, 23, 75); 
INSERT INTO `item_latents` VALUES (15796, 73, 5, 23, 76);
-- Pi Ring
INSERT INTO `item_latents` VALUES (15797, 170, 2, 23, 73);
INSERT INTO `item_latents` VALUES (15797, 170, 2, 23, 74); 
INSERT INTO `item_latents` VALUES (15797, 170, 2, 23, 75); 
INSERT INTO `item_latents` VALUES (15797, 170, 2, 23, 76);
-- Omicron Ring
INSERT INTO `item_latents` VALUES (15794, 291, 2, 23, 73);
INSERT INTO `item_latents` VALUES (15794, 291, 2, 23, 74); 
INSERT INTO `item_latents` VALUES (15794, 291, 2, 23, 75); 
INSERT INTO `item_latents` VALUES (15794, 291, 2, 23, 76);
-- Epsilon Ring
INSERT INTO `item_latents` VALUES (15798, 30, 3, 23, 73);
INSERT INTO `item_latents` VALUES (15798, 30, 3, 23, 74); 
INSERT INTO `item_latents` VALUES (15798, 30, 3, 23, 75); 
INSERT INTO `item_latents` VALUES (15798, 30, 3, 23, 76);
INSERT INTO `item_latents` VALUES (15798, 121, 3, 23, 73);
INSERT INTO `item_latents` VALUES (15798, 121, 3, 23, 74); 
INSERT INTO `item_latents` VALUES (15798, 121, 3, 23, 75); 
INSERT INTO `item_latents` VALUES (15798, 121, 3, 23, 76);
-- Volunteers Ring
-- INSERT INTO `item_latents` VALUES (15791, 71, 5, ?, ?); -- Besiged
-- Mercenarys Ring
-- INSERT INTO `item_latents` VALUES (15792, 161, -5, ?, ?); -- Besiged
-- Skrimishers Ring
-- INSERT INTO `item_latents` VALUES (15829, 3, 5, ?, ?); -- Campaign
-- Patriots Ring
-- INSERT INTO `item_latents` VALUES (15830, 6, 4, ?, ?); -- Campaign
-- Atlauas Ring
-- INSERT INTO `item_latents` VALUES (14658, ?, 4, ?, ?); -- Charm +4 Vs Amorphs
-- INSERT INTO `item_latents` VALUES (14658, ?, 4, ?, ?); -- Charm +4 Vs Aquans
-- Wind Ring
INSERT INTO `item_latents` VALUES (13562, 3, -15, 31, 0);
INSERT INTO `item_latents` VALUES (13562, 68, 10, 31, 0);
-- Water Ring
INSERT INTO `item_latents` VALUES (13565, 6, -15, 30, 0);
INSERT INTO `item_latents` VALUES (13565, 296, 15, 30, 0);
-- Lightning Ring
INSERT INTO `item_latents` VALUES (13564, 3, -15, 35, 0);
INSERT INTO `item_latents` VALUES (13564, 25, 15, 35, 0);
INSERT INTO `item_latents` VALUES (13564, 26, 15, 35, 0);
-- Ladybug Ring +1
INSERT INTO `item_latents` VALUES (15816, 2, 22, 26, 0);
INSERT INTO `item_latents` VALUES (15816, 5, 22, 26, 0);
-- Ladybug Ring
INSERT INTO `item_latents` VALUES (15815, 2, 20, 26, 0);
INSERT INTO `item_latents` VALUES (15815, 5, 20, 26, 0);
-- Ice Ring
INSERT INTO `item_latents` VALUES (13561, 6, -15, 30, 0);
INSERT INTO `item_latents` VALUES (13561, 115, 15, 30, 0);
-- Fire Ring
INSERT INTO `item_latents` VALUES (13560, 3, -15, 28, 0);
INSERT INTO `item_latents` VALUES (13560, 23, 15, 28, 0);
INSERT INTO `item_latents` VALUES (13560, 24, 15, 28, 0);
-- Earth Ring
INSERT INTO `item_latents` VALUES (13563, 3, -15, 29, 0);
INSERT INTO `item_latents` VALUES (13563, 1, 15, 29, 0);
-- Diaboloss Ring
INSERT INTO `item_latents` VALUES (15557, 6, -15, 32, 0);
INSERT INTO `item_latents` VALUES (15557, 116, 15, 32, 0);
-- Patronus Ring
-- INSERT INTO `item_latents` VALUES (15844, 161, -10, ?, ?); -- Campaign
-- Patriarch Protectors Ring
INSERT INTO `item_latents` VALUES (13559, 2, 6, 53, 0);
INSERT INTO `item_latents` VALUES (13559, 8, 2, 53, 0);
INSERT INTO `item_latents` VALUES (13559, 12, 3, 53, 0); 
-- Grand Knights Ring
INSERT INTO `item_latents` VALUES (13557, 5, 6, 53, 0);
INSERT INTO `item_latents` VALUES (13557, 9, 2, 53, 0);
INSERT INTO `item_latents` VALUES (13557, 13, 3, 53, 0); 
-- Gold Musketeers Ring
INSERT INTO `item_latents` VALUES (13558, 1, 4, 53, 0);
INSERT INTO `item_latents` VALUES (13558, 10, 3, 53, 0);
INSERT INTO `item_latents` VALUES (13558, 11, 2, 53, 0); 
-- Royal Knights Sigil Ring
-- INSERT INTO `item_latents` VALUES (11636, 370, 1, ?, ?); -- Campaign 
-- Ulthalams Ring
-- INSERT INTO `item_latents` VALUES (15808, 8, 4, ?, ?); -- Assault
-- INSERT INTO `item_latents` VALUES (15808, 9, 4, ?, ?); -- Assault
-- INSERT INTO `item_latents` VALUES (15808, 370, 1, ?, ?); -- Assault
INSERT INTO `item_latents` VALUES (15808, 8, 4, 23, 73);
INSERT INTO `item_latents` VALUES (15808, 8, 4, 23, 74); 
INSERT INTO `item_latents` VALUES (15808, 8, 4, 23, 75); 
INSERT INTO `item_latents` VALUES (15808, 8, 4, 23, 76);
INSERT INTO `item_latents` VALUES (15808, 9, 4, 23, 73);
INSERT INTO `item_latents` VALUES (15808, 9, 4, 23, 74); 
INSERT INTO `item_latents` VALUES (15808, 9, 4, 23, 75); 
INSERT INTO `item_latents` VALUES (15808, 9, 4, 23, 76);
INSERT INTO `item_latents` VALUES (15808, 370, 1, 23, 73);
INSERT INTO `item_latents` VALUES (15808, 370, 1, 23, 74); 
INSERT INTO `item_latents` VALUES (15808, 370, 1, 23, 75); 
INSERT INTO `item_latents` VALUES (15808, 370, 1, 23, 76);
-- Troopers Ring
-- INSERT INTO `item_latents` VALUES (15560, 10, 10, ?, ?); -- Besieged
-- Storm Ring
-- INSERT INTO `item_latents` VALUES (15774, 25, 10, ?, ?); -- Assault
-- Jalzahns Ring
-- INSERT INTO `item_latents` VALUES (15809, 11, 6, ?, ?); -- Assault
-- INSERT INTO `item_latents` VALUES (15809, 365, 5, ?, ?); -- Assault
-- Imperial Ring
-- INSERT INTO `item_latents` VALUES (15773, 384, 40, ?, ?); -- Assault
-- Balrahns Ring
-- INSERT INTO `item_latents` VALUES (15773, 370, 1, ?, ?); -- Assault
-- INSERT INTO `item_latents` VALUES (15773, 13, 4, ?, ?); -- Assault
-- INSERT INTO `item_latents` VALUES (15773, 14, 4, ?, ?); -- Assault
-- INSERT INTO `item_latents` VALUES (15773, 12, 4, ?, ?); -- Assault
-- INSERT INTO `item_latents` VALUES (15773, 30, 4, ?, ?); -- Assault
-- Multiple Ring
INSERT INTO `item_latents` VALUES (15790, 2, 50, 39, 0);
INSERT INTO `item_latents` VALUES (15790, 5, 20, 39, 0);
-- Desperado Ring
INSERT INTO `item_latents` VALUES (15835, 23, 5, 5, 5);
-- Ossa Grip
-- INSERT INTO `item_latents` VALUES (18812, ?, 2, 13, 35); -- Ice Elemental MAB +2
-- Oneiros Grip
INSERT INTO `item_latents` VALUES (18811, 369, 1, 4, 76);
-- Cadushi Grip
INSERT INTO `item_latents` VALUES (18810, 2, 25, 26, 1);
-- Rose Grip
-- INSERT INTO `item_latents` VALUES (19041, 288, 20, ?, ?); -- Campaign
-- Ariesan Grip
-- INSERT INTO `item_latents` VALUES (19042, 71, 20, ?, ?); -- Campaign
-- Sword Strap
-- INSERT INTO `item_latents` VALUES (19024, ?, ?, ?, ?); -- 2 Handed weapon equipped 2 handed delay -3%
-- Koga Kyahan +1
INSERT INTO `item_latents` VALUES (15677, 9, 7, 26, 2);
-- Koga Kyahan
INSERT INTO `item_latents` VALUES (15144, 9, 7, 26, 1);
-- Argute Loafers +2
-- INSERT INTO `item_latents` VALUES (10749, ?, ?, 52, 1); -- Enhances Celerity - Weather
-- INSERT INTO `item_latents` VALUES (10749, ?, ?, 52, 2);
-- INSERT INTO `item_latents` VALUES (10749, ?, ?, 52, 3);
-- INSERT INTO `item_latents` VALUES (10749, ?, ?, 52, 4);
-- INSERT INTO `item_latents` VALUES (10749, ?, ?, 52, 5);
-- INSERT INTO `item_latents` VALUES (10749, ?, ?, 52, 6);
-- INSERT INTO `item_latents` VALUES (10749, ?, ?, 52, 7);
-- INSERT INTO `item_latents` VALUES (10749, ?, ?, 52, 8);
-- INSERT INTO `item_latents` VALUES (10749, ?, ?, 52, 1); -- Enhances Alacrity - Weather
-- INSERT INTO `item_latents` VALUES (10749, ?, ?, 52, 2);
-- INSERT INTO `item_latents` VALUES (10749, ?, ?, 52, 3);
-- INSERT INTO `item_latents` VALUES (10749, ?, ?, 52, 4);
-- INSERT INTO `item_latents` VALUES (10749, ?, ?, 52, 5);
-- INSERT INTO `item_latents` VALUES (10749, ?, ?, 52, 6);
-- INSERT INTO `item_latents` VALUES (10749, ?, ?, 52, 7);
-- INSERT INTO `item_latents` VALUES (10749, ?, ?, 52, 8);
-- Argute Loafers +1
-- INSERT INTO `item_latents` VALUES (11399, ?, ?, 52, 1); -- Enhances Celerity - Weather
-- INSERT INTO `item_latents` VALUES (11399, ?, ?, 52, 2);
-- INSERT INTO `item_latents` VALUES (11399, ?, ?, 52, 3);
-- INSERT INTO `item_latents` VALUES (11399, ?, ?, 52, 4);
-- INSERT INTO `item_latents` VALUES (11399, ?, ?, 52, 5);
-- INSERT INTO `item_latents` VALUES (11399, ?, ?, 52, 6);
-- INSERT INTO `item_latents` VALUES (11399, ?, ?, 52, 7);
-- INSERT INTO `item_latents` VALUES (11399, ?, ?, 52, 8);
-- INSERT INTO `item_latents` VALUES (11399, ?, ?, 52, 1); -- Enhances Alacrity - Weather
-- INSERT INTO `item_latents` VALUES (11399, ?, ?, 52, 2);
-- INSERT INTO `item_latents` VALUES (11399, ?, ?, 52, 3);
-- INSERT INTO `item_latents` VALUES (11399, ?, ?, 52, 4);
-- INSERT INTO `item_latents` VALUES (11399, ?, ?, 52, 5);
-- INSERT INTO `item_latents` VALUES (11399, ?, ?, 52, 6);
-- INSERT INTO `item_latents` VALUES (11399, ?, ?, 52, 7);
-- INSERT INTO `item_latents` VALUES (11399, ?, ?, 52, 8);
-- Argute Loafers
-- INSERT INTO `item_latents` VALUES (11398, ?, ?, 52, 1); -- Enhances Celerity - Weather
-- INSERT INTO `item_latents` VALUES (11398, ?, ?, 52, 2);
-- INSERT INTO `item_latents` VALUES (11398, ?, ?, 52, 3);
-- INSERT INTO `item_latents` VALUES (11398, ?, ?, 52, 4);
-- INSERT INTO `item_latents` VALUES (11398, ?, ?, 52, 5);
-- INSERT INTO `item_latents` VALUES (11398, ?, ?, 52, 6);
-- INSERT INTO `item_latents` VALUES (11398, ?, ?, 52, 7);
-- INSERT INTO `item_latents` VALUES (11398, ?, ?, 52, 8);
-- INSERT INTO `item_latents` VALUES (11398, ?, ?, 52, 1); -- Enhances Alacrity - Weather
-- INSERT INTO `item_latents` VALUES (11398, ?, ?, 52, 2);
-- INSERT INTO `item_latents` VALUES (11398, ?, ?, 52, 3);
-- INSERT INTO `item_latents` VALUES (11398, ?, ?, 52, 4);
-- INSERT INTO `item_latents` VALUES (11398, ?, ?, 52, 5);
-- INSERT INTO `item_latents` VALUES (11398, ?, ?, 52, 6);
-- INSERT INTO `item_latents` VALUES (11398, ?, ?, 52, 7);
-- INSERT INTO `item_latents` VALUES (11398, ?, ?, 52, 8);
-- Frisky Sabots
-- INSERT INTO `item_latents` VALUES (11444, ?, ?, ?, ?); -- Flee when you take dmg 2% ?
-- Serpentes Sabots
INSERT INTO `item_latents` VALUES (14085, 369, 1, 26, 0);
INSERT INTO `item_latents` VALUES (14085, 370, 1, 26, 1);
-- Tantra Gaiters +2
INSERT INTO `item_latents` VALUES (11145, 62, 3, 13, 406);
-- Tantra Gaiters +1
INSERT INTO `item_latents` VALUES (11245, 62, 5, 13, 406);
-- Posie Shoes
INSERT INTO `item_latents` VALUES (11418, 384, 51, 13, 406);
-- Sneaking Boots
-- INSERT INTO `item_latents` VALUES (11418, 68, 8, ?, ?); -- Besieged
-- Storm Crackows
-- INSERT INTO `item_latents` VALUES (15692, 169, 12, ?, ?); -- Assault
-- Kyoshu Kyahan
INSERT INTO `item_latents` VALUES (11405, 23, 20, 13, 406);
INSERT INTO `item_latents` VALUES (11405, 25, 20, 13, 406);
-- Galvanic Slops
-- INSERT INTO `item_latents` VALUES (11962, 342, 7, ?, ?); -- Shock Spikes on casting
-- INSERT INTO `item_latents` VALUES (11962, 344, 7, ?, ?); --
-- Sorcerers Tonban +1
INSERT INTO `item_latents` VALUES (15583, 32, 5, 28, 0);
INSERT INTO `item_latents` VALUES (15583, 33, 5, 34, 0);
INSERT INTO `item_latents` VALUES (15583, 34, 5, 31, 0);
INSERT INTO `item_latents` VALUES (15583, 35, 5, 29, 0);
INSERT INTO `item_latents` VALUES (15583, 36, 5, 35, 0);
INSERT INTO `item_latents` VALUES (15583, 37, 5, 30, 0);
INSERT INTO `item_latents` VALUES (15583, 38, 5, 36, 0);
INSERT INTO `item_latents` VALUES (15583, 39, 5, 32, 0);
-- Sorcerers Tonban
INSERT INTO `item_latents` VALUES (15120, 32, 5, 28, 0);
INSERT INTO `item_latents` VALUES (15120, 33, 5, 34, 0);
INSERT INTO `item_latents` VALUES (15120, 34, 5, 31, 0);
INSERT INTO `item_latents` VALUES (15120, 35, 5, 29, 0);
INSERT INTO `item_latents` VALUES (15120, 36, 5, 35, 0);
INSERT INTO `item_latents` VALUES (15120, 37, 5, 30, 0);
INSERT INTO `item_latents` VALUES (15120, 38, 5, 36, 0);
INSERT INTO `item_latents` VALUES (15120, 39, 5, 32, 0);
-- Koga Hakama +1
INSERT INTO `item_latents` VALUES (15120, 68, 10, 26, 2);
-- Koga Hakama
INSERT INTO `item_latents` VALUES (15129, 68, 10, 26, 1);
-- Koga Hakama +1
INSERT INTO `item_latents` VALUES (15592, 68, 12, 26, 2);
-- Bards Cannions +2
INSERT INTO `item_latents` VALUES (10719, 12, 8, 11, 0); -- Courtly Measure
INSERT INTO `item_latents` VALUES (10719, 13, 8, 11, 0);
INSERT INTO `item_latents` VALUES (10719, 14, 8, 11, 0);
-- Unkai Haidate +2
INSERT INTO `item_latents` VALUES (11135, 383, 26, 13, 353);
-- Unkai Haidate +1
INSERT INTO `item_latents` VALUES (11235, 383, 15, 13, 353);
-- Saotome Haidate +2
INSERT INTO `item_latents` VALUES (10721, 289, 25, 13, 67);
-- Saotome Haidate +1
INSERT INTO `item_latents` VALUES (15591, 289, 15, 13, 67);
-- Saotome Haidate
INSERT INTO `item_latents` VALUES (15128, 289, 10, 13, 67);
-- Iga Hakama +2
INSERT INTO `item_latents` VALUES (11136, 289, 15, 13, 420);
-- Iga Hakama +1
INSERT INTO `item_latents` VALUES (11236, 289, 10, 13, 420);
-- Armadillo Cuisses
INSERT INTO `item_latents` VALUES (16340, 23, 15, 13, 16);
INSERT INTO `item_latents` VALUES (16340, 25, 15, 13, 16);
-- Clown Subligar +1
INSERT INTO `item_latents` VALUES (14289, 4, 25, 26, 0);
INSERT INTO `item_latents` VALUES (14289, 7, 25, 26, 1);
-- Clown Subligar
INSERT INTO `item_latents` VALUES (14288, 4, 20, 26, 0);
INSERT INTO `item_latents` VALUES (14288, 7, 20, 26, 1);
-- Malagigis Trousers
-- INSERT INTO `item_latents` VALUES (16354, 170, 10, ?, ?); -- Campaign
-- Luna Subligar
INSERT INTO `item_latents` VALUES (14287, 11, 2, 65, 0);
INSERT INTO `item_latents` VALUES (14287, 14, 7, 65, 0);
INSERT INTO `item_latents` VALUES (14287, 9, 2, 65, 0);
-- Royal Squire Breeches +2
INSERT INTO `item_latents` VALUES (14262, 2, 16, 53, 0);
-- Royal Squire Breeches +1
INSERT INTO `item_latents` VALUES (14261, 2, 14, 53, 0);
-- Combat Casters Slacks +2
INSERT INTO `item_latents` VALUES (14276, 5, 16, 53, 0);
-- Combat Casters Slacks +1
INSERT INTO `item_latents` VALUES (14275, 5, 14, 53, 0);
-- Woodsy Shorts +1
-- INSERT INTO `item_latents` VALUES (10342, 5, 20, ?, ?); -- Sunny Weather
-- Woodsy Boxers +1
-- INSERT INTO `item_latents` VALUES (10341, 5, 20, ?, ?); -- Sunny Weather
-- Wonder Trunks +1
-- INSERT INTO `item_latents` VALUES (16333, 2, 20, ?, ?); -- Sunny Weather
-- Wonder Shorts +1
-- INSERT INTO `item_latents` VALUES (16334, 2, 20, ?, ?); -- Sunny Weather
-- Savage Shorts +1
-- INSERT INTO `item_latents` VALUES (16335, 2, 20, ?, ?); -- Sunny Weather
-- River Shorts +1
-- INSERT INTO `item_latents` VALUES (10344, 5, 20, ?, ?); -- Sunny Weather
-- Marine Shorts +1
-- INSERT INTO `item_latents` VALUES (10339, 5, 20, ?, ?); -- Sunny Weather
-- Marine Boxers +1
-- INSERT INTO `item_latents` VALUES (10338, 5, 20, ?, ?); -- Sunny Weather
-- Magna Trunks +1
-- INSERT INTO `item_latents` VALUES (16331, 2, 20, ?, ?); -- Sunny Weather
-- Magna Shorts +1
-- INSERT INTO `item_latents` VALUES (16332, 2, 20, ?, ?); -- Sunny Weather
-- Elder Trunks +1
-- INSERT INTO `item_latents` VALUES (16336, 2, 20, ?, ?); -- Sunny Weather
-- Dune Boxers +1
-- INSERT INTO `item_latents` VALUES (10345, 5, 20, ?, ?); -- Sunny Weather
-- Diner Hose
INSERT INTO `item_latents` VALUES (16378, 14, 1, 26, 1);
INSERT INTO `item_latents` VALUES (16378, 27, -1, 0, 75);
-- Mercenarys Trousers
-- INSERT INTO `item_latents` VALUES (15622, 30, 10, ?, ?); -- Besieged
-- Mekira Meikogai
-- INSERT INTO `item_latents` VALUES (11868, 288, 3, ?, ?); -- Sphere
-- Tocis Harness
-- INSERT INTO `item_latents` VALUES (11866, 302, 3, ?, ?); -- Sphere
-- Hekas Kalasiris
-- INSERT INTO `item_latents` VALUES (11867, 170, ?, ?, ?); -- Sphere
-- Mextli Harness
-- INSERT INTO `item_latents` VALUES (11855, 165, 3, ?, ?); -- Sphere
-- Fazheluo Radiant Mail
-- INSERT INTO `item_latents` VALUES (11857, 73, 6, ?, ?); -- Sphere
-- Bard Justacorps +2
INSERT INTO `item_latents` VALUES (10679, 8, 10, 10, 0);
INSERT INTO `item_latents` VALUES (10679, 9, 10, 10, 0);
INSERT INTO `item_latents` VALUES (10679, 10, 10, 10, 0);
INSERT INTO `item_latents` VALUES (10679, 11, 10, 10, 0);
-- Anhur Robe
-- INSERT INTO `item_latents` VALUES (11856, 28, 5, ?, ?); -- Sphere
-- Sylvan Caban +2
INSERT INTO `item_latents` VALUES (11094, 172, -20, 13, 371);
-- Sylvan Caban +1
INSERT INTO `item_latents` VALUES (11194, 172, -20, 13, 371);
-- Volunteers Nails
-- INSERT INTO `item_latents` VALUES (15710, 391, 4, ?, ?); -- Besieged
-- Capricornia Rope
-- INSERT INTO `item_latents` VALUES (15935, 398, 15, ?, ?); -- Campaign
-- Minstrels Ring
INSERT INTO `item_latents` VALUES (13295, 399, 25, 0, 75);
-- Chimeric Fleuret
INSERT INTO `item_latents` VALUES (18895, 288, 4, 13, 94);
INSERT INTO `item_latents` VALUES (18895, 288, 4, 13, 95);
INSERT INTO `item_latents` VALUES (18895, 288, 4, 13, 96);
INSERT INTO `item_latents` VALUES (18895, 288, 4, 13, 97);
INSERT INTO `item_latents` VALUES (18895, 288, 4, 13, 98);
INSERT INTO `item_latents` VALUES (18895, 288, 4, 13, 99);
INSERT INTO `item_latents` VALUES (18895, 288, 4, 13, 277);
INSERT INTO `item_latents` VALUES (18895, 288, 4, 13, 278);
INSERT INTO `item_latents` VALUES (18895, 288, 4, 13, 279);
INSERT INTO `item_latents` VALUES (18895, 288, 4, 13, 280);
INSERT INTO `item_latents` VALUES (18895, 288, 4, 13, 281);
INSERT INTO `item_latents` VALUES (18895, 288, 4, 13, 282);
-- Fencers Ring
INSERT INTO `item_latents` VALUES (13290, 343, 5, 2, 75);
-- Honorbound
INSERT INTO `item_latents` VALUES (18896, 343, 10, 13, 274);
-- Lamia Garland
INSERT INTO `item_latents` VALUES (16123, 14, 7, 23, 54);
INSERT INTO `item_latents` VALUES (16123, 14, 7, 23, 62);
INSERT INTO `item_latents` VALUES (16123, 14, 7, 23, 65);
INSERT INTO `item_latents` VALUES (16123, 252, 2, 23, 54);
INSERT INTO `item_latents` VALUES (16123, 252, 2, 23, 62);
INSERT INTO `item_latents` VALUES (16123, 252, 2, 23, 65);
-- Lamiabane
INSERT INTO `item_latents` VALUES (18693, 28, 2, 23, 54);
INSERT INTO `item_latents` VALUES (18693, 28, 2, 23, 62);
INSERT INTO `item_latents` VALUES (18693, 28, 2, 23, 65);
-- Mamool Ja Helm
INSERT INTO `item_latents` VALUES (16121, 8, 4, 23, 54);
INSERT INTO `item_latents` VALUES (16121, 8, 4, 23, 62);
INSERT INTO `item_latents` VALUES (16121, 8, 4, 23, 65);
INSERT INTO `item_latents` VALUES (16121, 56, 15, 23, 54);
INSERT INTO `item_latents` VALUES (16121, 56, 15, 23, 62);
INSERT INTO `item_latents` VALUES (16121, 56, 15, 23, 65);
INSERT INTO `item_latents` VALUES (16121, 60, 15, 23, 54);
INSERT INTO `item_latents` VALUES (16121, 60, 15, 23, 62);
INSERT INTO `item_latents` VALUES (16121, 60, 15, 23, 65);
-- Mamoolbane
INSERT INTO `item_latents` VALUES (18692, 25, 6, 23, 54);
INSERT INTO `item_latents` VALUES (18692, 25, 6, 23, 62);
INSERT INTO `item_latents` VALUES (18692, 25, 6, 23, 65);
-- Qiqirn Hood
INSERT INTO `item_latents` VALUES (16124, 26, 2, 23, 54);
INSERT INTO `item_latents` VALUES (16124, 26, 2, 23, 62);
INSERT INTO `item_latents` VALUES (16124, 26, 2, 23, 65);
INSERT INTO `item_latents` VALUES (16124, 24, 2, 23, 54);
INSERT INTO `item_latents` VALUES (16124, 24, 2, 23, 62);
INSERT INTO `item_latents` VALUES (16124, 24, 2, 23, 65);
INSERT INTO `item_latents` VALUES (16124, 68, 2, 23, 54);
INSERT INTO `item_latents` VALUES (16124, 68, 2, 23, 62);
INSERT INTO `item_latents` VALUES (16124, 68, 2, 23, 65);
-- Troll Coif
INSERT INTO `item_latents` VALUES (16122, 2, 32, 23, 54);
INSERT INTO `item_latents` VALUES (16122, 2, 32, 23, 62);
INSERT INTO `item_latents` VALUES (16122, 2, 32, 23, 65);
INSERT INTO `item_latents` VALUES (16122, 10, 5, 23, 54);
INSERT INTO `item_latents` VALUES (16122, 10, 5, 23, 62);
INSERT INTO `item_latents` VALUES (16122, 10, 5, 23, 65);
INSERT INTO `item_latents` VALUES (16122, 59, -50, 23, 54);
INSERT INTO `item_latents` VALUES (16122, 59, -50, 23, 62);
INSERT INTO `item_latents` VALUES (16122, 59, -50, 23, 65);
-- Trollbane
INSERT INTO `item_latents` VALUES (18694, 165, 5, 23, 54);
INSERT INTO `item_latents` VALUES (18694, 165, 5, 23, 62);
INSERT INTO `item_latents` VALUES (18694, 165, 5, 23, 65);
-- Sylphid Epee
INSERT INTO `item_latents` VALUES (17754, 23, 10, 31, 0);
INSERT INTO `item_latents` VALUES (17754, 23, -10, 29, 0);
-- Iga Tekko +1
INSERT INTO `item_latents` VALUES (11216, 420, 10, 13, 441);
-- Iga Tekko +2
INSERT INTO `item_latents` VALUES (11116, 420, 15, 13, 441);
-- Dinner Jacket
INSERT INTO `item_latents` VALUES (11355, 14, 1, 26, 1);
INSERT INTO `item_latents` VALUES (11355, 27, -1, 0, 75);
-- Mythril Musketeer Livery
INSERT INTO `item_latents` VALUES (11357, 423, 12, 53, 3);
INSERT INTO `item_latents` VALUES (11357, 424, 12, 53, 3);
INSERT INTO `item_latents` VALUES (11357, 169, 12, 53, 3);
-- Republic Aketon
INSERT INTO `item_latents` VALUES (14429, 169, 12, 23, 234);
INSERT INTO `item_latents` VALUES (14429, 169, 12, 23, 235);
INSERT INTO `item_latents` VALUES (14429, 169, 12, 23, 236);
INSERT INTO `item_latents` VALUES (14429, 169, 12, 23, 237);
-- Federation Aketon
INSERT INTO `item_latents` VALUES (14430, 169, 12, 23, 238);
INSERT INTO `item_latents` VALUES (14430, 169, 12, 23, 239);
INSERT INTO `item_latents` VALUES (14430, 169, 12, 23, 240);
INSERT INTO `item_latents` VALUES (14430, 169, 12, 23, 241);
INSERT INTO `item_latents` VALUES (14430, 169, 12, 23, 242);
-- Kingdom Aketon
INSERT INTO `item_latents` VALUES (14428, 169, 12, 23, 230);
INSERT INTO `item_latents` VALUES (14428, 169, 12, 23, 231);
INSERT INTO `item_latents` VALUES (14428, 169, 12, 23, 232);
INSERT INTO `item_latents` VALUES (14428, 169, 12, 23, 233);
-- Royal Guard Livery
INSERT INTO `item_latents` VALUES (11356, 423, 12, 53, 2);
INSERT INTO `item_latents` VALUES (11356, 424, 12, 53, 2);
INSERT INTO `item_latents` VALUES (11356, 169, 12, 53, 2);
-- Patriarch Protector Liver
INSERT INTO `item_latents` VALUES (11358, 423, 12, 53, 4);
INSERT INTO `item_latents` VALUES (11358, 424, 12, 53, 4);
INSERT INTO `item_latents` VALUES (11358, 169, 12, 53, 4);
-- Chocobo Shirt
INSERT INTO `item_latents` VALUES (10293, 25, 50, 50, 31);
INSERT INTO `item_latents` VALUES (10293, 26, 50, 50, 31);
INSERT INTO `item_latents` VALUES (10293, 30, 50, 50, 31);
-- INSERT INTO `item_latents` VALUES (10293, ?, ?, ?, ?); -- Initiate and below Reduces synthesis material loss
-- Coalition Shield
-- INSERT INTO `item_latents` VALUES (28666, 160, -10, ?, ?); -- Reives
-- INSERT INTO `item_latents` VALUES (28666, 1, 40, ?, ?); -- Reives
-- Athos's Gloves
INSERT INTO `item_latents` VALUES (10501, 32, 10, 28, 0);
INSERT INTO `item_latents` VALUES (10501, 33, 10, 34, 0);
INSERT INTO `item_latents` VALUES (10501, 34, 10, 31, 0);
INSERT INTO `item_latents` VALUES (10501, 35, 10, 29, 0);
INSERT INTO `item_latents` VALUES (10501, 36, 10, 35, 0);
INSERT INTO `item_latents` VALUES (10501, 37, 10, 30, 0);
INSERT INTO `item_latents` VALUES (10501, 38, 10, 36, 0);
INSERT INTO `item_latents` VALUES (10501, 39, 10, 32, 0);
-- Koga Tekko +1
INSERT INTO `item_latents` VALUES (14921, 8, 13, 26, 2);
INSERT INTO `item_latents` VALUES (14921, 384, 41, 26, 2);
-- Koga Tekko
INSERT INTO `item_latents` VALUES (15114, 8, 12, 26, 1);
INSERT INTO `item_latents` VALUES (15114, 384, 41, 26, 1);
-- Caller's Bracers +2
-- INSERT INTO `item_latents` VALUES (11118, 371, ?, 28, 0); -- 1/2 Avatar Prepetuation
-- INSERT INTO `item_latents` VALUES (11118, 371, ?, 34, 0); -- Depends on Day / Weather
-- INSERT INTO `item_latents` VALUES (11118, 371, ?, 31, 0);
-- INSERT INTO `item_latents` VALUES (11118, 371, ?, 29, 0);
-- INSERT INTO `item_latents` VALUES (11118, 371, ?, 35, 0);
-- INSERT INTO `item_latents` VALUES (11118, 371, ?, 30, 0);
-- INSERT INTO `item_latents` VALUES (11118, 371, ?, 36, 0);
-- INSERT INTO `item_latents` VALUES (11118, 371, ?, 32, 0);
INSERT INTO `item_latents` VALUES (11118, 371, 5, 52, 1);
INSERT INTO `item_latents` VALUES (11118, 371, 5, 52, 5);
INSERT INTO `item_latents` VALUES (11118, 371, 5, 52, 4);
INSERT INTO `item_latents` VALUES (11118, 371, 5, 52, 2);
INSERT INTO `item_latents` VALUES (11118, 371, 5, 52, 6);
INSERT INTO `item_latents` VALUES (11118, 371, 5, 52, 3);
INSERT INTO `item_latents` VALUES (11118, 371, 5, 52, 7);
INSERT INTO `item_latents` VALUES (11118, 371, 5, 52, 8);
-- Caller's Bracers +1
-- INSERT INTO `item_latents` VALUES (11118, 371, ?, 28, 0); -- 1/2 Avatar Prepetuation
-- INSERT INTO `item_latents` VALUES (11118, 371, ?, 34, 0); -- Depends on Day
-- INSERT INTO `item_latents` VALUES (11118, 371, ?, 31, 0);
-- INSERT INTO `item_latents` VALUES (11118, 371, ?, 29, 0);
-- INSERT INTO `item_latents` VALUES (11118, 371, ?, 35, 0);
-- INSERT INTO `item_latents` VALUES (11118, 371, ?, 30, 0);
-- INSERT INTO `item_latents` VALUES (11118, 371, ?, 36, 0);
-- Serpentes Cuffs
INSERT INTO `item_latents` VALUES (15019, 369, 1, 26, 0);
INSERT INTO `item_latents` VALUES (15019, 370, 1, 26, 1);
-- Danzo Tekko
-- INSERT INTO `item_latents` VALUES (15033, 291, 4, ?, ?); -- Campaign
-- INSERT INTO `item_latents` VALUES (15033, 73, 5, ?, ?); -- Campaign
-- Trainee Gloves
INSERT INTO `item_latents` VALUES (15008, 133, 1, 24, 0);
-- Rune Arrow
INSERT INTO `item_latents` VALUES (17333, 10, 1, 12, 0);
INSERT INTO `item_latents` VALUES (17333, 26, 10, 12, 0);
INSERT INTO `item_latents` VALUES (17333, 370, -1, 12, 0);
-- Slayer's Ring
INSERT INTO `item_latents` VALUES (13293, 433, 10, 0, 75);
-- Murti Bow
-- INSERT INTO `item_latents` VALUES (19468, 355, 200, ?, ?); -- 13 Ranged Weaponskills get 1 use
-- Tamer's Ring
INSERT INTO `item_latents` VALUES (13294, 224, 2, 3, 75);
INSERT INTO `item_latents` VALUES (13294, 225, 2, 3, 75);
INSERT INTO `item_latents` VALUES (13294, 226, 2, 3, 75);
INSERT INTO `item_latents` VALUES (13294, 227, 2, 3, 75);
INSERT INTO `item_latents` VALUES (13294, 228, 2, 3, 75);
INSERT INTO `item_latents` VALUES (13294, 229, 2, 3, 75);
INSERT INTO `item_latents` VALUES (13294, 230, 3, 3, 75);
INSERT INTO `item_latents` VALUES (13294, 231, 2, 3, 75);
INSERT INTO `item_latents` VALUES (13294, 232, 2, 3, 75);
INSERT INTO `item_latents` VALUES (13294, 233, 2, 3, 75);
INSERT INTO `item_latents` VALUES (13294, 234, 2, 3, 75);
INSERT INTO `item_latents` VALUES (13294, 235, 2, 3, 75);
INSERT INTO `item_latents` VALUES (13294, 237, 2, 3, 75);
INSERT INTO `item_latents` VALUES (13294, 238, 2, 3, 75);
-- Coalition Bow
-- INSERT INTO `item_latents` VALUES (13294, 376, 23, ?, ?); -- Reives
-- Futatokoroto
INSERT INTO `item_latents` VALUES (18347, 355, 200, 48, 0);
-- Perdu Bow
INSERT INTO `item_latents` VALUES (18717, 366, 4, 7, 100);
INSERT INTO `item_latents` VALUES (18717, 26, 5, 7, 100);
INSERT INTO `item_latents` VALUES (18717, 24, 15, 7, 100);
-- Expunger
INSERT INTO `item_latents` VALUES (17207, 376, 13, 47, 0);
-- Imperial Kaman
INSERT INTO `item_latents` VALUES (18685, 32, 10, 28, 0);
INSERT INTO `item_latents` VALUES (18685, 33, 10, 34, 0);
INSERT INTO `item_latents` VALUES (18685, 34, 10, 31, 0);
INSERT INTO `item_latents` VALUES (18685, 35, 10, 29, 0);
INSERT INTO `item_latents` VALUES (18685, 36, 10, 35, 0);
INSERT INTO `item_latents` VALUES (18685, 37, 10, 30, 0);
INSERT INTO `item_latents` VALUES (18685, 38, 10, 36, 0);
INSERT INTO `item_latents` VALUES (18685, 39, 10, 32, 0);
-- Doombringer
INSERT INTO `item_latents` VALUES (18388, 32, 10, 28, 0);
INSERT INTO `item_latents` VALUES (18388, 33, 10, 34, 0);
INSERT INTO `item_latents` VALUES (18388, 34, 10, 31, 0);
INSERT INTO `item_latents` VALUES (18388, 35, 10, 29, 0);
INSERT INTO `item_latents` VALUES (18388, 36, 10, 35, 0);
INSERT INTO `item_latents` VALUES (18388, 37, 10, 30, 0);
INSERT INTO `item_latents` VALUES (18388, 38, 10, 36, 0);
INSERT INTO `item_latents` VALUES (18388, 39, 10, 32, 0);
-- Hotasrumaru
INSERT INTO `item_latents` VALUES (18435, 32, 10, 28, 0);
INSERT INTO `item_latents` VALUES (18435, 33, 10, 34, 0);
INSERT INTO `item_latents` VALUES (18435, 34, 10, 31, 0);
INSERT INTO `item_latents` VALUES (18435, 35, 10, 29, 0);
INSERT INTO `item_latents` VALUES (18435, 36, 10, 35, 0);
INSERT INTO `item_latents` VALUES (18435, 37, 10, 30, 0);
INSERT INTO `item_latents` VALUES (18435, 38, 10, 36, 0);
INSERT INTO `item_latents` VALUES (18435, 39, 10, 32, 0);
-- Imperial Bhuj
INSERT INTO `item_latents` VALUES (18485, 32, 10, 28, 0);
INSERT INTO `item_latents` VALUES (18485, 33, 10, 34, 0);
INSERT INTO `item_latents` VALUES (18485, 34, 10, 31, 0);
INSERT INTO `item_latents` VALUES (18485, 35, 10, 29, 0);
INSERT INTO `item_latents` VALUES (18485, 36, 10, 35, 0);
INSERT INTO `item_latents` VALUES (18485, 37, 10, 30, 0);
INSERT INTO `item_latents` VALUES (18485, 38, 10, 36, 0);
INSERT INTO `item_latents` VALUES (18485, 39, 10, 32, 0);
-- Imperial Gun
INSERT INTO `item_latents` VALUES (18686, 32, 10, 28, 0);
INSERT INTO `item_latents` VALUES (18686, 33, 10, 34, 0);
INSERT INTO `item_latents` VALUES (18686, 34, 10, 31, 0);
INSERT INTO `item_latents` VALUES (18686, 35, 10, 29, 0);
INSERT INTO `item_latents` VALUES (18686, 36, 10, 35, 0);
INSERT INTO `item_latents` VALUES (18686, 37, 10, 30, 0);
INSERT INTO `item_latents` VALUES (18686, 38, 10, 36, 0);
INSERT INTO `item_latents` VALUES (18686, 39, 10, 32, 0);
-- Imperial Neza
INSERT INTO `item_latents` VALUES (18113, 32, 10, 28, 0);
INSERT INTO `item_latents` VALUES (18113, 33, 10, 34, 0);
INSERT INTO `item_latents` VALUES (18113, 34, 10, 31, 0);
INSERT INTO `item_latents` VALUES (18113, 35, 10, 29, 0);
INSERT INTO `item_latents` VALUES (18113, 36, 10, 35, 0);
INSERT INTO `item_latents` VALUES (18113, 37, 10, 30, 0);
INSERT INTO `item_latents` VALUES (18113, 38, 10, 36, 0);
INSERT INTO `item_latents` VALUES (18113, 39, 10, 32, 0);
-- Imperial Pole
INSERT INTO `item_latents` VALUES (18583, 32, 10, 28, 0);
INSERT INTO `item_latents` VALUES (18583, 33, 10, 34, 0);
INSERT INTO `item_latents` VALUES (18583, 34, 10, 31, 0);
INSERT INTO `item_latents` VALUES (18583, 35, 10, 29, 0);
INSERT INTO `item_latents` VALUES (18583, 36, 10, 35, 0);
INSERT INTO `item_latents` VALUES (18583, 37, 10, 30, 0);
INSERT INTO `item_latents` VALUES (18583, 38, 10, 36, 0);
INSERT INTO `item_latents` VALUES (18583, 39, 10, 32, 0);
-- Khanjar
INSERT INTO `item_latents` VALUES (18025, 32, 10, 28, 0);
INSERT INTO `item_latents` VALUES (18025, 33, 10, 34, 0);
INSERT INTO `item_latents` VALUES (18025, 34, 10, 31, 0);
INSERT INTO `item_latents` VALUES (18025, 35, 10, 29, 0);
INSERT INTO `item_latents` VALUES (18025, 36, 10, 35, 0);
INSERT INTO `item_latents` VALUES (18025, 37, 10, 30, 0);
INSERT INTO `item_latents` VALUES (18025, 38, 10, 36, 0);
INSERT INTO `item_latents` VALUES (18025, 39, 10, 32, 0);
-- Pahluwan Patas
INSERT INTO `item_latents` VALUES (18365, 32, 10, 28, 0);
INSERT INTO `item_latents` VALUES (18365, 33, 10, 34, 0);
INSERT INTO `item_latents` VALUES (18365, 34, 10, 31, 0);
INSERT INTO `item_latents` VALUES (18365, 35, 10, 29, 0);
INSERT INTO `item_latents` VALUES (18365, 36, 10, 35, 0);
INSERT INTO `item_latents` VALUES (18365, 37, 10, 30, 0);
INSERT INTO `item_latents` VALUES (18365, 38, 10, 36, 0);
INSERT INTO `item_latents` VALUES (18365, 39, 10, 32, 0);
-- Sayosamonji
INSERT INTO `item_latents` VALUES (18417, 32, 10, 28, 0);
INSERT INTO `item_latents` VALUES (18417, 33, 10, 34, 0);
INSERT INTO `item_latents` VALUES (18417, 34, 10, 31, 0);
INSERT INTO `item_latents` VALUES (18417, 35, 10, 29, 0);
INSERT INTO `item_latents` VALUES (18417, 36, 10, 35, 0);
INSERT INTO `item_latents` VALUES (18417, 37, 10, 30, 0);
INSERT INTO `item_latents` VALUES (18417, 38, 10, 36, 0);
INSERT INTO `item_latents` VALUES (18417, 39, 10, 32, 0);
-- Storm Tabar
INSERT INTO `item_latents` VALUES (17951, 32, 10, 28, 0);
INSERT INTO `item_latents` VALUES (17951, 33, 10, 34, 0);
INSERT INTO `item_latents` VALUES (17951, 34, 10, 31, 0);
INSERT INTO `item_latents` VALUES (17951, 35, 10, 29, 0);
INSERT INTO `item_latents` VALUES (17951, 36, 10, 35, 0);
INSERT INTO `item_latents` VALUES (17951, 37, 10, 30, 0);
INSERT INTO `item_latents` VALUES (17951, 38, 10, 36, 0);
INSERT INTO `item_latents` VALUES (17951, 39, 10, 32, 0);
-- Storm Tulwar
INSERT INTO `item_latents` VALUES (17715, 32, 10, 28, 0);
INSERT INTO `item_latents` VALUES (17715, 33, 10, 34, 0);
INSERT INTO `item_latents` VALUES (17715, 34, 10, 31, 0);
INSERT INTO `item_latents` VALUES (17715, 35, 10, 29, 0);
INSERT INTO `item_latents` VALUES (17715, 36, 10, 35, 0);
INSERT INTO `item_latents` VALUES (17715, 37, 10, 30, 0);
INSERT INTO `item_latents` VALUES (17715, 38, 10, 36, 0);
INSERT INTO `item_latents` VALUES (17715, 39, 10, 32, 0);
-- Storm Zaghnal
INSERT INTO `item_latents` VALUES (18065, 32, 10, 28, 0);
INSERT INTO `item_latents` VALUES (18065, 33, 10, 34, 0);
INSERT INTO `item_latents` VALUES (18065, 34, 10, 31, 0);
INSERT INTO `item_latents` VALUES (18065, 35, 10, 29, 0);
INSERT INTO `item_latents` VALUES (18065, 36, 10, 35, 0);
INSERT INTO `item_latents` VALUES (18065, 37, 10, 30, 0);
INSERT INTO `item_latents` VALUES (18065, 38, 10, 36, 0);
INSERT INTO `item_latents` VALUES (18065, 39, 10, 32, 0);
-- Yigit Bulawa
INSERT INTO `item_latents` VALUES (18408, 32, 10, 28, 0);
INSERT INTO `item_latents` VALUES (18408, 33, 10, 34, 0);
INSERT INTO `item_latents` VALUES (18408, 34, 10, 31, 0);
INSERT INTO `item_latents` VALUES (18408, 35, 10, 29, 0);
INSERT INTO `item_latents` VALUES (18408, 36, 10, 35, 0);
INSERT INTO `item_latents` VALUES (18408, 37, 10, 30, 0);
INSERT INTO `item_latents` VALUES (18408, 38, 10, 36, 0);
INSERT INTO `item_latents` VALUES (18408, 39, 10, 32, 0);
-- Arco De Velocidad
INSERT INTO `item_latents` VALUES (17165, 370, 1, 26, 0);
-- Mighty Bow
INSERT INTO `item_latents` VALUES (17204, 376, 10, 31, 0);
INSERT INTO `item_latents` VALUES (17204, 56, 15, 36, 0);
-- Imperial Bow
-- INSERT INTO `item_latents` VALUES (18683, 376, 2, ?, ?); -- Assault
-- INSERT INTO `item_latents` VALUES (18683, 26, 10, ?, ?); -- Assault
-- Selene's Bow
INSERT INTO `item_latents` VALUES (17212, 25, 25, 59, 0);
INSERT INTO `item_latents` VALUES (17212, 26, 5, 59, 0);
INSERT INTO `item_latents` VALUES (17212, 25, 15, 37, 0);
INSERT INTO `item_latents` VALUES (17212, 26, 15, 37, 0);
INSERT INTO `item_latents` VALUES (17212, 25, 15, 62, 0);
INSERT INTO `item_latents` VALUES (17212, 26, 15, 62, 0);
INSERT INTO `item_latents` VALUES (17212, 25, 5, 65, 0);
INSERT INTO `item_latents` VALUES (17212, 26, 25, 65, 0);
INSERT INTO `item_latents` VALUES (17212, 25, 10, 63, 0);
INSERT INTO `item_latents` VALUES (17212, 26, 20, 63, 0);
INSERT INTO `item_latents` VALUES (17212, 25, 10, 64, 0);
INSERT INTO `item_latents` VALUES (17212, 26, 20, 64, 0);
INSERT INTO `item_latents` VALUES (17212, 25, 20, 60, 0);
INSERT INTO `item_latents` VALUES (17212, 26, 10, 60, 0);
INSERT INTO `item_latents` VALUES (17212, 25, 20, 61, 0);
INSERT INTO `item_latents` VALUES (17212, 26, 10, 61, 0); 
-- Wurger
INSERT INTO `item_latents` VALUES (19222, 24, 3, 52, 3);
-- Hamayumi
-- INSERT INTO `item_latents` VALUES (17208, 26, 10, ?, ?); --  Vs Empty
-- INSERT INTO `item_latents` VALUES (17208, 66, 10, ?, ?); --  Vs Empty
-- Coalition Axe
-- INSERT INTO `item_latents` VALUES (20830, 366, 11, ?, ?); -- Reives
-- Aytanri
-- INSERT INTO `item_latents` VALUES (18542, ?, ?, ?, ?); -- Attack Varies with Def
-- Tonatiuh Axe
INSERT INTO `item_latents` VALUES (20830, 366, 10, 65, 0);
-- Cleofun Axe
-- INSERT INTO `item_latents` VALUES (18532, 355, 73, ?, ?); -- 13 Weaponskills get 1 use
-- Erlking's Tabar
-- INSERT INTO `item_latents` VALUES (17966, 366, 4, ?, ?); -- Main Hand
-- Mighty Pick
INSERT INTO `item_latents` VALUES (17941, 366, 5, 31, 0);
INSERT INTO `item_latents` VALUES (17941, 56, 15, 31, 0);
-- Barkborer
-- INSERT INTO `item_latents` VALUES (17964, 165, 7, ?, ?); -- Vs Plantoids
-- Reserve Captian's Pick
INSERT INTO `item_latents` VALUES (17934, 23, 10, 33, 0);
-- Pick of Trials
INSERT INTO `item_latents` VALUES (17933, 2, -20, 47, 0); -- WS Points <= 300
INSERT INTO `item_latents` VALUES (17933, 56, -10, 47, 0);
INSERT INTO `item_latents` VALUES (17933, 58, -10, 47, 0);
-- Rune Axe
INSERT INTO `item_latents` VALUES (16647, 23, 5, 12, 0);
INSERT INTO `item_latents` VALUES (16647, 370, 5, 12, 0);
INSERT INTO `item_latents` VALUES (16647, 369, -3, 12, 0);
-- Marid Ancus
-- INSERT INTO `item_latents` VALUES (17950, 366, 4, ?, ?); -- Assault
-- INSERT INTO `item_latents` VALUES (17950, 411, 2, ?, ?);
-- Rainmaker
-- INSERT INTO `item_latents` VALUES (17963, 8, 5, ?, ?); -- Campaign
-- Combat Caster's Axe +2
INSERT INTO `item_latents` VALUES (17932, 9, 3, 53, 1);
-- Combat Caster's Axe +1
INSERT INTO `item_latents` VALUES (17931, 9, 2, 53, 1);
-- Garde Pick
INSERT INTO `item_latents` VALUES (17947, 26, 3, 53, 0);
INSERT INTO `item_latents` VALUES (17947, 24, 3, 53, 0);
-- Tortoise Shield
INSERT INTO `item_latents` VALUES (12374, 7, 15, 53, 0);
-- Ajase Beads
INSERT INTO `item_latents` VALUES (15504, 23, 3, 53, 0);
INSERT INTO `item_latents` VALUES (15504, 25, 3, 53, 0);

-- Trunkcleaver
-- INSERT INTO `item_latents` VALUES (20876, 366, 27, ?, ?); -- Reives
-- Wardancer
INSERT INTO `item_latents` VALUES (18486, 381, -6, 25, 0);
-- Mensur Epee
INSERT INTO `item_latents` VALUES (17719, 366, -2, 15, 2);
INSERT INTO `item_latents` VALUES (17719, 366, -2, 15, 3);
INSERT INTO `item_latents` VALUES (17719, 366, -2, 15, 4);
INSERT INTO `item_latents` VALUES (17719, 366, -2, 15, 5);
INSERT INTO `item_latents` VALUES (17719, 366, -2, 15, 6);
-- Coalition Rod
-- INSERT INTO `item_latents` VALUES (21131, 366, 13, ?, ?); -- Reives
-- Ankylosis Wand
INSERT INTO `item_latents` VALUES (18888, 30, 20, 13, 11);
INSERT INTO `item_latents` VALUES (18888, 28, 20, 13, 11);
-- Molva Maul
-- INSERT INTO `item_latents` VALUES (17066, 355, 170, ?, ?); -- 13 Weaponskills get 1 use
-- Dewormer Maul
-- INSERT INTO `item_latents` VALUES (18870, 366, 3, ?, ?); -- Main Hand
-- Zonure
-- INSERT INTO `item_latents` VALUES (18865, 165, 7, ?, ?); -- Vs Vermin
-- Senior Musketeer's Rod
INSERT INTO `item_latents` VALUES (17457, 71, 7, 33, 1);
-- Reserve Captian's Mace
INSERT INTO `item_latents` VALUES (17458, 71, 7, 33, 1);
-- Club of Trials
INSERT INTO `item_latents` VALUES (17456, 2, -10, 47, 0); -- WS Points <= 300
INSERT INTO `item_latents` VALUES (17456, 5, -10, 47, 0);
INSERT INTO `item_latents` VALUES (17456, 57, -10, 47, 0);
INSERT INTO `item_latents` VALUES (17456, 59, -10, 47, 0);
-- Rune Rod
INSERT INTO `item_latents` VALUES (17461, 23, 10, 12, 0);
INSERT INTO `item_latents` VALUES (17461, 112, 6, 12, 0);
INSERT INTO `item_latents` VALUES (17461, 369, -4, 12, 0);
-- Mighty Cudgel
INSERT INTO `item_latents` VALUES (17465, 12, 9, 28, 0);
INSERT INTO `item_latents` VALUES (17465, 13, 9, 28, 0);
INSERT INTO `item_latents` VALUES (17465, 54, 15, 28, 0);
-- Imperial Wand
-- INSERT INTO `item_latents` VALUES (18407, 366, 2, ?, ?); -- Assault
-- INSERT INTO `item_latents` VALUES (18407, 5, 20, ?, ?); -- Assault
-- INSERT INTO `item_latents` VALUES (18407, 27, -3, ?, ?); -- Assault
-- Thoth's Wand
INSERT INTO `item_latents` VALUES (17443, 366, 10, 59, 0);
INSERT INTO `item_latents` VALUES (17443, 341, 7, 59, 0);
INSERT INTO `item_latents` VALUES (17443, 343, 20, 59, 0);
INSERT INTO `item_latents` VALUES (17443, 366, 5, 37, 0);
INSERT INTO `item_latents` VALUES (17443, 366, 5, 62, 0);
INSERT INTO `item_latents` VALUES (17443, 366, 1, 65, 0);
INSERT INTO `item_latents` VALUES (17443, 341, 8, 65, 0);
INSERT INTO `item_latents` VALUES (17443, 343, 20, 65, 0);
INSERT INTO `item_latents` VALUES (17443, 366, 7, 63, 0);
INSERT INTO `item_latents` VALUES (17443, 366, 7, 64, 0);
INSERT INTO `item_latents` VALUES (17443, 366, 2, 60, 0);
INSERT INTO `item_latents` VALUES (17433, 366, 2, 61, 0); 
-- Artemis Wand
INSERT INTO `item_latents` VALUES (17419, 366, 7, 59, 0);
INSERT INTO `item_latents` VALUES (17419, 341, 7, 59, 0);
INSERT INTO `item_latents` VALUES (17419, 343, 8, 59, 0);
INSERT INTO `item_latents` VALUES (17419, 366, 5, 37, 0);
INSERT INTO `item_latents` VALUES (17419, 366, 5, 62, 0);
INSERT INTO `item_latents` VALUES (17419, 366, 1, 65, 0);
INSERT INTO `item_latents` VALUES (17419, 341, 8, 65, 0);
INSERT INTO `item_latents` VALUES (17419, 343, 8, 65, 0);
INSERT INTO `item_latents` VALUES (17419, 366, 4, 63, 0);
INSERT INTO `item_latents` VALUES (17419, 366, 4, 64, 0);
INSERT INTO `item_latents` VALUES (17419, 366, 3, 60, 0);
INSERT INTO `item_latents` VALUES (17419, 366, 3, 61, 0); 
-- Moonlight Wand
INSERT INTO `item_latents` VALUES (17418, 366, 6, 59, 0);
INSERT INTO `item_latents` VALUES (17418, 341, 7, 59, 0);
INSERT INTO `item_latents` VALUES (17418, 343, 6, 59, 0);
INSERT INTO `item_latents` VALUES (17418, 366, 4, 37, 0);
INSERT INTO `item_latents` VALUES (17418, 366, 4, 62, 0);
INSERT INTO `item_latents` VALUES (17418, 366, 1, 65, 0);
INSERT INTO `item_latents` VALUES (17418, 341, 8, 65, 0);
INSERT INTO `item_latents` VALUES (17418, 343, 6, 65, 0);
INSERT INTO `item_latents` VALUES (17418, 366, 3, 63, 0);
INSERT INTO `item_latents` VALUES (17418, 366, 3, 64, 0);
INSERT INTO `item_latents` VALUES (17418, 366, 2, 60, 0);
INSERT INTO `item_latents` VALUES (17418, 366, 2, 61, 0); 
-- Moonwatch Wand
INSERT INTO `item_latents` VALUES (17421, 366, 9, 59, 0);
INSERT INTO `item_latents` VALUES (17421, 341, 7, 59, 0);
INSERT INTO `item_latents` VALUES (17421, 343, 17, 59, 0);
INSERT INTO `item_latents` VALUES (17421, 366, 5, 37, 0);
INSERT INTO `item_latents` VALUES (17421, 366, 5, 62, 0);
INSERT INTO `item_latents` VALUES (17421, 366, 1, 65, 0);
INSERT INTO `item_latents` VALUES (17421, 341, 8, 65, 0);
INSERT INTO `item_latents` VALUES (17421, 343, 17, 65, 0);
INSERT INTO `item_latents` VALUES (17421, 366, 7, 63, 0);
INSERT INTO `item_latents` VALUES (17421, 366, 7, 64, 0);
INSERT INTO `item_latents` VALUES (17421, 366, 2, 60, 0);
INSERT INTO `item_latents` VALUES (17421, 366, 2, 61, 0); 
-- Rabbit Wand
INSERT INTO `item_latents` VALUES (17417, 366, 9, 59, 0);
INSERT INTO `item_latents` VALUES (17417, 341, 7, 59, 0);
INSERT INTO `item_latents` VALUES (17417, 343, 4, 59, 0);
INSERT INTO `item_latents` VALUES (17417, 366, 5, 37, 0);
INSERT INTO `item_latents` VALUES (17417, 366, 5, 62, 0);
INSERT INTO `item_latents` VALUES (17417, 366, 1, 65, 0);
INSERT INTO `item_latents` VALUES (17417, 341, 8, 65, 0);
INSERT INTO `item_latents` VALUES (17417, 343, 4, 65, 0);
INSERT INTO `item_latents` VALUES (17417, 366, 7, 63, 0);
INSERT INTO `item_latents` VALUES (17417, 366, 7, 64, 0);
INSERT INTO `item_latents` VALUES (17417, 366, 2, 60, 0);
INSERT INTO `item_latents` VALUES (17417, 366, 2, 61, 0); 
-- Selene's Wand
INSERT INTO `item_latents` VALUES (17420, 366, 5, 59, 0);
INSERT INTO `item_latents` VALUES (17420, 341, 7, 59, 0);
INSERT INTO `item_latents` VALUES (17420, 343, 4, 59, 0);
INSERT INTO `item_latents` VALUES (17420, 366, 3, 37, 0);
INSERT INTO `item_latents` VALUES (17420, 366, 3, 62, 0);
INSERT INTO `item_latents` VALUES (17420, 366, 1, 65, 0);
INSERT INTO `item_latents` VALUES (17420, 341, 8, 65, 0);
INSERT INTO `item_latents` VALUES (17420, 343, 4, 65, 0);
INSERT INTO `item_latents` VALUES (17420, 366, 3, 63, 0);
INSERT INTO `item_latents` VALUES (17420, 366, 3, 64, 0);
INSERT INTO `item_latents` VALUES (17420, 366, 2, 60, 0);
INSERT INTO `item_latents` VALUES (17420, 366, 2, 61, 0); 
-- Tactician Magician's Wand +2
INSERT INTO `item_latents` VALUES (17447, 5, 20, 53, 1);
-- Tactician Magician's Wand +1
INSERT INTO `item_latents` VALUES (17446, 5, 18, 53, 1);
-- Sandorian Mace
INSERT INTO `item_latents` VALUES (17448, 13, 1, 53, 1);
-- Kingdom Mace
INSERT INTO `item_latents` VALUES (17449, 13, 2, 53, 1);
-- Coalition Dirk
-- INSERT INTO `item_latents` VALUES (20638, 366, 7, ?, ?); -- Reives
-- Clement Skean
-- INSERT INTO `item_latents` VALUES (17066, 355, 26, ?, ?); -- 13 Weaponskills get 1 use
-- Leste Jambiya
INSERT INTO `item_latents` VALUES (16756, 68, 20, 31, 0);
-- Ogre Jambiya
-- INSERT INTO `item_latents` VALUES (19117, 366, 6, ?, ?); -- Main Hand
-- Fane Baselard
-- INSERT INTO `item_latents` VALUES (19115, 366, 5, ?, ?); -- Main Hand
-- Dewormer Knife
-- INSERT INTO `item_latents` VALUES (19116, 366, 3, ?, ?); -- Main Hand
-- Trilling Dagger
INSERT INTO `item_latents` VALUES (19109, 23, 10, 13, 6);
-- Mighty Knife
INSERT INTO `item_latents` VALUES (18000, 366, 10, 31, 0);
INSERT INTO `item_latents` VALUES (18000, 56, 15, 31, 0);
-- Papilio Kirpan
INSERT INTO `item_latents` VALUES (19126, 366, 6, 6, 100);
INSERT INTO `item_latents` VALUES (19126, 23, 14, 6, 100);
INSERT INTO `item_latents` VALUES (19126, 25, 5, 6, 100);
-- Ermine's Tail
-- INSERT INTO `item_latents` VALUES (19113, 165, 7, ?, ?); -- Vs Lizards
-- Master Caster's Knife
INSERT INTO `item_latents` VALUES (19108, 1, 10, 33, 2);
-- Dagger of Trials
INSERT INTO `item_latents` VALUES (17616, 2, -20, 47, 0); -- WS Points <= 300
INSERT INTO `item_latents` VALUES (17616, 55, -10, 47, 0);
INSERT INTO `item_latents` VALUES (17616, 57, -10, 47, 0);
-- Daylight Dagger
INSERT INTO `item_latents` VALUES (17619, 25, 12, 26, 0);
-- Assassin's Jambiya
-- INSERT INTO `item_latents` VALUES (18017, 366, 3, ?, ?); -- Assault
-- INSERT INTO `item_latents` VALUES (18017, 165, 5, ?, ?);
-- Trainee Knife
INSERT INTO `item_latents` VALUES (19101, 135, 1, 24, 0);
-- Conqueror 75
INSERT INTO `item_latents` VALUES (18991, 62, 5, 13, 56);
INSERT INTO `item_latents` VALUES (18991, 63, -5, 13, 56);
INSERT INTO `item_latents` VALUES (18991, 165, 5, 13, 56);
-- Conqueror 80
INSERT INTO `item_latents` VALUES (19060, 62, 6, 13, 56);
INSERT INTO `item_latents` VALUES (19060, 63, -6, 13, 56);
INSERT INTO `item_latents` VALUES (19060, 165, 7, 13, 56);
-- Conqueror 85
INSERT INTO `item_latents` VALUES (19080, 62, 7, 13, 56);
INSERT INTO `item_latents` VALUES (19080, 63, -7, 13, 56);
INSERT INTO `item_latents` VALUES (19080, 165, 9, 13, 56);
-- Conqueror 90
INSERT INTO `item_latents` VALUES (19612, 62, 8, 13, 56);
INSERT INTO `item_latents` VALUES (19612, 63, -8, 13, 56);
INSERT INTO `item_latents` VALUES (19612, 165, 11, 13, 56);
-- Conqueror 95
INSERT INTO `item_latents` VALUES (19710, 62, 8, 13, 56);
INSERT INTO `item_latents` VALUES (19710, 63, -8, 13, 56);
INSERT INTO `item_latents` VALUES (19710, 165, 11, 13, 56);
-- Conqueror 99
INSERT INTO `item_latents` VALUES (19819, 62, 9, 13, 56);
INSERT INTO `item_latents` VALUES (19819, 63, -9, 13, 56);
INSERT INTO `item_latents` VALUES (19819, 165, 14, 13, 56);
-- Conqueror 99 - 2
INSERT INTO `item_latents` VALUES (19948, 62, 9, 13, 56);
INSERT INTO `item_latents` VALUES (19948, 63, -9, 13, 56);
INSERT INTO `item_latents` VALUES (19948, 165, 14, 13, 56);
-- Barbarus Bhuj
-- INSERT INTO `item_latents` VALUES (18513, 355, 89, ?, ?); -- 13 Weaponskills get 1 use
-- Wootz Amood +1
INSERT INTO `item_latents` VALUES (18496, 366, -33, 37, 0);
INSERT INTO `item_latents` VALUES (18496, 288, 15, 37, 0);
-- Wootz Amood
INSERT INTO `item_latents` VALUES (18495, 366, -33, 37, 0);
INSERT INTO `item_latents` VALUES (18495, 288, 10, 37, 0);
-- Amood +1
INSERT INTO `item_latents` VALUES (18483, 366, -30, 37, 0);
INSERT INTO `item_latents` VALUES (18483, 288, 15, 37, 0);
-- Amood
INSERT INTO `item_latents` VALUES (18482, 366, -30, 37, 0);
INSERT INTO `item_latents` VALUES (18482, 288, 10, 37, 0);
-- Balestarius
-- INSERT INTO `item_latents` VALUES (18498, 288, 10, ?, ?); -- Campaign
-- Mighty Axe
INSERT INTO `item_latents` VALUES (18213, 366, 10, 28, 0);
INSERT INTO `item_latents` VALUES (18213, 32, 15, 28, 0);
-- Rampager
-- INSERT INTO `item_latents` VALUES (18217, 366, 13, ?, ?); -- WS Points <= 500
-- INSERT INTO `item_latents` VALUES (18217, 165, 6, ?, ?);
-- Footkiller
-- INSERT INTO `item_latents` VALUES (18497, 8, 8, ?, ?); -- Unknown latent
-- Eventreuse
-- INSERT INTO `item_latents` VALUES (18497, 165, 7, ?, ?); -- Vs Plantoid
-- Senior Gold Musketeer's Axe
INSERT INTO `item_latents` VALUES (18196, 23, 10, 33, 1);
-- Senior Gold Musketeer's Scimitar
INSERT INTO `item_latents` VALUES (17655, 1, 10, 33, 1);
-- Seismic Axe
-- INSERT INTO `item_latents` VALUES (18501, 48, 10, ?, ?); -- WS Acc + Dmg on WS of Scission
-- INSERT INTO `item_latents` VALUES (18501, ?, 10, ?, ?);
-- Wamoura Axe
-- INSERT INTO `item_latents` VALUES (18484, 366, 5, ?, ?); -- Assault
-- INSERT INTO `item_latents` VALUES (18484, 288, 1, ?, ?);
-- Rune Chopper
INSERT INTO `item_latents` VALUES (18206, 25, 5, 12, 0);
INSERT INTO `item_latents` VALUES (18206, 384, 92, 12, 0);
INSERT INTO `item_latents` VALUES (18206, 369, -3, 12, 0);
-- Schwarz Axe
INSERT INTO `item_latents` VALUES (16728, 25, 10, 26, 1);
-- Republic Greataxe
INSERT INTO `item_latents` VALUES (16733, 8, 2, 53, 1);
-- Bastokan Greataxe
INSERT INTO `item_latents` VALUES (16732, 8, 1, 53, 1);
-- Trainee Axe
INSERT INTO `item_latents` VALUES (18502, 128, 1, 24, 0);
-- Tenkomaru
-- INSERT INTO `item_latents` VALUES (18466, 288, 10, ?, 0); -- In front of enemy
-- Kashiwadachi
-- INSERT INTO `item_latents` VALUES (21055, 366, 16, ?, ?); -- Reives
-- Ame-no-ohabari
-- INSERT INTO `item_latents` VALUES (18455, 355, 153, ?, ?); -- 13 Weaponskills get 1 use
-- Futsuno Mitama
INSERT INTO `item_latents` VALUES (17810, 8, 8, 6, 100);
-- Ushikirimaru
-- INSERT INTO `item_latents` VALUES (17804, 25, 7, ?, ?); -- Vs Beasts
-- Nukemaru
INSERT INTO `item_latents` VALUES (17824, 366, 10, 26, 0);
INSERT INTO `item_latents` VALUES (17824, 32, 15, 26, 0);
-- Kumokirimaru
-- INSERT INTO `item_latents` VALUES (18438, 165, 8, ?, ?); -- Vs Vermin
-- Kamewari
-- INSERT INTO `item_latents` VALUES (16968, 165, 7, ?, ?); -- Vs Arcana
-- Irasya
INSERT INTO `item_latents` VALUES (19176, 160, 2, 13, 479);
INSERT INTO `item_latents` VALUES (19176, 160, 2, 13, 480);
INSERT INTO `item_latents` VALUES (19176, 27, 10, 13, 479);
INSERT INTO `item_latents` VALUES (19176, 27, 10, 13, 480);
-- Coalition Sword
-- INSERT INTO `item_latents` VALUES (20782, 366, 22, ?, ?); -- Reives
-- Khloros Blade
-- INSERT INTO `item_latents` VALUES (19167, 355, 57, ?, ?); -- 13 Weaponskills get 1 use
-- Mighty Sword
INSERT INTO `item_latents` VALUES (18374, 366, 9, 28, 0);
INSERT INTO `item_latents` VALUES (18374, 32, 15, 28, 0);
-- Scheherazade
-- INSERT INTO `item_latents` VALUES (19158, 165, 7, ?, ?); -- Vs Lizards
-- Cruadin
-- INSERT INTO `item_latents` VALUES (19155, 165, 15, ?, ?); -- Campaign
-- INSERT INTO `item_latents` VALUES (19155, 459, 5, ?, ?); -- Campaign
-- Royal Swordsman's Blade +2
INSERT INTO `item_latents` VALUES (16949, 10, 3, 53, 1);
-- Royal Swordsman's Blade +1
INSERT INTO `item_latents` VALUES (16948, 10, 2, 53, 1);
-- Coalition Blade
-- INSERT INTO `item_latents` VALUES (20741, 366, 12, ?, ?); -- Reives
-- Fieldrazer Scythe
-- INSERT INTO `item_latents` VALUES (20921, 366, 31, ?, ?); -- Reives
-- Coalition Lance
-- INSERT INTO `item_latents` VALUES (20966, 366, 26, ?, ?); -- Reives
-- Kotekirigo
-- INSERT INTO `item_latents` VALUES (21008, 366, 9, ?, ?); -- Reives
-- Sortilevel Staff
-- INSERT INTO `item_latents` VALUES (21204, 28, 50, ?, ?); -- Reives
-- Avitap Pole
-- INSERT INTO `item_latents` VALUES (21205, 312, 30, ?, ?); -- Reives
-- Shadeshot Gun
-- INSERT INTO `item_latents` VALUES (21205, 366, 12, ?, ?); -- Reives
-- Vineslash Cesti
-- INSERT INTO `item_latents` VALUES (20550, 366, 9, ?, ?); -- Reives
-- Glanzfaust 75
INSERT INTO `item_latents` VALUES (18992, 62, 5, 13, 59);
INSERT INTO `item_latents` VALUES (18992, 165, 5, 13, 59);
INSERT INTO `item_latents` VALUES (18992, 291, 5, 13, 60);
-- Glanzfaust 80
INSERT INTO `item_latents` VALUES (19061, 62, 10, 13, 59);
INSERT INTO `item_latents` VALUES (19061, 165, 10, 13, 59);
INSERT INTO `item_latents` VALUES (19061, 291, 7, 13, 60);
-- Glanzfaust 85
INSERT INTO `item_latents` VALUES (19081, 62, 15, 13, 59);
INSERT INTO `item_latents` VALUES (19081, 165, 15, 13, 59);
INSERT INTO `item_latents` VALUES (19081, 291, 9, 13, 60);
-- Glanzfaust 90
INSERT INTO `item_latents` VALUES (19613, 62, 20, 13, 59);
INSERT INTO `item_latents` VALUES (19613, 165, 20, 13, 59);
INSERT INTO `item_latents` VALUES (19613, 291, 11, 13, 60);
-- Glanzfaust 95
INSERT INTO `item_latents` VALUES (19711, 62, 20, 13, 59);
INSERT INTO `item_latents` VALUES (19711, 165, 20, 13, 59);
INSERT INTO `item_latents` VALUES (19711, 291, 11, 13, 60);
-- Glanzfaust 99
INSERT INTO `item_latents` VALUES (19820, 62, 25, 13, 59);
INSERT INTO `item_latents` VALUES (19820, 165, 25, 13, 59);
INSERT INTO `item_latents` VALUES (19820, 291, 13, 13, 60);
-- Glanzfaust 99 - 2
INSERT INTO `item_latents` VALUES (19949, 62, 25, 13, 59);
INSERT INTO `item_latents` VALUES (19949, 165, 25, 13, 59);
INSERT INTO `item_latents` VALUES (19949, 291, 13, 13, 60);
-- Taurine Cesti
INSERT INTO `item_latents` VALUES (18779, 62, 5, 13, 59);
-- Heofon Knuckles
-- INSERT INTO `item_latents` VALUES (18776, 355, 10, ?, ?); -- 13 Weaponskills get 1 use
-- Lunaris Claws
INSERT INTO `item_latents` VALUES (16427, 23, 1, 59, 0);
INSERT INTO `item_latents` VALUES (16427, 24, 1, 59, 0);
INSERT INTO `item_latents` VALUES (16427, 23, 10, 37, 0);
INSERT INTO `item_latents` VALUES (16427, 24, 8, 37, 0);
INSERT INTO `item_latents` VALUES (16427, 23, 10, 62, 0);
INSERT INTO `item_latents` VALUES (16427, 24, 8, 62, 0);
INSERT INTO `item_latents` VALUES (16427, 23, 20, 65, 0);
INSERT INTO `item_latents` VALUES (16427, 24, 15, 65, 0);
INSERT INTO `item_latents` VALUES (16427, 23, 15, 63, 0);
INSERT INTO `item_latents` VALUES (16427, 24, 12, 63, 0);
INSERT INTO `item_latents` VALUES (16427, 23, 15, 64, 0);
INSERT INTO `item_latents` VALUES (16427, 24, 12, 64, 0);
INSERT INTO `item_latents` VALUES (16427, 23, 5, 60, 0);
INSERT INTO `item_latents` VALUES (16427, 24, 3, 60, 0);
INSERT INTO `item_latents` VALUES (16427, 23, 5, 61, 0);
INSERT INTO `item_latents` VALUES (16427, 24, 3, 61, 0); 
-- Destroyers
-- INSERT INTO `item_latents` VALUES (17509, 366, 13, ?, ?); -- WS Points >= 500
-- INSERT INTO `item_latents` VALUES (17509, 195, 6, ?, ?);
-- Mighty Patas
INSERT INTO `item_latents` VALUES (18352, 366, 5, 28, 0);
INSERT INTO `item_latents` VALUES (18352, 54, 15, 28, 0);
-- Master Caster's Baghnakhs
INSERT INTO `item_latents` VALUES (17508, 23, 10, 33, 2);
-- Rune Baghnakhs
INSERT INTO `item_latents` VALUES (17333, 8, 7, 12, 0);
INSERT INTO `item_latents` VALUES (17333, 291, 1, 12, 0);
INSERT INTO `item_latents` VALUES (17333, 370, -4, 12, 0);
-- Pahluwan Katars
-- INSERT INTO `item_latents` VALUES (18364, 366, 2, ?, ?); -- Assault
-- INSERT INTO `item_latents` VALUES (18364, 291, 1, ?, 0);
-- Gnole Sainti +1
INSERT INTO `item_latents` VALUES (18758, 8, 3, 65, 0);
INSERT INTO `item_latents` VALUES (18758, 9, 3, 65, 0);
-- Gnole Sainti
INSERT INTO `item_latents` VALUES (18757, 8, 2, 65, 0);
INSERT INTO `item_latents` VALUES (18757, 9, 2, 65, 0);
-- Birdbanes
-- INSERT INTO `item_latents` VALUES (18768, 25, 8, ?, ?); -- Vs Birds
-- Tactician Magician's Hooks +2
INSERT INTO `item_latents` VALUES (17502, 14, 4, 53, 1);
-- Tactician Magician's Hooks +1
INSERT INTO `item_latents` VALUES (17501, 14, 3, 53, 1);
-- Trainee Scissors
INSERT INTO `item_latents` VALUES (18763, 131, 1, 24, 0);
-- Kakesu
INSERT INTO `item_latents` VALUES (19297, 23, 5, 13, 66);
INSERT INTO `item_latents` VALUES (19297, 23, 10, 13, 444);
INSERT INTO `item_latents` VALUES (19297, 23, 15, 13, 445);
INSERT INTO `item_latents` VALUES (19297, 23, 20, 13, 446);
-- Sekirei
-- INSERT INTO `item_latents` VALUES (19288, 355, 137, ?, ?); -- 13 Weaponskills get 1 use
-- Aizenkunitoshi
-- INSERT INTO `item_latents` VALUES (19290, 457, ?, ?, ?); -- WS Dmg +? -- Unknown latent
-- Tsukumo
-- INSERT INTO `item_latents` VALUES (19275, 366, 4, ?, ?); -- Main Hand
-- Rai Kunimitsu
INSERT INTO `item_latents` VALUES (17791, 366, 7, 28, 0);
INSERT INTO `item_latents` VALUES (17791, 54, 15, 28, 0);
-- Onishibari
-- INSERT INTO `item_latents` VALUES (19273, 165, 7, ?, ?); -- Vs Beasts
-- Karasuageha
-- INSERT INTO `item_latents` VALUES (18416, 366, 4, ?, ?); -- Assault
-- INSERT INTO `item_latents` VALUES (18416, 291, 1, ?, ?);
-- Sairen
INSERT INTO `item_latents` VALUES (17788, 8, 7, 32, 0);
INSERT INTO `item_latents` VALUES (17788, 27, 1, 32, 0);
-- Hototogisu
INSERT INTO `item_latents` VALUES (16899, 110, 5, 25, 0);
-- Trainee Burin
INSERT INTO `item_latents` VALUES (19274, 130, 1, 24, 0);
-- Gastraphetes - 75
INSERT INTO `item_latents` VALUES (19001, 26, 5, 13, 73);
-- Gastraphetes - 80
INSERT INTO `item_latents` VALUES (19070, 26, 10, 13, 73);
-- Gastraphetes - 85
INSERT INTO `item_latents` VALUES (19090, 26, 15, 13, 73);
-- Gastraphetes - 90
INSERT INTO `item_latents` VALUES (19622, 26, 20, 13, 73);
-- Gastraphetes - 95
INSERT INTO `item_latents` VALUES (19720, 26, 20, 13, 73);
-- Gastraphetes - 99
INSERT INTO `item_latents` VALUES (19829, 26, 25, 13, 73);
-- Gastraphetes - 99 - 2
INSERT INTO `item_latents` VALUES (19958, 26, 25, 13, 73);
-- Exequy Gun
-- INSERT INTO `item_latents` VALUES (19191, 355, 216, ?, ?); -- 13 Weaponskills get 1 use
-- Perdu Crossbow
INSERT INTO `item_latents` VALUES (18718, 366, 4, 6, 100);
INSERT INTO `item_latents` VALUES (18718, 26, 5, 6, 100);
INSERT INTO `item_latents` VALUES (18718, 24, 18, 6, 100);
-- Coffinmaker
-- INSERT INTO `item_latents` VALUES (17275, 366, 13, ?, ?); -- WS Points >= 500
-- Storm Zamburak
-- INSERT INTO `item_latents` VALUES (18684, 366, 2, ?, ?); -- Assault
-- INSERT INTO `item_latents` VALUES (18684, 26, 10, ?, ?);
-- Skogul Lance
-- INSERT INTO `item_latents` VALUES (19312, 355, 121, ?, ?); -- 13 Weaponskills get 1 use
-- Mighty Lance
INSERT INTO `item_latents` VALUES (18091, 366, 12, 28, 0);
INSERT INTO `item_latents` VALUES (18091, 54, 15, 28, 0);
-- Zaide
-- INSERT INTO `item_latents` VALUES (18131, 165, 7, ?, ?); -- Vs Beasts
-- Z's Trident
-- INSERT INTO `item_latents` VALUES (18101, 8, 12, ?, ?); -- Party Members with X Knife and Y Scythe
-- Reserve Captian's Lance
INSERT INTO `item_latents` VALUES (16893, 1, 10, 33, 2);
-- Rune Halberd
INSERT INTO `item_latents` VALUES (18084, 9, 6, 12, 0);
INSERT INTO `item_latents` VALUES (18084, 288, 5, 12, 0);
INSERT INTO `item_latents` VALUES (18084, 370, -3, 12, 0);
-- Puk Lance
-- INSERT INTO `item_latents` VALUES (18112, 366, 9, ?, ?); -- Assault
-- INSERT INTO `item_latents` VALUES (18112, 495, 50, ?, ?); -- Wyvern HP +50
-- Tonbo-Giri
-- INSERT INTO `item_latents` VALUES (16838, 165, 3, ?, ?); -- Vs Vermin
-- Grand Temple Knight's Army Collar
INSERT INTO `item_latents` VALUES (13140, 1, 7, 53, 1);
INSERT INTO `item_latents` VALUES (13140, 23, 5, 53, 1);
INSERT INTO `item_latents` VALUES (13140, 25, 5, 53, 1);
-- Master Caster's Mitts
INSERT INTO `item_latents` VALUES (14016, 11, 2, 53, 1);
-- Royal Knights Lance +2
INSERT INTO `item_latents` VALUES (18073, 23, 9, 53, 1);
-- Royal Knights Lance +1
INSERT INTO `item_latents` VALUES (18072, 23, 8, 53, 1);
-- Rossignol
INSERT INTO `item_latents` VALUES (18075, 8, 2, 26, 2);
-- Sandorian Halberd
INSERT INTO `item_latents` VALUES (18070, 25, 3, 53, 1);
-- Kingdom Halberd
INSERT INTO `item_latents` VALUES (18071, 25, 4, 53, 1);
-- San d'Orian Dagger
INSERT INTO `item_latents` VALUES (17972, 11, 1, 53, 1);
-- Kingdom Dagger
INSERT INTO `item_latents` VALUES (17973, 11, 2, 53, 1);
-- Grand Temple Knight's Bangles
INSERT INTO `item_latents` VALUES (14014, 11, 2, 53, 1);
-- Pitchfork +1
INSERT INTO `item_latents` VALUES (18072, 169, 20, 13, 127);
-- Desert Boots
INSERT INTO `item_latents` VALUES (14166, 169, 12, 13, 181);
-- Desert Boots +1
INSERT INTO `item_latents` VALUES (14167, 169, 12, 13, 181);
-- Adflictio
-- INSERT INTO `item_latents` VALUES (18565, 8, 15, ?, ?); -- Active above 100 HP
-- INSERT INTO `item_latents` VALUES (18565, ?, ?, ?, ?); -- Drains 60-90 HP per Crit Hit
-- INSERT INTO `item_latents` VALUES (18565, ?, ?, ?, ?); -- Drains 50 HP per regular hit
-- Woeborn
INSERT INTO `item_latents` VALUES (14167, 343, 5, 13, 288);
-- Crisis Scythe
-- INSERT INTO `item_latents` VALUES (18966, 355, 105, ?, ?); -- 13 Weaponskills get 1 use
-- Y's Scythe
INSERT INTO `item_latents` VALUES (18057, 346, 2, 9, 7);
INSERT INTO `item_latents` VALUES (18057, 346, 2, 9, 9);
INSERT INTO `item_latents` VALUES (18057, 346, 2, 9, 16);
INSERT INTO `item_latents` VALUES (18057, 346, -2, 9, 8);
INSERT INTO `item_latents` VALUES (18057, 346, -2, 9, 6);
-- Diabolos's Rope
INSERT INTO `item_latents` VALUES (18057, 346, 1, 9, 16);
-- Accord Hat
INSERT INTO `item_latents` VALUES (18057, 346, 1, 9, 9);
-- Mighty Zaghnal
INSERT INTO `item_latents` VALUES (18049, 366, 8, 31, 0);
INSERT INTO `item_latents` VALUES (18049, 56, 15, 31, 0);
-- Senior Gold Musketeer's Scythe
INSERT INTO `item_latents` VALUES (16799, 25, 7, 33, 1);
-- Volunteer's Scythe
-- INSERT INTO `item_latents` VALUES (18064, 366, 2, ?, ?); -- Assault
-- INSERT INTO `item_latents` VALUES (18064, 23, 6, ?, ?);
-- Goshisho's Scythe
-- INSERT INTO `item_latents` VALUES (16792, 25, 7, ?, ?); -- Vs Undead
-- Windurstian Scythe
INSERT INTO `item_latents` VALUES (18036, 5, 10, 53, 1);
-- Federation Scythe
INSERT INTO `item_latents` VALUES (18037, 5, 12, 53, 1);
-- Tupsimati 75
-- INSERT INTO `item_latents` VALUES (18990, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (18990, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (18990, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (18990, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (18990, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (18990, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (18990, 27, -20, ?, ?); -- Same elemental magic as weather
-- Tupsimati 80
-- INSERT INTO `item_latents` VALUES (19079, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19079, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19079, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19079, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19079, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19079, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19079, 27, -20, ?, ?); -- Same elemental magic as weather
-- Tupsimati 85
-- INSERT INTO `item_latents` VALUES (19099, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19099, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19099, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19099, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19099, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19099, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19099, 27, -20, ?, ?); -- Same elemental magic as weather
-- Tupsimati 90
-- INSERT INTO `item_latents` VALUES (19631, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19631, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19631, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19631, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19631, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19631, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19631, 27, -20, ?, ?); -- Same elemental magic as weather
-- Tupsimati 95
-- INSERT INTO `item_latents` VALUES (19729, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19729, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19729, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19729, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19729, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19729, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19729, 27, -20, ?, ?); -- Same elemental magic as weather
-- Tupsimati 99
-- INSERT INTO `item_latents` VALUES (19838, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19838, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19838, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19838, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19838, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19838, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19838, 27, -20, ?, ?); -- Same elemental magic as weather
-- Tupsimati 99 - 2
-- INSERT INTO `item_latents` VALUES (19967, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19967, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19967, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19967, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19967, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19967, 27, -20, ?, ?); -- Same elemental magic as weather
-- INSERT INTO `item_latents` VALUES (19967, 27, -20, ?, ?); -- Same elemental magic as weather
-- Chthonic Staff
-- INSERT INTO `item_latents` VALUES (18619, 355, 185, ?, ?); -- 13 Weaponskills get 1 use
-- Numen Staff
INSERT INTO `item_latents` VALUES (18619, 369, 1, 10, 0);
-- Mighty Pole
INSERT INTO `item_latents` VALUES (17581, 56, 15, 31, 0);
INSERT INTO `item_latents` VALUES (17581, 111, 13, 31, 0);
INSERT INTO `item_latents` VALUES (17581, 115, 13, 31, 0);
-- Prester
-- INSERT INTO `item_latents` VALUES (18598, 48, 10, ?, ?); -- WS Acc + Dmg on WS of Detonation
-- INSERT INTO `item_latents` VALUES (18598, ?, 10, ?, ?);
-- Master Caster's Pole
INSERT INTO `item_latents` VALUES (17530, 72, 8, 33, 2);
-- Yigit Staff
-- INSERT INTO `item_latents` VALUES (18582, 366, 1, ?, ?); -- Assault
-- INSERT INTO `item_latents` VALUES (18582, 6, 2, ?, ?);
-- Rune Staff
INSERT INTO `item_latents` VALUES (17093, 12, 2, 12, 0);
INSERT INTO `item_latents` VALUES (17093, 13, 2, 12, 0);
INSERT INTO `item_latents` VALUES (17093, 14, 2, 12, 0);
INSERT INTO `item_latents` VALUES (17093, 369, -4, 12, 0);
-- Sunlight Pole
-- INSERT INTO `item_latents` VALUES (17093, 370, 1, ?, 0); -- Sunny weather
-- Musketeer's Pole +2
INSERT INTO `item_latents` VALUES (17540, 2, 12, 53, 1);
INSERT INTO `item_latents` VALUES (17540, 5, 12, 53, 1);
-- Musketeer's Pole +1
INSERT INTO `item_latents` VALUES (17539, 2, 10, 53, 1);
INSERT INTO `item_latents` VALUES (17539, 5, 10, 53, 1);
-- Apollo's Staff
INSERT INTO `item_latents` VALUES (17588, 346, 3, 9, 6);
INSERT INTO `item_latents` VALUES (17588, 346, 3, 9, 8);
INSERT INTO `item_latents` VALUES (17588, 346, -3, 9, 16);
INSERT INTO `item_latents` VALUES (17588, 346, -3, 9, 9);
INSERT INTO `item_latents` VALUES (17588, 346, -3, 9, 7);
-- Aquilo's Staff
INSERT INTO `item_latents` VALUES (17548, 346, 3, 9, 14);
INSERT INTO `item_latents` VALUES (17548, 346, 3, 9, 1);
-- Auster's Staff
INSERT INTO `item_latents` VALUES (17550, 346, 3, 9, 13);
INSERT INTO `item_latents` VALUES (17550, 346, 3, 9, 2);
-- Dark Staff
INSERT INTO `item_latents` VALUES (17559, 346, 2, 9, 16);
INSERT INTO `item_latents` VALUES (17559, 346, 2, 9, 9);
INSERT INTO `item_latents` VALUES (17559, 346, 2, 9, 7);
INSERT INTO `item_latents` VALUES (17559, 346, -2, 9, 6);
INSERT INTO `item_latents` VALUES (17559, 346, -2, 9, 8);
-- Earth Staff
INSERT INTO `item_latents` VALUES (17551, 346, 2, 9, 11);
INSERT INTO `item_latents` VALUES (17551, 346, 2, 9, 3);
-- Fire Staff
INSERT INTO `item_latents` VALUES (17545, 346, 2, 9, 10);
INSERT INTO `item_latents` VALUES (17545, 346, 2, 9, 0);
-- Ice Staff
INSERT INTO `item_latents` VALUES (17547, 346, 2, 9, 14);
INSERT INTO `item_latents` VALUES (17547, 346, 2, 9, 1);
-- Jupiter's Staff
INSERT INTO `item_latents` VALUES (17554, 346, 2, 9, 15);
INSERT INTO `item_latents` VALUES (17554, 346, 2, 9, 4);
-- Light Staff
INSERT INTO `item_latents` VALUES (17557, 346, -2, 9, 16);
INSERT INTO `item_latents` VALUES (17557, 346, -2, 9, 9);
INSERT INTO `item_latents` VALUES (17557, 346, -2, 9, 7);
INSERT INTO `item_latents` VALUES (17557, 346, 2, 9, 6);
INSERT INTO `item_latents` VALUES (17557, 346, 2, 9, 8);
-- Neptune's Staff
INSERT INTO `item_latents` VALUES (17556, 346, 2, 9, 12);
INSERT INTO `item_latents` VALUES (17556, 346, 2, 9, 5);
-- Pluto's Staff
INSERT INTO `item_latents` VALUES (17560, 346, 3, 9, 16);
INSERT INTO `item_latents` VALUES (17560, 346, 3, 9, 9);
INSERT INTO `item_latents` VALUES (17560, 346, 3, 9, 7);
INSERT INTO `item_latents` VALUES (17560, 346, -3, 9, 6);
INSERT INTO `item_latents` VALUES (17560, 346, -3, 9, 8);
-- Terra's Staff
INSERT INTO `item_latents` VALUES (17552, 346, 3, 9, 11);
INSERT INTO `item_latents` VALUES (17552, 346, 3, 9, 3);
-- Thunder Staff
INSERT INTO `item_latents` VALUES (17553, 346, 2, 9, 15);
INSERT INTO `item_latents` VALUES (17553, 346, 2, 9, 4);
-- Vulcan's Staff
INSERT INTO `item_latents` VALUES (17546, 346, 3, 9, 10);
INSERT INTO `item_latents` VALUES (17546, 346, 3, 9, 0);
-- Water Staff
INSERT INTO `item_latents` VALUES (17555, 346, 2, 9, 12);
INSERT INTO `item_latents` VALUES (17555, 346, 2, 9, 5);
-- Wind Staff
INSERT INTO `item_latents` VALUES (17549, 346, 2, 9, 13);
INSERT INTO `item_latents` VALUES (17549, 346, 2, 9, 2);
-- Unshomaru
INSERT INTO `item_latents` VALUES (18453, 8, 1, 16, 2);
INSERT INTO `item_latents` VALUES (18453, 8, 1, 16, 3);
INSERT INTO `item_latents` VALUES (18453, 8, 1, 16, 4);
INSERT INTO `item_latents` VALUES (18453, 8, 1, 16, 5);
-- Lyft Crossbow
INSERT INTO `item_latents` VALUES (19233, 13, 1, 16, 2);
INSERT INTO `item_latents` VALUES (19233, 73, 12, 16, 2);
INSERT INTO `item_latents` VALUES (19233, 13, 1, 16, 3);
INSERT INTO `item_latents` VALUES (19233, 73, 12, 16, 3);
INSERT INTO `item_latents` VALUES (19233, 13, 1, 16, 4);
INSERT INTO `item_latents` VALUES (19233, 73, 12, 16, 4);
INSERT INTO `item_latents` VALUES (19233, 13, 1, 16, 5);
INSERT INTO `item_latents` VALUES (19233, 73, 12, 16, 5);
-- Lyft Hexagun
INSERT INTO `item_latents` VALUES (19234, 11, 1, 16, 2);
INSERT INTO `item_latents` VALUES (19234, 11, 1, 16, 3);
INSERT INTO `item_latents` VALUES (19234, 11, 1, 16, 4);
INSERT INTO `item_latents` VALUES (19234, 11, 1, 16, 5);
-- Trainee Hammer
INSERT INTO `item_latents` VALUES (18855, 129, 1, 24, 0);
-- Trainee Sword
INSERT INTO `item_latents` VALUES (17764, 134, 1, 24, 0);
-- Trainee Needle
INSERT INTO `item_latents` VALUES (19110, 132, 1, 24, 0);
-- Trainee Specs
INSERT INTO `item_latents` VALUES (11499, 127, 1, 24, 0);
-- Frenzy Fife
INSERT INTO `item_latents` VALUES (17365, 8, 4, 25, 0);
-- Storm Fife
-- INSERT INTO `item_latents` VALUES (17851, 448, 2, ?, ?); -- Assault
-- Oliphant
INSERT INTO `item_latents` VALUES (17843, 121, 3, 53, 1);
-- Snakeeye +1
INSERT INTO `item_latents` VALUES (18709, 8, 6, 13, 3);
-- Snakeeye
INSERT INTO `item_latents` VALUES (18708, 8, 5, 13, 3);
-- Volunteer's Dart
-- INSERT INTO `item_latents` VALUES (18689, 23, 10, ?, ?); -- Besieged
-- Mercemary's Dart
-- INSERT INTO `item_latents` VALUES (18690, 12, 3, ?, ?); -- Besieged
-- INSERT INTO `item_latents` VALUES (18690, 13, 3, ?, ?); -- Besieged
-- INSERT INTO `item_latents` VALUES (18690, 14, 3, ?, ?); -- Besieged
-- Junior Musketeer's Chakram +2
INSERT INTO `item_latents` VALUES (18135, 8, 3, 53, 1);
-- Junior Musketeer's Chakram +1
INSERT INTO `item_latents` VALUES (18134, 8, 2, 53, 1);
-- Combat Caster's Boomerang +2
INSERT INTO `item_latents` VALUES (18133, 25, 5, 53, 1);
INSERT INTO `item_latents` VALUES (18133, 26, 5, 53, 1);
-- Combat Caster's Boomerang +1
INSERT INTO `item_latents` VALUES (18132, 25, 4, 53, 1);
INSERT INTO `item_latents` VALUES (18132, 26, 4, 53, 1);
-- Orphic Egg
INSERT INTO `item_latents` VALUES (18256, 25, 1, 25, 0);
INSERT INTO `item_latents` VALUES (18256, 23, 1, 25, 0);
INSERT INTO `item_latents` VALUES (18256, 68, 1, 25, 0);
-- Corbenic Sword
-- INSERT INTO `item_latents` VALUES (18892, 355, 43, ?, ?); -- 13 Weaponskills get 1 use
-- Oberon's Rapier
-- INSERT INTO `item_latents` VALUES (17761, 366, 6, ?, ?); -- Main Hand
-- Erlking's Sword
-- INSERT INTO `item_latents` VALUES (17762, 366, 4, ?, ?); -- Main Hand
-- Oberon's Rapier
-- INSERT INTO `item_latents` VALUES (17763, 366, 6, ?, ?); -- Main Hand
-- Lex Talionis
-- INSERT INTO `item_latents` VALUES (17763, 355, 239, ?, ?); -- Campaign
-- INSERT INTO `item_latents` VALUES (17763, 25, 20, ?, ?);
-- Griffinclaw
-- INSERT INTO `item_latents` VALUES (17684, 355, 238, ?, ?); -- Campaign
-- INSERT INTO `item_latents` VALUES (17684, 25, 20, ?, ?);
-- Mighty Talwar
INSERT INTO `item_latents` VALUES (17697, 366, 8, 31, 0);
INSERT INTO `item_latents` VALUES (17697, 56, 15, 31, 0);
-- Koggelmander
-- INSERT INTO `item_latents` VALUES (17759, 165, 7, ?, ?); -- Vs Vermin
-- Senior Gold Musketeer Scimitar
INSERT INTO `item_latents` VALUES (17530, 1, 10, 33, 1);
-- Storm Scimitar
-- INSERT INTO `item_latents` VALUES (17661, 366, 4, ?, ?); -- Assault
-- INSERT INTO `item_latents` VALUES (17661, 27, 5, ?, ?);
-- Rune Blade
INSERT INTO `item_latents` VALUES (16563, 366, 4, 12, 0);
INSERT INTO `item_latents` VALUES (16563, 9, 5, 12, 0);
INSERT INTO `item_latents` VALUES (16563, 370, -4, 12, 0);
-- Nightmare Sword
INSERT INTO `item_latents` VALUES (16563, 25, 12, 26, 1);
-- Corsair's Scimitar
-- INSERT INTO `item_latents` VALUES (17737, 23, 10, ?, ?); -- Besieged
-- INSERT INTO `item_latents` VALUES (17737, 24, 10, ?, ?);
-- Save the Queen
INSERT INTO `item_latents` VALUES (16604, 25, 20, 13, 114);
-- Singh Kilij
-- INSERT INTO `item_latents` VALUES (17723, 2, 20, ?, ?); -- Besieged
-- Temple Knight Army Sword +2
INSERT INTO `item_latents` VALUES (17671, 9, 4, 53, 1);
-- Temple Knight Army Sword +1
INSERT INTO `item_latents` VALUES (17670, 9, 3, 53, 1);
-- Tactician Magician's Espadon +2
INSERT INTO `item_latents` VALUES (17677, 2, 20, 53, 1);
-- Tactician Magician's Espadon +1
INSERT INTO `item_latents` VALUES (17676, 2, 18, 53, 1);
-- Musketeer's Sword +2
INSERT INTO `item_latents` VALUES (17681, 5, 20, 53, 1);
-- Musketeer's Sword +1
INSERT INTO `item_latents` VALUES (17680, 5, 18, 53, 1);
-- Junior Musketeer's Tuck +2
INSERT INTO `item_latents` VALUES (17667, 26, 5, 53, 1);
INSERT INTO `item_latents` VALUES (17667, 24, 5, 53, 1);
-- Junior Musketeer's Tuck +1
INSERT INTO `item_latents` VALUES (17666, 26, 4, 53, 1);
INSERT INTO `item_latents` VALUES (17666, 24, 4, 53, 1);
-- Musketeer Gun +1
INSERT INTO `item_latents` VALUES (17269, 25, 8, 53, 1);
-- Musketeer Gun +2
INSERT INTO `item_latents` VALUES (17270, 25, 9, 53, 1);
-- Combat Caster's Scimitar +2
INSERT INTO `item_latents` VALUES (17675, 5, 16, 53, 1);
-- Combat Caster's Scimitar +1
INSERT INTO `item_latents` VALUES (17674, 5, 14, 53, 1);
-- Windurstian Kukri
INSERT INTO `item_latents` VALUES (17978, 23, 7, 53, 1);
-- Federation Kukri
INSERT INTO `item_latents` VALUES (17979, 23, 7, 53, 1);
-- Windurstian Scarf
INSERT INTO `item_latents` VALUES (13142, 1, 7, 53, 1);
INSERT INTO `item_latents` VALUES (13142, 68, 7, 53, 1); 
-- Republic Sword
INSERT INTO `item_latents` VALUES (17673, 2, 6, 53, 1);
INSERT INTO `item_latents` VALUES (17673, 5, 6, 53, 1);
-- Bastokan Sword
INSERT INTO `item_latents` VALUES (17672, 2, 5, 53, 1);
INSERT INTO `item_latents` VALUES (17672, 5, 5, 53, 1);
-- Orison Duckbills +2
INSERT INTO `item_latents` VALUES (11146, 289, 10, 13, 275);
-- Orison Duckbills +1
INSERT INTO `item_latents` VALUES (11246, 289, 5, 13, 275);
-- Savant's Gown +1
INSERT INTO `item_latents` VALUES (11203, 27, -10, 13, 401);
INSERT INTO `item_latents` VALUES (11203, 27, -10, 13, 402);
-- Savant's Gown +2
INSERT INTO `item_latents` VALUES (11103, 27, -20, 13, 401);
INSERT INTO `item_latents` VALUES (11103, 27, -20, 13, 402);
-- Savant's Bonnet +2
INSERT INTO `item_latents` VALUES (11083, 374, 10, 13, 364);
-- Savant's Bonnet +1
INSERT INTO `item_latents` VALUES (11183, 374, 5, 13, 364);
-- Savant's Pants +2
INSERT INTO `item_latents` VALUES (11143, 30, 15, 13, 360);
INSERT INTO `item_latents` VALUES (11143, 30, 15, 13, 401);
INSERT INTO `item_latents` VALUES (11143, 30, 15, 13, 362);
INSERT INTO `item_latents` VALUES (11143, 30, 15, 13, 366);
INSERT INTO `item_latents` VALUES (11143, 30, 15, 13, 364);
INSERT INTO `item_latents` VALUES (11143, 30, 15, 13, 412);
INSERT INTO `item_latents` VALUES (11143, 30, 15, 13, 414);
INSERT INTO `item_latents` VALUES (11143, 30, 15, 13, 469);
INSERT INTO `item_latents` VALUES (11143, 30, 15, 13, 361);
INSERT INTO `item_latents` VALUES (11143, 30, 15, 13, 363);
INSERT INTO `item_latents` VALUES (11143, 30, 15, 13, 402);
INSERT INTO `item_latents` VALUES (11143, 30, 15, 13, 367);
INSERT INTO `item_latents` VALUES (11143, 30, 15, 13, 365);
INSERT INTO `item_latents` VALUES (11143, 30, 15, 13, 413);
INSERT INTO `item_latents` VALUES (11143, 30, 15, 13, 415);
INSERT INTO `item_latents` VALUES (11143, 30, 15, 13, 470);
-- Savant's Pants +1
INSERT INTO `item_latents` VALUES (11243, 30, 7, 13, 360);
INSERT INTO `item_latents` VALUES (11243, 30, 7, 13, 401);
INSERT INTO `item_latents` VALUES (11243, 30, 7, 13, 362);
INSERT INTO `item_latents` VALUES (11243, 30, 7, 13, 366);
INSERT INTO `item_latents` VALUES (11243, 30, 7, 13, 364);
INSERT INTO `item_latents` VALUES (11243, 30, 7, 13, 412);
INSERT INTO `item_latents` VALUES (11243, 30, 7, 13, 414);
INSERT INTO `item_latents` VALUES (11243, 30, 7, 13, 469);
INSERT INTO `item_latents` VALUES (11243, 30, 7, 13, 361);
INSERT INTO `item_latents` VALUES (11243, 30, 7, 13, 363);
INSERT INTO `item_latents` VALUES (11243, 30, 7, 13, 402);
INSERT INTO `item_latents` VALUES (11243, 30, 7, 13, 367);
INSERT INTO `item_latents` VALUES (11243, 30, 7, 13, 365);
INSERT INTO `item_latents` VALUES (11243, 30, 7, 13, 413);
INSERT INTO `item_latents` VALUES (11243, 30, 7, 13, 415);
INSERT INTO `item_latents` VALUES (11243, 30, 7, 13, 470);
-- Savant's Bracers +1
INSERT INTO `item_latents` VALUES (11223, 174, 5, 13, 470);
-- Savant's Bracers +2
INSERT INTO `item_latents` VALUES (11123, 174, 10, 13, 470);
-- Melee Gaiters
INSERT INTO `item_latents` VALUES (15133, 291, 5, 13, 61);
-- Melee Gaiters +1
INSERT INTO `item_latents` VALUES (15666, 291, 5, 13, 61);
-- Melee Gaiters +2
INSERT INTO `item_latents` VALUES (10731, 291, 10, 13, 61);
-- Unkai Sune-Ate +1
INSERT INTO `item_latents` VALUES (11255, 414, 5, 13, 440);
INSERT INTO `item_latents` VALUES (11255, 174, 5, 13, 440);
-- Unkai Sune-Ate +2
INSERT INTO `item_latents` VALUES (11155, 414, 10, 13, 440);
INSERT INTO `item_latents` VALUES (11155, 174, 10, 13, 440);
-- Saotome Haidate +2
INSERT INTO `item_latents` VALUES (10721, 291, 25, 13, 67);
-- Saotome Haidate +1
INSERT INTO `item_latents` VALUES (15591, 291, 15, 13, 67);
-- Saotome Haidate +1
INSERT INTO `item_latents` VALUES (15128, 291, 10, 13, 67);
-- Volitional Mantle
INSERT INTO `item_latents` VALUES (16220, 10, 10, 13, 114);
-- Charis Casaque +1
INSERT INTO `item_latents` VALUES (11202, 165, 25, 13, 468);
-- Charis Casaque +1
INSERT INTO `item_latents` VALUES (11102, 165, 50, 13, 468);
-- Warrior's Lornica
INSERT INTO `item_latents` VALUES (15087, 68, 12, 13, 58);
-- Warrior's Lornica +1
INSERT INTO `item_latents` VALUES (14500, 68, 12, 13, 58);
-- Warrior's Lornica +2
INSERT INTO `item_latents` VALUES (10670, 68, 12, 13, 58);
-- Sonia's Plectrum
-- INSERT INTO `item_latents` VALUES (18735, 477, 10, ?, ?); -- Campaign
-- Beast Bazubands
-- INSERT INTO `item_latents` VALUES (14958, ?, ?, ?, ?); -- Besieged -- Augments Call Beast
-- Bison Warbonnet
-- INSERT INTO `item_latents` VALUES (15157, 411, 5, ?, ?); -- Vs Beasts
-- Brave's Warbonnet
-- INSERT INTO `item_latents` VALUES (15158, 411, 6, ?, ?); -- Vs Beasts
-- Khimaira Bonnet
-- INSERT INTO `item_latents` VALUES (16104, 411, 6, ?, ?); -- Vs Beasts
-- Evoker's Doublet
INSERT INTO `item_latents` VALUES (12650, 54, 20, 21, 10);
INSERT INTO `item_latents` VALUES (12650, 54, 20, 21, 0);
INSERT INTO `item_latents` VALUES (12650, 55, 20, 21, 14);
INSERT INTO `item_latents` VALUES (12650, 55, 20, 21, 1);
INSERT INTO `item_latents` VALUES (12650, 56, 20, 21, 13);
INSERT INTO `item_latents` VALUES (12650, 56, 20, 21, 2);
INSERT INTO `item_latents` VALUES (12650, 57, 20, 21, 11);
INSERT INTO `item_latents` VALUES (12650, 57, 20, 21, 3);
INSERT INTO `item_latents` VALUES (12650, 58, 20, 21, 15);
INSERT INTO `item_latents` VALUES (12650, 58, 20, 21, 4);
INSERT INTO `item_latents` VALUES (12650, 59, 20, 21, 12);
INSERT INTO `item_latents` VALUES (12650, 59, 20, 21, 5);
INSERT INTO `item_latents` VALUES (12650, 60, 20, 21, 8);
INSERT INTO `item_latents` VALUES (12650, 60, 20, 21, 6);
INSERT INTO `item_latents` VALUES (12650, 61, 20, 21, 9);
INSERT INTO `item_latents` VALUES (12650, 61, 20, 21, 7);
INSERT INTO `item_latents` VALUES (12650, 61, 20, 21, 16);
-- Laevateinn - 75
INSERT INTO `item_latents` VALUES (18994, 418, 10, 13, 79);
-- Laevateinn - 80
INSERT INTO `item_latents` VALUES (19063, 418, 10, 13, 79);
-- Laevateinn - 85
INSERT INTO `item_latents` VALUES (19083, 418, 10, 13, 79);
-- Laevateinn - 90
INSERT INTO `item_latents` VALUES (19615, 418, 10, 13, 79);
-- Laevateinn - 95
INSERT INTO `item_latents` VALUES (19713, 418, 10, 13, 79);
-- Laevateinn - 99
INSERT INTO `item_latents` VALUES (19822, 418, 10, 13, 79);
-- Laevateinn - 99-2
INSERT INTO `item_latents` VALUES (19951, 418, 10, 13, 79);
-- Brisingamen
INSERT INTO `item_latents` VALUES (13097, 2, 10, 26, 0);
INSERT INTO `item_latents` VALUES (13097, 5, 10, 26, 1);
INSERT INTO `item_latents` VALUES (13097, 8, 5, 28, 0);
INSERT INTO `item_latents` VALUES (13097, 10, 5, 29, 0);
INSERT INTO `item_latents` VALUES (13097, 13, 5, 30, 0);
INSERT INTO `item_latents` VALUES (13097, 11, 5, 31, 0);
INSERT INTO `item_latents` VALUES (13097, 12, 5, 34, 0);
INSERT INTO `item_latents` VALUES (13097, 9, 5, 35, 0);
INSERT INTO `item_latents` VALUES (13097, 14, 5, 36, 0);
-- Brisingamen +1
INSERT INTO `item_latents` VALUES (13162, 2, 15, 26, 0);
INSERT INTO `item_latents` VALUES (13162, 5, 15, 26, 1);
INSERT INTO `item_latents` VALUES (13162, 8, 6, 28, 0);
INSERT INTO `item_latents` VALUES (13162, 10, 6, 29, 0);
INSERT INTO `item_latents` VALUES (13162, 13, 6, 30, 0);
INSERT INTO `item_latents` VALUES (13162, 11, 6, 31, 0);
INSERT INTO `item_latents` VALUES (13162, 12, 6, 34, 0);
INSERT INTO `item_latents` VALUES (13162, 9, 6, 35, 0);
INSERT INTO `item_latents` VALUES (13162, 14, 6, 36, 0);
-- Morgana Pigaches
INSERT INTO `item_latents` VALUES (11408, 313, 5, 9, 14);
-- Karura Hachigane
INSERT INTO `item_latents` VALUES (11408, 491, 10, 9, 13);
INSERT INTO `item_latents` VALUES (11408, 493, 10, 9, 13);
-- Forefront Cesti
-- INSERT INTO `item_latents` VALUES (20547, 366, 14, ?, ?); -- Reives
-- INSERT INTO `item_latents` VALUES (20547, 25, 25, ?, ?); -- Reives
-- Forefront Dagger
-- INSERT INTO `item_latents` VALUES (20635, 366, 13, ?, ?); -- Reives
-- INSERT INTO `item_latents` VALUES (20635, 25, 25, ?, ?); -- Reives
-- Forefront Blade
-- INSERT INTO `item_latents` VALUES (20737, 366, 18, ?, ?); -- Reives
-- INSERT INTO `item_latents` VALUES (20737, 25, 25, ?, ?); -- Reives
-- Forefront Claymore
-- INSERT INTO `item_latents` VALUES (20777, 366, 32, ?, ?); -- Reives
-- INSERT INTO `item_latents` VALUES (20777, 25, 25, ?, ?); -- Reives
-- Forefront Axe
-- INSERT INTO `item_latents` VALUES (20825, 366, 21, ?, ?); -- Reives
-- INSERT INTO `item_latents` VALUES (20825, 25, 25, ?, ?); -- Reives
-- Forefront Labrys
-- INSERT INTO `item_latents` VALUES (20871, 366, 36, ?, ?); -- Reives
-- INSERT INTO `item_latents` VALUES (20871, 25, 25, ?, ?); -- Reives
-- Forefront Scythe
-- INSERT INTO `item_latents` VALUES (20547, 366, 38, ?, ?); -- Reives
-- INSERT INTO `item_latents` VALUES (20547, 25, 25, ?, ?); -- Reives
-- Forefront Lance
-- INSERT INTO `item_latents` VALUES (20962, 366, 35, ?, ?); -- Reives
-- INSERT INTO `item_latents` VALUES (20962, 25, 25, ?, ?); -- Reives
-- Forefront Wand
-- INSERT INTO `item_latents` VALUES (21127, 366, 16, ?, ?); -- Reives
-- INSERT INTO `item_latents` VALUES (21127, 25, 25, ?, ?); -- Reives
-- Forefront Staff
-- INSERT INTO `item_latents` VALUES (21196, 30, 56, ?, ?); -- Reives
-- INSERT INTO `item_latents` VALUES (21196, 28, 60, ?, ?); -- Reives
-- Forefront Scepter
-- INSERT INTO `item_latents` VALUES (21197, 313, ?, ?, ?); -- Reives
-- INSERT INTO `item_latents` VALUES (21197, 312, 50, ?, ?); -- Reives
-- Forefront Bow
-- INSERT INTO `item_latents` VALUES (21237, 366, 31, ?, ?); -- Reives
-- INSERT INTO `item_latents` VALUES (21237, 26, 25, ?, ?); -- Reives
-- Forefront Bowgun
-- INSERT INTO `item_latents` VALUES (21255, 366, 18, ?, ?); -- Reives
-- INSERT INTO `item_latents` VALUES (21255, 26, 25, ?, ?); -- Reives
-- Forefront Gun
-- INSERT INTO `item_latents` VALUES (21287, 366, 15, ?, ?); -- Reives
-- INSERT INTO `item_latents` VALUES (21287, 26, 25, ?, ?); -- Reives
-- Rustic Trunks +1
INSERT INTO `item_latents` VALUES (28088, 68, 3, 52, 3);
-- Shoal Trunks +1
INSERT INTO `item_latents` VALUES (28089, 68, 3, 52, 3);
-- Forefront Flute
-- INSERT INTO `item_latents` VALUES (21409, 436, 2, ?, ?); -- Reives
-- Huactzin Shield
-- INSERT INTO `item_latents` VALUES (28660, 1, 11, ?, ?); -- Reives
-- Sorcerer's Ring
INSERT INTO `item_latents` VALUES (13289, 28, 10, 2, 76);
-- Opo-opo Crown
INSERT INTO `item_latents` VALUES (13870, 14, 14, 49, 4468);
INSERT INTO `item_latents` VALUES (13870, 12, 1, 49, 4468);
INSERT INTO `item_latents` VALUES (13870, 11, -3, 49, 4468);
INSERT INTO `item_latents` VALUES (13870, 8, -3, 49, 4468);
INSERT INTO `item_latents` VALUES (13870, 5, 50, 49, 4468);
INSERT INTO `item_latents` VALUES (13870, 2, 50, 49, 4468);
INSERT INTO `item_latents` VALUES (13870, 14, 14, 49, 4596);
INSERT INTO `item_latents` VALUES (13870, 12, 1, 49, 4596);
INSERT INTO `item_latents` VALUES (13870, 11, -3, 49, 4596);
INSERT INTO `item_latents` VALUES (13870, 8, -3, 49, 4596);
INSERT INTO `item_latents` VALUES (13870, 5, 50, 49, 4596);
INSERT INTO `item_latents` VALUES (13870, 2, 50, 49, 4596);
-- Mistilteinn
INSERT INTO `item_latents` VALUES (17073, 406, 3, 7, 2);
INSERT INTO `item_latents` VALUES (17073, 369, 1, 7, 2);
-- Glanzfaust 99 - 3
INSERT INTO `item_latents` VALUES (20482, 62, 25, 13, 59);
INSERT INTO `item_latents` VALUES (20482, 165, 25, 13, 59);
INSERT INTO `item_latents` VALUES (20482, 291, 13, 13, 60);
-- Glanzfaust 99 - 4
INSERT INTO `item_latents` VALUES (20483, 62, 25, 13, 59);
INSERT INTO `item_latents` VALUES (20483, 165, 25, 13, 59);
INSERT INTO `item_latents` VALUES (20483, 291, 13, 13, 60);
-- Cama. Knuckles
-- INSERT INTO `item_latents` VALUES (20545, 288, 15, ?, ?); -- Revives occ atk 2x
-- Mindmeld Kris
-- INSERT INTO `item_latents` VALUES (20628, 366, 8, ?, ?); -- latent ??
-- Cama. Dagger
-- INSERT INTO `item_latents` VALUES (20633, 288, 15, ?,?); -- Revives occ atk 2x
-- Valor Surcoat +1
INSERT INTO `item_latents` VALUES (14506, 600, 20, 13, 114);
-- Valor Surcoat +2
INSERT INTO `item_latents` VALUES (10676, 600, 30, 13, 114);
-- Affinity Earring
INSERT INTO `item_latents` VALUES (16031, 314, 1, 22, 18);
-- Ardent Earring
INSERT INTO `item_latents` VALUES (16017, 28, 1, 22, 4);
-- Ataraxy Earring
INSERT INTO `item_latents` VALUES (16018, 30, 1, 22, 5);
-- Booster Earring
INSERT INTO `item_latents` VALUES (16029, 2, 10, 22, 16);
INSERT INTO `item_latents` VALUES (16029, 5, 10, 22, 16);
-- Brawn Earring
INSERT INTO `item_latents` VALUES (16021, 8, 1, 22, 8);
-- Chary Earring
INSERT INTO `item_latents` VALUES (16016, 27, -1, 22, 3);
-- Esse Earring
INSERT INTO `item_latents` VALUES (16015, 2, 20, 22, 2);
-- Elan Earring
INSERT INTO `item_latents` VALUES (16026, 68, 4, 22, 13);
-- Fidelity Earring
INSERT INTO `item_latents` VALUES (16022, 315, 1, 22, 9);
-- Forte Earring
INSERT INTO `item_latents` VALUES (16019, 9, 1, 22, 6);
-- Impetus Earring
INSERT INTO `item_latents` VALUES (16024, 24, 1, 22, 11);
-- Muffle Earring
INSERT INTO `item_latents` VALUES (16032, 289, 3, 22, 19);
-- Mystique Earring
INSERT INTO `item_latents` VALUES (16023, 14, 1, 22, 10);
-- Psyche Earring
INSERT INTO `item_latents` VALUES (16028, 5, 15, 22, 15); 
-- Rathe Earring
INSERT INTO `item_latents` VALUES (16025, 73, 1, 22, 12);
-- Seeker Earring
INSERT INTO `item_latents` VALUES (16027, 25, 1, 22, 14);
-- Soarer Earring
INSERT INTO `item_latents` VALUES (16030, 26, 1, 22, 17);
-- Stormer Earring
INSERT INTO `item_latents` VALUES (16014, 23, 4, 22, 1);
-- Survivor Earring
INSERT INTO `item_latents` VALUES (16020, 10, 1, 22, 7);
-- Sylph Earring
INSERT INTO `item_latents` VALUES (16033, 71, 1, 22, 20);
-- Nyx Gorget        
INSERT INTO `item_latents` VALUES (11587, 25, 12, 13, 75);
-- Aesir Torque
INSERT INTO `item_latents` VALUES (11589, 115, 3, 32, 0);
INSERT INTO `item_latents` VALUES (11589, 116, 3, 32, 0);
-- Vampire Earring           
INSERT INTO `item_latents` VALUES (14783, 8, 4, 26, 1);
INSERT INTO `item_latents` VALUES (14783, 10, 4, 26, 1);
-- Ladybug Earring           
INSERT INTO `item_latents` VALUES (15996, 24, 3, 26, 0);
-- Ladybug Earring +1        
INSERT INTO `item_latents` VALUES (15997, 24, 4, 26, 0);
-- Mercenary Major's Charm
-- INSERT INTO `item_latents` VALUES (11588, 369, 1, ?, ?); -- Campaign
-- Earthy Belt
-- INSERT INTO `item_latents` VALUES (15936, 43, 15, ?, ?); -- Campaign
-- Temple Earring
-- INSERT INTO `item_latents` VALUES (15967, 46, 15, ?, ?); -- Campaign
-- Oneiros Tathlum
INSERT INTO `item_latents` VALUES (15967, 23, 6, 13, 199);
-- Titanis Earring
INSERT INTO `item_latents` VALUES (14765, 27, 4, 13, 197);
-- Minuet Earring
INSERT INTO `item_latents` VALUES (14764, 25, 3, 13, 198);
-- Rajas Ring
INSERT INTO `item_latents` VALUES (15543, 8, 1, 51, 45);
INSERT INTO `item_latents` VALUES (15543, 8, 1, 51, 60);
INSERT INTO `item_latents` VALUES (15543, 8, 1, 51, 75);
INSERT INTO `item_latents` VALUES (15543, 9, 1, 51, 45);
INSERT INTO `item_latents` VALUES (15543, 9, 1, 51, 60);
INSERT INTO `item_latents` VALUES (15543, 9, 1, 51, 75);
-- Tamas Ring    
INSERT INTO `item_latents` VALUES (15544, 2, 5, 51, 45);
INSERT INTO `item_latents` VALUES (15544, 2, 5, 51, 60);
INSERT INTO `item_latents` VALUES (15544, 2, 5, 51, 75);
INSERT INTO `item_latents` VALUES (15544, 10, 1, 51, 45);
INSERT INTO `item_latents` VALUES (15544, 10, 1, 51, 60);
INSERT INTO `item_latents` VALUES (15544, 10, 1, 51, 75);
INSERT INTO `item_latents` VALUES (15544, 11, 1, 51, 45);
INSERT INTO `item_latents` VALUES (15544, 11, 1, 51, 60);
INSERT INTO `item_latents` VALUES (15544, 11, 1, 51, 75);
-- Sattva Ring
INSERT INTO `item_latents` VALUES (15545, 5, 5, 51, 45);
INSERT INTO `item_latents` VALUES (15545, 5, 5, 51, 60);
INSERT INTO `item_latents` VALUES (15545, 5, 5, 51, 75);
INSERT INTO `item_latents` VALUES (15545, 12, 1, 51, 45);
INSERT INTO `item_latents` VALUES (15545, 12, 1, 51, 60);
INSERT INTO `item_latents` VALUES (15545, 12, 1, 51, 75);
INSERT INTO `item_latents` VALUES (15545, 13, 1, 51, 45);
INSERT INTO `item_latents` VALUES (15545, 13, 1, 51, 60);
INSERT INTO `item_latents` VALUES (15545, 13, 1, 51, 75);
-- Parade Gorget
INSERT INTO `item_latents` VALUES (15506, 369, 1, 1, 85);
-- Caitiff's Socks
-- INSERT INTO `item_latents` VALUES (15324, ???, 1, 2, 25); -- Flee when HP <25% and TP <100%
-- Medicine Ring
INSERT INTO `item_latents` VALUES (13288, 374, 10, 2, 75);
-- Guardian's Ring   
INSERT INTO `item_latents` VALUES (13292, 385, 10, 2, 75);
-- Drake Ring  
INSERT INTO `item_latents` VALUES (13299, 361, 10, 2, 75);
-- Conjurer's Earring 
INSERT INTO `item_latents` VALUES (13433, 160, -20, 2, 25);
-- Gaudy Harness
INSERT INTO `item_latents` VALUES (14413, 369, 1, 5, 49);
-- Ninja Kyahan
INSERT INTO `item_latents` VALUES (14101, 169, 25, 26, 1);
-- Artemis Medal
INSERT INTO `item_latents` VALUES (11607, 30, 10, 59, 0);
INSERT INTO `item_latents` VALUES (11607, 30, 5, 37, 0);
INSERT INTO `item_latents` VALUES (11607, 30, 5, 62, 0);
INSERT INTO `item_latents` VALUES (11607, 28, 5, 37, 0);
INSERT INTO `item_latents` VALUES (11607, 28, 5, 62, 0);
INSERT INTO `item_latents` VALUES (11607, 30, 1, 65, 0);
INSERT INTO `item_latents` VALUES (11607, 28, 10, 65, 0);
INSERT INTO `item_latents` VALUES (11607, 28, 6, 63, 0);
INSERT INTO `item_latents` VALUES (11607, 28, 8, 64, 0);
INSERT INTO `item_latents` VALUES (11607, 28, 1, 60, 0);
INSERT INTO `item_latents` VALUES (17433, 28, 3, 61, 0);
-- Master Caster's Bow
INSERT INTO `item_latents` VALUES (18145, 26, 7, 33, 2);
-- Master Caster's Knife
INSERT INTO `item_latents` VALUES (18145, 1, 10, 33, 2);
-- Reserve Captian's Great Sword
INSERT INTO `item_latents` VALUES (16953, 24, 7, 33, 0);
-- Bastokan Cuisses
INSERT INTO `item_latents` VALUES (14267, 23, 5, 53, 0);
INSERT INTO `item_latents` VALUES (14267, 24, 5, 53, 0);
-- Republic Cuisess
INSERT INTO `item_latents` VALUES (14268, 23, 7, 53, 0);
INSERT INTO `item_latents` VALUES (14268, 24, 7, 53, 0);
-- Windurstian Gaiters
INSERT INTO `item_latents` VALUES (14153, 23, 5, 53, 0);
INSERT INTO `item_latents` VALUES (14153, 24, 5, 53, 0);
-- Federation Gaiters
INSERT INTO `item_latents` VALUES (14154, 24, 7, 53, 0);
INSERT INTO `item_latents` VALUES (14154, 23, 7, 53, 0);
-- Windurstian Gloves
INSERT INTO `item_latents` VALUES (14045, 23, 5, 53, 0);
INSERT INTO `item_latents` VALUES (14045, 24, 5, 53, 0);
-- Federation Gloves
INSERT INTO `item_latents` VALUES (14046, 23, 7, 53, 0);
INSERT INTO `item_latents` VALUES (14046, 24, 7, 53, 0);
-- Royal Squire's Chainmail +1
INSERT INTO `item_latents` VALUES (14340, 23, 6, 53, 0);
INSERT INTO `item_latents` VALUES (14340, 24, 6, 53, 0);
-- Royal Squire's Chainmail +2
INSERT INTO `item_latents` VALUES (14341, 23, 8, 53, 0);
INSERT INTO `item_latents` VALUES (14341, 24, 8, 53, 0);
-- Royal Knight's Belt +1
INSERT INTO `item_latents` VALUES (13277, 24, 5, 53, 0);
-- Royal Knight's Belt +2
INSERT INTO `item_latents` VALUES (13278, 24, 6, 53, 0);
-- Iron Musketeer's Gambison +1
INSERT INTO `item_latents` VALUES (14356, 1, 6, 53, 0);
-- Iron Musketeer's Gambison +2
INSERT INTO `item_latents` VALUES (14357, 1, 8, 53, 0);
-- Bastokan Greaves
INSERT INTO `item_latents` VALUES (14147, 25, 3, 53, 0);
INSERT INTO `item_latents` VALUES (14147, 26, 3, 53, 0);
-- Republic Greaves
INSERT INTO `item_latents` VALUES (14148, 25, 4, 53, 0);
INSERT INTO `item_latents` VALUES (14148, 26, 4, 53, 0);
-- Tactician Magician's Cuffs +1
INSERT INTO `item_latents` VALUES (14049, 25, 5, 53, 0);
-- Tactician Magician's Cuffs +2
INSERT INTO `item_latents` VALUES (14050, 25, 6, 53, 0);
-- Royal Knight's Mufflers +1
INSERT INTO `item_latents` VALUES (14029, 26, 5, 53, 0);
-- Royal Knight's Mufflers +2
INSERT INTO `item_latents` VALUES (14030, 26, 6, 53, 0);
-- Bastokan Scale Mail
INSERT INTO `item_latents` VALUES (14346, 108, 3, 53, 0);
-- Republic Scale Mail
INSERT INTO `item_latents` VALUES (14347, 108, 4, 53, 0);
-- Combat Caster's Cloak +1
INSERT INTO `item_latents` VALUES (14354, 108, 4, 53, 0);
-- Combat Caster's Cloak +2
INSERT INTO `item_latents` VALUES (14355, 108, 6, 53, 0);
-- Iron Musketeer's Cuirass +1
INSERT INTO `item_latents` VALUES (14342, 108, 6, 53, 0);
INSERT INTO `item_latents` VALUES (14342, 2, 6, 53, 0);
-- Iron Musketeer's Cuirass +2
INSERT INTO `item_latents` VALUES (14343, 108, 7, 53, 0);
INSERT INTO `item_latents` VALUES (14343, 2, 7, 53, 0);
-- Combat Caster's Shoes +1
INSERT INTO `item_latents` VALUES (14155, 11, 2, 53, 0);
-- Combat Caster's Shoes +2
INSERT INTO `item_latents` VALUES (14156, 11, 3, 53, 0);
-- Iron Musketeer's Sabatons +1
INSERT INTO `item_latents` VALUES (14143, 11, 2, 53, 0);
-- Iron Musketeer's Sabatons +2
INSERT INTO `item_latents` VALUES (14144, 11, 3, 53, 0);
-- Royal Knight's Cloak +1
INSERT INTO `item_latents` VALUES (14360, 11, 3, 53, 0);
-- Royal Knight's Cloak +2
INSERT INTO `item_latents` VALUES (14361, 11, 4, 53, 0);
-- Windurstian Doublet
INSERT INTO `item_latents` VALUES (14352, 14, 1, 53, 0);
-- Federation Doublet
INSERT INTO `item_latents` VALUES (14353, 14, 2, 53, 0);
-- San d'Orian Sollerets
INSERT INTO `item_latents` VALUES (14141, 9, 1, 53, 0);
-- Kingdom Sollerets
INSERT INTO `item_latents` VALUES (14142, 9, 2, 53, 0);
-- Royal Squire's Robe +1
INSERT INTO `item_latents` VALUES (14358, 12, 2, 53, 0);
-- Royal Squire's Robe +2
INSERT INTO `item_latents` VALUES (14359, 12, 3, 53, 0);
-- Tactician Magician's Hat +1
INSERT INTO `item_latents` VALUES (13905, 12, 3, 53, 0);
INSERT INTO `item_latents` VALUES (13905, 13, 3, 53, 0);
-- Tactician Magician's Hat +2
INSERT INTO `item_latents` VALUES (13906, 12, 4, 53, 0);
INSERT INTO `item_latents` VALUES (13906, 13, 4, 53, 0);
-- Tactician Magician's Pigaches +1
INSERT INTO `item_latents` VALUES (14157, 12, 3, 53, 0);
-- Tactician Magician's Pigaches +2
INSERT INTO `item_latents` VALUES (14158, 12, 4, 53, 0);
-- Tactician Magician's Slops +1
INSERT INTO `item_latents` VALUES (14277, 13, 3, 53, 0);
-- Tactician Magician's Slops +2
INSERT INTO `item_latents` VALUES (14278, 13, 4, 53, 0);
-- Bastokan Finger Gauntlets
INSERT INTO `item_latents` VALUES (14039, 8, 1, 53, 0);
-- Federation Finger Gauntlets
INSERT INTO `item_latents` VALUES (14040, 8, 2, 53, 0);
-- Iron Musketeer's Gauntlets +1
INSERT INTO `item_latents` VALUES (14035, 8, 2, 53, 0);
-- Iron Musketeer's Gauntlets +2
INSERT INTO `item_latents` VALUES (14036, 8, 3, 53, 0);
-- Royal Knight Army Shield +1
INSERT INTO `item_latents` VALUES (12368, 8, 3, 53, 0);
-- Royal Knight Army Shield +2
INSERT INTO `item_latents` VALUES (12369, 8, 4, 53, 0);
-- Royal Knight's Sollerets +1
INSERT INTO `item_latents` VALUES (14137, 10, 3, 53, 0);
-- Royal Knight's Sollerets +2
INSERT INTO `item_latents` VALUES (14138, 10, 4, 53, 0);
-- Temple Knight Army Shield +1
INSERT INTO `item_latents` VALUES (12376, 10, 3, 53, 0);
-- Temple Knight Army Shield +2
INSERT INTO `item_latents` VALUES (12377, 10, 4, 53, 0);
-- Bastokan Visor
INSERT INTO `item_latents` VALUES (13897, 2, 5, 53, 0);
INSERT INTO `item_latents` VALUES (13897, 5, 5, 53, 0);
-- Republic Visor
INSERT INTO `item_latents` VALUES (13898, 2, 6, 53, 0);
INSERT INTO `item_latents` VALUES (13898, 5, 6, 53, 0);
-- Windurstian Brais
INSERT INTO `item_latents` VALUES (14271, 2, 10, 53, 0);
-- Federation Brais
INSERT INTO `item_latents` VALUES (14272, 2, 12, 53, 0);
-- San d'Orian Helm
INSERT INTO `item_latents` VALUES (13891, 2, 10, 53, 0);
-- Kingdom Helm
INSERT INTO `item_latents` VALUES (13892, 2, 12, 53, 0);
-- Combat Caster's Mitts +1
INSERT INTO `item_latents` VALUES (14047, 2, 14, 53, 0);
-- Combat Caster's Mitts +2
INSERT INTO `item_latents` VALUES (14048, 2, 16, 53, 0);
-- Royal Squire's Shield +1
INSERT INTO `item_latents` VALUES (12366, 2, 7, 53, 0);
INSERT INTO `item_latents` VALUES (12366, 5, 7, 53, 0);
-- Royal Squire's Shield +2
INSERT INTO `item_latents` VALUES (12367, 2, 8, 53, 0);
INSERT INTO `item_latents` VALUES (12367, 5, 8, 53, 0);
-- San d'Orian Mufflers
INSERT INTO `item_latents` VALUES (14033, 5, 10, 53, 0);
-- Kingdom Mufflers
INSERT INTO `item_latents` VALUES (14034, 5, 12, 53, 0);
-- Windurstian Headgear
INSERT INTO `item_latents` VALUES (13903, 5, 10, 53, 0);
-- Federation Headgear
INSERT INTO `item_latents` VALUES (13904, 5, 12, 53, 0);
-- Iron Musketeer's Armet +1
INSERT INTO `item_latents` VALUES (13893, 5, 14, 53, 0);
-- Iron Musketeer's Armet +2
INSERT INTO `item_latents` VALUES (13894, 5, 16, 53, 0);
-- Tactician Magician's Coat +1
INSERT INTO `item_latents` VALUES (14362, 5, 18, 53, 0);
-- Tactician Magician's Coat +2
INSERT INTO `item_latents` VALUES (14363, 5, 20, 53, 0);
-- Bastokan Circlet
INSERT INTO `item_latents` VALUES (13899, 3, 15, 53, 0);
-- Republic Circlet
INSERT INTO `item_latents` VALUES (13900, 5, 15, 53, 0);
-- Dandy Spectacles
INSERT INTO `item_latents` VALUES (16132, 25, -20, 26, 1);
-- Fancy Spectacles
INSERT INTO `item_latents` VALUES (16133, 25, -30, 26, 1);
-- Fenrir's Earring
INSERT INTO `item_latents` VALUES (13399, 23, 10, 26, 0);
INSERT INTO `item_latents` VALUES (13399, 24, 10, 26, 1);
-- Garden Bangles
INSERT INTO `item_latents` VALUES (14065, 370, 1, 26, 0);
-- Feronia's Bangles
INSERT INTO `item_latents` VALUES (14066, 370, 1, 26, 0);
-- Louhi's Mask
INSERT INTO `item_latents` VALUES (11474, 161, -3, 26, 0);
INSERT INTO `item_latents` VALUES (11474, 163, -3, 26, 1);
-- Vampire Cloak
INSERT INTO `item_latents` VALUES (14443, 370, 1, 26, 0);
INSERT INTO `item_latents` VALUES (14443, 369, 1, 26, 1);
-- Eerie Cloak +1
INSERT INTO `item_latents` VALUES (11300, 369, 1, 40, 0);
-- Ninja Hakama
INSERT INTO `item_latents` VALUES (14226, 68, 10, 26, 1);
-- Ninja Hakama +1
INSERT INTO `item_latents` VALUES (15573, 68, 10, 26, 2);
-- Nightmare Sword
INSERT INTO `item_latents` VALUES (17649, 25, 12, 26, 1);
-- Vampire Boots
INSERT INTO `item_latents` VALUES (15338, 68, 10, 26, 1);
-- Vampire Mask
INSERT INTO `item_latents` VALUES (15197, 25, 3, 26, 1);
-- Ninja Kyahan +1
INSERT INTO `item_latents` VALUES (15364, 169, 25, 26, 2);
-- she-slime_hat
INSERT INTO `item_latents` VALUES (27726, 370, 1, 13, 2);
INSERT INTO `item_latents` VALUES (27726, 370, 1, 13, 19);