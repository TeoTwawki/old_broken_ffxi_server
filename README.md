# Gas Giant Project #

Welcome to the Gas Giant project, an extension of the DarkStar project.

** Goals: **

* Add additional content that DSP says it isn't ready for yet.

* Maintain compatibility w/both DSP and retail where we can.

* Allow for a much higher degree of customization.


### Repository URL: ###

 https://bitbucket.org/gas-giant_dev_team/gas-giant-server.git


### Forums: ###

 https://tinyurl.com/gas-giant-forum


## Bug Reports ##

We currently use the issue tracker built into BitBucket for bug reports and feature requests.

* You can find the tracker here: https://bitbucket.org/gas-giant_dev_team/gas-giant-server/issues