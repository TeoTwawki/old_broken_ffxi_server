-----------------------------------
-- Area: Kamihr drifts
-- NM: Icy_palissade
-- @zone 267
-- @pos many
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/missions");
require("scripts/globals/titles");
require("scripts/globals/status");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function onMobSpawn(mob)
end;


-----------------------------------
-- onMobEngaged
-----------------------------------
function onMobEngaged(mob,target)
end;

-----------------------------------
-- onMobDeath Action
-----------------------------------

function onMobDeath(mob,killer)

	local A = GetMobAction(17871060);
	local B = GetMobAction(17871061);
	local D = GetMobAction(17871068);
	local E = GetMobAction(17871069);
	local G = GetMobAction(17871076);
	local H = GetMobAction(17871077);
	local J = GetMobAction(17871084);
	local K = GetMobAction(17871085);

	local C = A+B;
	local F = D+E;
	local I = G+H;
	local L = J+K;

	if(C >= 42 and C <= 50)then
		GetNPCByID(17871175):openDoor(3600);
		DespawnMob(17871062);
		DespawnMob(17871063);
		DespawnMob(17871064);
		DespawnMob(17871065);
		DespawnMob(17871066);
		DespawnMob(17871067);
	elseif(F >= 42 and F <= 50)then
		GetNPCByID(17871176):openDoor(3600);
		DespawnMob(17871070);
		DespawnMob(17871071);
		DespawnMob(17871072);
		DespawnMob(17871073);
		DespawnMob(17871074);
		DespawnMob(17871075);
	elseif(I >= 42 and I <= 50)then
		GetNPCByID(17871177):openDoor(3600);
		DespawnMob(17871078);
		DespawnMob(17871079);
		DespawnMob(17871080);
		DespawnMob(17871081);
		DespawnMob(17871082);
		DespawnMob(17871083);
	elseif(L >= 42 and L <= 50)then
		GetNPCByID(17871178):openDoor(3600);
		DespawnMob(17871086);
		DespawnMob(17871087);
		DespawnMob(17871088);
		DespawnMob(17871089);
		DespawnMob(17871090);
		DespawnMob(17871091);
	end
end;