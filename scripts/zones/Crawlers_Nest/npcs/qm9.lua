-----------------------------------
-- Area: Crawlers Nest
-- NPC: ??? (qm8)
-- Quests: A Boy's Dream
-- @zone 197
-- @pos -26.488 -11.149 122.158
-----------------------------------
package.loaded["scripts/zones/Crawlers_Nest/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/quests");
require("scripts/globals/settings");
require("scripts/zones/Crawlers_Nest/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	local DreadbugTimer = player:getVar("DreadbugNM_Timer");
	local DreadbugDay = player:getVar("DreadbugNM_Day");
	local MyDay = VanadielDayOfTheYear();
	local aBoysDream = player:getQuestStatus(SANDORIA, A_BOY_S_DREAM);

	if(MyDay ~= DreadbugDay and aBoysDream == QUEST_ACCEPTED) then
		local canSpawn = (os.time() - DreadbugTimer) > 30;

		if(canSpawn) then
			SpawnMob(17584425,168):updateEnmity(player); -- Despawn after 3 minutes (-12 seconds for despawn delay).
			player:setVar("DreadbugNM_Timer",os.time()+180);
			player:setVar("DreadbugNM_Day",VanadielDayOfTheYear());
			player:messageSpecial(SENSE_OF_FOREBODING);
		else
			player:messageSpecial(NOTHING_SEEMS_TO_HAPPEN);
		end
	else
		player:messageSpecial(NOTHING_WILL_HAPPEN_YET);
	end
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
end;
