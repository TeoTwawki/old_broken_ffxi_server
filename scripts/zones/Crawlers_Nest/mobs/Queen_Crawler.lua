-----------------------------------
-- Area: Crawlers Nest
-- NM: Queen Crawler
-- @zone 197
-- @pos -337.156 -3.607 -253.294
-- Note: Spawned by trading a
-- Rolanberry 874 to the basket at
-- D-7 on the third map. Does not
-- spawn 100% of the time; can
-- display the message "Nothing
-- Happened", which still uses up
-- a Rolanberry 874.
-----------------------------------


-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)
	GetNPCByID(17584457):hideNPC(900); -- qm5
end;