-----------------------------------
-- Area: Cloister of Flames
-- NPC: Fire Protocrystal
-- Quests: Trial by Fire, Trial
-- Size Trial by Fire
-- @zone 207
-- @pos -722.131 -1.400 -597.955
-----------------------------------
package.loaded["scripts/zones/Cloister_of_Flames/TextIDs"] = nil;
package.loaded["scripts/globals/bcnm"] = nil;
-----------------------------------

require("scripts/globals/keyitems");
require("scripts/globals/bcnm");
require("scripts/zones/Cloister_of_Flames/TextIDs");

	---- 0: 
	---- 1: Trial by Fire (Ifirit Avatar Fight)
	---- 2: 
	---- 3: Sugar Coated Directive (ASA Mission 4)

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)

	if(TradeBCNM(player,player:getZone(),trade,npc))then
		return;
	end

end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	if(EventTriggerBCNM(player,npc))then
		return;
	else
		player:messageSpecial(PROTOCRYSTAL);
	end

end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("onUpdate CSID: %u",csid);
	-- printf("onUpdate RESULT: %u",option);

	if(EventUpdateBCNM(player,csid,option))then
		return;
	end

end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("onFinish CSID: %u",csid);
	-- printf("onFinish RESULT: %u",option);

	if (csid == 0x7D02) then
		player:setVar("ASA_ifrit_done",1);
	elseif (csid == 0x0002) then
		player:setVar("ASA_ifrit_done",3);
		player:delKeyItem(DOMINAS_SCARLET_SEAL);
		player:addKeyItem(SCARLET_COUNTERSEAL);
		player:messageSpecial(KEYITEM_OBTAINED,SCARLET_COUNTERSEAL);
	elseif (EventFinishBCNM(player,csid,option)) then
		return;
	end

end;