-----------------------------------
-- Area: Sauromugue Champaign
-- HNM: Roc
-- @zone 120
-- @pos 232.000, -0.010, -327.000
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/titles");

-----------------------------------
-- onMobFight Action
-----------------------------------

function onMobFight(mob,target)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)	

    killer:addTitle(ROC_STAR);

    -- Set Roc's spawnpoint and respawn time (21-24 hours)
    UpdateNMSpawnPoint(mob:getID());
    mob:setRespawnTime((math.random((75600),(86400))) /LongNM_TIMER_MOD);

end;
