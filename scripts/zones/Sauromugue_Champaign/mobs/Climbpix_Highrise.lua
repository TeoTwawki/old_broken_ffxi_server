-----------------------------------
-- Area: Sauromugue Champaign
-- NM: Climbpix Highrise
-- @zone 120
-- @pos 481.000 23.461 122.486
-----------------------------------

require("/scripts/globals/fieldsofvalor");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)
	checkRegime(killer,mob,97,2);
	checkRegime(killer,mob,98,2);
end;
