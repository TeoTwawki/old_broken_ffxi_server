-----------------------------------
-- Area: Buburimu Peninsula
-- MOB:  Buburimboo
-- Quests: The Miraculous Dale
-- @zone 118
-- @pos 444.916 19.499 154.289
-----------------------------------	

require("scripts/globals/settings");
require("scripts/globals/quests");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)

	-- Set Buburimboo's Window Open Time
	local wait = math.random((3600),(7200));
	SetServerVariable("[POP]Buburimboo", os.time(t) + (wait /NM_TIMER_MOD)); -- 1-2 hours
	DeterMob(mob:getID(), true);

	-- Set PH back to normal, then set to respawn spawn
	local PH = GetServerVariable("[PH]Buburimboo");
	SetServerVariable("[PH]Buburimboo", 0);
	DeterMob(PH, false);
	GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));

	local DALEQuest = player:getVar("DALEQuest");
	if(player:getQuestStatus(JEUNO,THE_MIRACULOUS_DALE) == QUEST_ACCEPTED) then
		DALEQuest = player:setMaskBit(DALEQuest,"DALEQuest",5,true);
	end

end;