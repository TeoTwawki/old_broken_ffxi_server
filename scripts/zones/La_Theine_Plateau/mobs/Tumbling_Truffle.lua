-----------------------------------	
-- Area: La Theine Plateau	
-- NM: Tumbling Truffle
-- Quests: The Miraculous Dale
-- @zone 102
-- @pos 434.000, 70.000, 241.000
-----------------------------------	
	
require("/scripts/globals/fieldsofvalor");	
require("scripts/globals/quests");
	
-----------------------------------	
-- onMobDeath	
-----------------------------------	
	
function onMobDeath(mob,killer)	
	checkRegime(killer,mob,71,2);
	local DALEQuest = player:getVar("DALEQuest");
	if(player:getQuestStatus(JEUNO,THE_MIRACULOUS_DALE) == QUEST_ACCEPTED) then
		DALEQuest = player:setMaskBit(DALEQuest,"DALEQuest",13,true);
	end
end;
