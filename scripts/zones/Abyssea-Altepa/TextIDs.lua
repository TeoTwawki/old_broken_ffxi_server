-- Area: Abyssea Altepa
-- Last updated: client ver. 30140910_0

-- Variable TextID   Description text

-- General Texts
ITEM_CANNOT_BE_OBTAINED = 6378; -- You cannot obtain the item <item> come back again after sorting your inventory
          ITEM_OBTAINED = 6381; -- Obtained: <item>
           GIL_OBTAINED = 6382; -- Obtained <number> gil
       KEYITEM_OBTAINED = 6384; -- Obtained key item: <keyitem>
         CRUOR_OBTAINED = 7465; -- <Possible Special Code: 1F>y<Player Name> obtains <Numeric Parameter 0> cruor.
            CRUOR_TOTAL = 6978; -- Obtained <Numeric Parameter 0> cruor. (Total: <Numeric Parameter 1>)<Prompt>

-- Dialog
MAUNADOLACE_GHETTO_DIALOG = 8194; -- No problem, hon. It's like the saying goes, you can lead a bedraggled adventurer to a smashing subligar, but you can't make <Multiple Choice (Player Gender)>[him/her] put them on.<Prompt>
    NOGELLE_GHETTO_DIALOG = 7935; -- Hm? Aren't there more importantaru matters to worry about, you say?<Prompt>
