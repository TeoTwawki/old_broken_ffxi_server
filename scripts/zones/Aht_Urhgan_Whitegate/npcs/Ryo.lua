-----------------------------------
-- Area: Aht Urhgan Whitegate
-- NPC: Ryo
-- Type: ZNM
-- @zone 50
-- @pos -127.086 0.999 22.693
-----------------------------------
package.loaded["scripts/zones/Aht_Urhgan_Whitegate/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/besieged");
require("scripts/zones/Aht_Urhgan_Whitegate/TextIDs");
require("scripts/globals/settings");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	player:startEvent(0x0391);
end;

function onEventSelection(player,csid,option)
	if (option == 300) then -- player asked about zeni balance
		player:updateEvent(player:getPoint(10),0);
	else
		player:updateEvent(0,0); -- first parameter serves as bitmask to delete options from main menu, needs to be reset
	end;
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
--printf("updateCSID: %u",csid);
--printf("updateRESULT: %u",option);

	if(option == 300) then
		player:updateEvent(player:getZeni(),0);
	else
		player:updateEvent(0,0);
	end

end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	--printf("finishCSID: %u",csid);
	--printf("finishRESULT: %u",option);
end;