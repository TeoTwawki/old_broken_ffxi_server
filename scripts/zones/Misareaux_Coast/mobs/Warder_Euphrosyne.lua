-----------------------------------
-- Area: Misareaux Coast
-- NM: Warder Euphrosyne
-- @zone 25
-- @pos 264.788 17.053 -415.758
-----------------------------------

require("scripts/globals/missions");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
	if(killer:getCurrentMission(COP) == A_PLACE_TO_RETURN and killer:getVar("PromathiaStatus") == 1)then 
		killer:setVar("Warder_Euphrosyne_KILL",1);
	end
end;