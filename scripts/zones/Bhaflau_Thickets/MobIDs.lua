-----------------------------------
-- Area: Bhaflau Thickets
-- Comments: -- posX, posY, posZ
--(Taken from 'mob_spawn_points' table)
-----------------------------------

-- Emergent Elm
Emergent_Elm=16990376;
Emergent_Elm_PH=
{ 
	[16990374] = '1', -- 86.000 -35.000 621.000
}; 

-- Mahishasura
Mahishasura=16990306;
Mahishasura_PH=
{ 
	[16990296] = '1', -- 215.000 -18.000 372.000
}

-- Nis Puk
Nis_Puk=16990403;
Nis_Puk_PH=
{ 
	[16990412] = '1', -- -145.000 -16.000 -610.000
}; 


