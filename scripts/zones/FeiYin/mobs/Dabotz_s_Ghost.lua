-----------------------------------
-- Area: FeiYin
-- NM: Dabotz's Ghost
-- Quests: Scattered in Shadow
-- @zone 204
-- @pos -170.000 0.100 245.000
-----------------------------------

require("scripts/globals/keyitems");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)

	if(killer:hasKeyItem(AQUAFLORA3)) then
		killer:setVar("DabotzKilled",1);
	end

end;