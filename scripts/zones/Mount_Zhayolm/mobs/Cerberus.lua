-----------------------------------
-- Area: Mount Zhayolm
-- NPC: Cerberus
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/titles");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
	killer:addTitle(CERBERUS_MUZZLER);
	mob:setRespawnTime((math.random((172800),(259200))) /HNM_TIMER_MOD); -- 48-72 hours
end;