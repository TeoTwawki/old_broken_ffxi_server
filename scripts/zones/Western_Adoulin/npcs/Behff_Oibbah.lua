-----------------------------------
-- Area: Western Adoulin
-- NPC: Behff Oibbah
-- Type: Patrol NPC
-- @zone 256
-- @pos 81.000 0.000 -18.000
-----------------------------------
package.loaded["scripts/zones/Western_Adoulin/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/zones/Western_Adoulin/TextIDs");
require("scripts/globals/pathfind");

-----------------------------------

local path = {
     126.519127 ,4.000000,-17.949345, 
     125.411697 ,4.000000,-17.982237, 
     124.391808 ,4.000000,-17.996605, 
     121.957954 ,4.000000,-17.999947, 
     91.931450 ,1.188560,-17.999987, 
     61.896976 ,0.000000,-17.999989, 
     62.926533 ,0.000000,-17.999989, 
     92.956779 ,1.291062,-17.999987, 
     100.714424 ,2.066827,-17.999987, 
     99.685104 ,1.963926,-17.999987, 
     69.657021 ,0.000000,-17.999987, 
     61.906887 ,0.000000,-17.999987, 
     62.938812 ,0.000000,-17.999987, 
     92.954178 ,1.290813,-17.999987, 
     122.971199 ,4.000000,-17.999987 
};

-----------------------------------
-- onSpawn Action
-----------------------------------

function onSpawn(npc)
	npc:initNpcAi();
	npc:setPos(pathfind.first(path));
	onPath(npc);
end;

-----------------------------------
-- onPath Action
-----------------------------------

function onPath(npc)
	pathfind.patrol(npc, path);
end;

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	player:startEvent(0x0202);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option,npc)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;