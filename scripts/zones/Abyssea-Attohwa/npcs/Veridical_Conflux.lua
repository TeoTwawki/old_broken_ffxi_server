-----------------------------------
-- Area: Abyssea Attohwa
-- NPC: Veridical Conflux
-- Type: Aybssea Teleport NPC
-- @zone 215
-- @pos -138.572 18.000 -207.557
-- @pos -486.873 -3.965 -6.661
-- @pos 265.254 20.293 -23.167
-- @pos -606.189 -4.538 191.950
-- @pos 467.491 20.243 75.615
-- @pos -246.715 14.099 286.005
-- @pos 379.743 20.000 -143.633
-- @pos 1.511 -3.850 148.714
-- @pos -280.000 -4.750 -6.000
-----------------------------------
package.loaded["scripts/globals/conflux"] = nil;
-----------------------------------

require("scripts/globals/keyitems");
require("scripts/globals/conflux");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end; 

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	local csid, param1, param2, param3, param4, param5, param6, param7, param8 = startConflux(player,npc);
	player:startEvent(csid, param1, param2, param3, param4, param5, param6, param7, param8);
	-- printf("csid:%u, param1:%u, param2:%u, param3:%u, param4:%u, param5:%u, param6:%u, param7:%u, param8:%u", csid, param1, param2, param3, param4, param5, param6, param7, param8);

end; 

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("Update: CSID: %u",csid);
	-- printf("Update: RESULT: %u",option);

	player:updateEvent(1,0,0,0,0,0,0,0);

end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("Finish: CSID: %u",csid);
	-- printf("Finish: RESULT: %u",option);

	finishConflux(player,csid,option);

end;