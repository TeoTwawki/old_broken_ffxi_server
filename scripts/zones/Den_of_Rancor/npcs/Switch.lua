-----------------------------------
-- Area: Den of Rancor
-- NPC: Switch
-- @zone 160
-- @pos -56.826 44.375 38.480
-----------------------------------

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end; 

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	GetNPCByID(17433050):openDoor(); -- Drop gate to Sacrificial Chamber   
end;