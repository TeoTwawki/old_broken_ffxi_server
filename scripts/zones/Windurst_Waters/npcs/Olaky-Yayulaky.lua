-----------------------------------
-- Area: Windurst Waters
-- NPC: Olaky-Yayulaky
-- Type: Event Item Storage NPC
-- @zone 238
-- @pos -61.247, -4.5, 72.551
-- player:startEvent(0x038E, Furnishing1, Weapons&Shields, Armor-Head, Armor-rest, 0, Furnishing2);
-----------------------------------
-- Items
-- Furnishings 1 - ALL             2147483647
-- 1. Sandorian Holiday Tree       1
-- 2. Bastokan Holiday Tree        2
-- 3. Windurstian Holiday Tree     4
-- 4. Kadomatsu                    8
-- 5. Wing Egg                     16
-- 6. Lamp Egg                     32
-- 7. Flower Egg                   64
-- 8. Adventuring Certificate      128
-- 9. Timepiece                    256
-- 10. Miniature Airship           512
-- 11. Pumpkin Lantern             1024
-- 12. Bomb Lantern                2048
-- 13. Mandragora Lantern          4096
-- 14. Dream Platter               8192
-- 15. Dream Coffer                16384
-- 16. Dream Stocking              32768
-- 17. Copy of Hoary Spire         65536
-- 18. Jeweled Egg                 131072
-- 19. Sprig of Red Bamboo Grass   262144
-- 20. Sprig of Blue Bamboo Grass  524288
-- 21. Sprig of Green Bamboo Grass 1048576
-- 22. Snowman Knight              2097152
-- 23. Snowman Miner               4194304
-- 24. Snowman Mage                8388608
-- 25. Bonbori                     16777216
-- 26. Set of Festive Dolls        33554432
-- 27. Melodious Egg               67108864
-- 28. Clockwork Egg               134217728
-- 29. Hatchling Egg               268435456
-- 30. Harpsichord                 536870912
-- 31. Aldebaran Horn              1073741824
--                                 
-- Furnishings 2 - ALL             16777215
-- 1. Stuffed Chocobo              1
-- 2. Egg Buffet                   2
-- 3. Adamantoise Statue           4
-- 4. Behemoth Statue              8
-- 5. Fafnir Statue                16
-- 6. Pepo Lantern                 32
-- 7. Cushaw Lantern               64
-- 8. Calabazilla Lantern          128
-- 9. Junoan Tree                  256
-- 10. Shadow Lord Statue          512
-- 11. Kabuto-kazari               1024
-- 12. Katana-kazari               2048
-- 13. Odin Statue                 4096
-- 14. Alexander Statue            8192
-- 15. Carillon Vermeil            16384
-- 16. Aeolsglockes                32768
-- 17. Leafbells                   65536
-- 18. San d'Orian Flag            131072
-- 19. Bastokan Flag               262144
-- 20. Windurstian Flag            524288
-- 21. Jack-o'-pricket             1048576
-- 22. Djinn Pricket               2097152
-- 23. Korrigan Pricket            4194304
-- 24. Mandragora Pricket          8388608
--                                 
-- Weapons & Shields - ALL         1048575
-- 1. Chocobo Wand                 1
-- 2. Trick Staff                  2
-- 3. Treat Staff                  4
-- 4. Treat Staff II               8
-- 5. Wooden Katana                16
-- 6. Hardwood Katana              32
-- 7. Pitchfork                    64
-- 8. Pitchfork +1                 128
-- 9. Charm Wand +1                256
-- 10. Lotus Katana                512
-- 11. Moogle Rod                  1024
-- 12. Battledore                  2048
-- 13. Miracle Wand +1             4096
-- 14. Shinai                      8192
-- 15. Ibushi Shinai               16384
-- 16. Ibushi Shinai +1            32768
-- 17. Town Moogle Shield          65536
-- 18. Nomad Moogle Shield         131072
-- 19. Dream Bell                  262144
-- 20. Dream Bell +1               524288
--                                 
-- Armor Helmets - ALL             524287
-- 1. Pumpkin Head                 1
-- 2. Horror Head                  2
-- 3. Pumpkin Head II              4
-- 4. Horror Head II               8
-- 5. Dream Hat                    16
-- 6. Dream Hat +1                 32
-- 7. Sprout Beret                 64
-- 8. Guide Beret                  128
-- 9. Mandragora Beret             256
-- 10. Witch Hat                   512
-- 11. Coven Hat                   1024
-- 12. Egg Helm                    2048
-- 13. Moogle Cap                  4096
-- 14. Nomad Cap                   8192
-- 15. Pair of Redeyes             16384
-- 16. Sol Cap                     32768
-- 17. Lunar Cap                   65536
-- 18. Snow Bunny Hat +1           131072
-- 19. Chocobo Beret               262144
--
-- Armor Other - ALL               8388607
-- 1. Onoko Yukata                 1
-- 2. Lords Yukata                 2
-- 3. Hume Gilet                   4
-- 4. Hume Gilet +1                8
-- 5. Pair of Hume Trunks          16
-- 6. Pair of Hume Trunks +1       32
-- 7. Dream Robe                   64
-- 8. Dream Robe +1                128
-- 9. Otoko Yukata                 256
-- 10. Otokogimi Yukata            512
-- 11. Pair of Dream Boots         1024
-- 12. Pair of Dream Boots +1      2048
-- 13. Custom Gilet                4096
-- 14. Custom Gilet +1             8192
-- 15. Pair of Custom Trunks       16384
-- 16. Pair of Custom Trunks +1    32768
-- 17. Eerie Cloak                 65536
-- 18. Eerie Cloak +1              131072
-- 19. Tidal Talisman              262144
-- 20. Otokogusa Yukata            524288
-- 21. Otokoeshi Yukata            1048576
-- 22. Dinner Jacket               2097152
-- 23. Pair of Dinner Boots        4194304
--
-----------------------------------
package.loaded["scripts/zones/Windurst_Waters/TextIDs"] = nil;
-----------------------------------

require("scripts/zones/Windurst_Waters/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
	player:startEvent(0x038e);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
	local eventItemsStored1 = player:getVar("eventItemsStored1");
	local eventItemsStored2 = player:getVar("eventItemsStored2");
	local eventItemsStored3 = player:getVar("eventItemsStored3");
	local eventItemsStored4 = player:getVar("eventItemsStored4");
	local eventItemsStored5 = player:getVar("eventItemsStored5");

	if(csid == 0x038D) then
		player:tradeComplete();
	elseif(csid == 0x038E and option ~= 1073741825) then
		if(player:getFreeSlotsCount(0) >= 1) then
			player:delGil(250);
			if(option == 0) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",0,false);
				player:addItem(86);
				player:messageSpecial(ITEM_OBTAINED, 86);
			elseif(option == 1) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",1,false);
				player:addItem(115);
				player:messageSpecial(ITEM_OBTAINED, 115);
			elseif(option == 2) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",2,false);
				player:addItem(116);
				player:messageSpecial(ITEM_OBTAINED, 116);
			elseif(option == 3) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",3,false);
				player:addItem(87);
				player:messageSpecial(ITEM_OBTAINED, 87);
			elseif(option == 4) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",4,false);
				player:addItem(117);
				player:messageSpecial(ITEM_OBTAINED, 117);
			elseif(option == 5) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",5,false);
				player:addItem(118);
				player:messageSpecial(ITEM_OBTAINED, 118);
			elseif(option == 6) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",6,false);
				player:addItem(119);
				player:messageSpecial(ITEM_OBTAINED, 119);
			elseif(option == 7) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",7,false);
				player:addItem(193);
				player:messageSpecial(ITEM_OBTAINED, 193);
			elseif(option == 8) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",8,false);
				player:addItem(88);
				player:messageSpecial(ITEM_OBTAINED, 88);
			elseif(option == 9) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",9,false);
				player:addItem(154);
				player:messageSpecial(ITEM_OBTAINED, 154);
			elseif(option == 10) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",10,false);
				player:addItem(204);
				player:messageSpecial(ITEM_OBTAINED, 204);
			elseif(option == 11) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",11,false);
				player:addItem(203);
				player:messageSpecial(ITEM_OBTAINED, 203);
			elseif(option == 12) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",12,false);
				player:addItem(205);
				player:messageSpecial(ITEM_OBTAINED, 205);
			elseif(option == 13) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",13,false);
				player:addItem(140);
				player:messageSpecial(ITEM_OBTAINED, 140);
			elseif(option == 14) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",14,false);
				player:addItem(141);
				player:messageSpecial(ITEM_OBTAINED, 141);
			elseif(option == 15) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",15,false);
				player:addItem(155);
				player:messageSpecial(ITEM_OBTAINED, 155);
			elseif(option == 64) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",16,false);
				player:addItem(192);
				player:messageSpecial(ITEM_OBTAINED, 192);
			elseif(option == 65) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",17,false);
				player:addItem(179);
				player:messageSpecial(ITEM_OBTAINED, 179);
			elseif(option == 66) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",18,false);
				player:addItem(323);
				player:messageSpecial(ITEM_OBTAINED, 323);
			elseif(option == 67) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",19,false);
				player:addItem(324);
				player:messageSpecial(ITEM_OBTAINED, 324);
			elseif(option == 68) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",20,false);
				player:addItem(325);
				player:messageSpecial(ITEM_OBTAINED, 325);
			elseif(option == 69) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",21,false);
				player:addItem(176);
				player:messageSpecial(ITEM_OBTAINED, 176);
			elseif(option == 70) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",22,false);
				player:addItem(177);
				player:messageSpecial(ITEM_OBTAINED, 177);
			elseif(option == 71) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",23,false);
				player:addItem(178);
				player:messageSpecial(ITEM_OBTAINED, 178);
			elseif(option == 72) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",24,false);
				player:addItem(180);
				player:messageSpecial(ITEM_OBTAINED, 180);
			elseif(option == 73) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",25,false);
				player:addItem(215);
				player:messageSpecial(ITEM_OBTAINED, 215);
			elseif(option == 74) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",26,false);
				player:addItem(196);
				player:messageSpecial(ITEM_OBTAINED, 196);
			elseif(option == 75) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",27,false);
				player:addItem(197);
				player:messageSpecial(ITEM_OBTAINED, 197);
			elseif(option == 76) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",28,false);
				player:addItem(199);
				player:messageSpecial(ITEM_OBTAINED, 199);
			elseif(option == 77) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",29,false);
				player:addItem(320);
				player:messageSpecial(ITEM_OBTAINED, 320);
			elseif(option == 78) then
				eventItemsStored1 = player:setMaskBit(eventItemsStored1,"eventItemsStored1",30,false);
				player:addItem(415);
				player:messageSpecial(ITEM_OBTAINED, 415);
			elseif(option == 16) then
				eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",0,false);
				player:addItem(17074);
				player:messageSpecial(ITEM_OBTAINED, 17074);
			elseif(option == 17) then
				eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",1,false);
				player:addItem(17565);
				player:messageSpecial(ITEM_OBTAINED, 17565);
			elseif(option == 18) then
				eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",2,false);
				player:addItem(17566);
				player:messageSpecial(ITEM_OBTAINED, 17566);
			elseif(option == 19) then
				eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",3,false);
				player:addItem(17588);
				player:messageSpecial(ITEM_OBTAINED, 17588);
			elseif(option == 20) then
				eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",4,false);
				player:addItem(17830);
				player:messageSpecial(ITEM_OBTAINED, 17830);
			elseif(option == 21) then
				eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",5,false);
				player:addItem(17831);
				player:messageSpecial(ITEM_OBTAINED, 17831);
			elseif(option == 22) then
				eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",6,false);
				player:addItem(18102);
				player:messageSpecial(ITEM_OBTAINED, 18102);
			elseif(option == 23) then
				eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",7,false);
				player:addItem(18103);
				player:messageSpecial(ITEM_OBTAINED, 18103);
			elseif(option == 24) then
				eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",8,false);
				player:addItem(18400);
				player:messageSpecial(ITEM_OBTAINED, 18400);
			elseif(option == 25) then
				eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",9,false);
				player:addItem(18436);
				player:messageSpecial(ITEM_OBTAINED, 18436);
			elseif(option == 26) then
				eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",10,false);
				player:addItem(18401);
				player:messageSpecial(ITEM_OBTAINED, 18401);
			elseif(option == 27) then
				eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",11,false);
				player:addItem(18846);
				player:messageSpecial(ITEM_OBTAINED, 18846);
			elseif(option == 28) then
				eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",12,false);
				player:addItem(18845);
				player:messageSpecial(ITEM_OBTAINED, 18845);
			elseif(option == 29) then
				eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",13,false);
				player:addItem(18441);
				player:messageSpecial(ITEM_OBTAINED, 18441);
			elseif(option == 30) then
				eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",14,false);
				player:addItem(17748);
				player:messageSpecial(ITEM_OBTAINED, 17748);
			elseif(option == 31) then
				eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",15,false);
				player:addItem(17749);
				player:messageSpecial(ITEM_OBTAINED, 17749);
			elseif(option == 92) then
				eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",16,false);
				player:addItem(16182);
				player:messageSpecial(ITEM_OBTAINED, 16182);
			elseif(option == 93) then
				eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",17,false);
				player:addItem(16183);
				player:messageSpecial(ITEM_OBTAINED, 16183);
			elseif(option == 94) then
				eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",18,false);
				player:addItem(18863);
				player:messageSpecial(ITEM_OBTAINED, 18863);
			elseif(option == 95) then
				eventItemsStored2 = player:setMaskBit(eventItemsStored2,"eventItemsStored2",19,false);
				player:addItem(18864);
				player:messageSpecial(ITEM_OBTAINED, 18864);
			elseif(option == 32) then
				eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",0,false);
				player:addItem(13916);
				player:messageSpecial(ITEM_OBTAINED, 13916);
			elseif(option == 33) then
				eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",1,false);
				player:addItem(13917);
				player:messageSpecial(ITEM_OBTAINED, 13917);
			elseif(option == 34) then
				eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",2,false);
				player:addItem(15176);
				player:messageSpecial(ITEM_OBTAINED, 15176);
			elseif(option == 35) then
				eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",3,false);
				player:addItem(15177);
				player:messageSpecial(ITEM_OBTAINED, 15177);
			elseif(option == 36) then
				eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",4,false);
				player:addItem(15178);
				player:messageSpecial(ITEM_OBTAINED, 15178);
			elseif(option == 37) then
				eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",5,false);
				player:addItem(15179);
				player:messageSpecial(ITEM_OBTAINED, 15179);
			elseif(option == 38) then
				eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",6,false);
				player:addItem(15198);
				player:messageSpecial(ITEM_OBTAINED, 15198);
			elseif(option == 39) then
				eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",7,false);
				player:addItem(15199);
				player:messageSpecial(ITEM_OBTAINED, 15199);
			elseif(option == 40) then
				eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",8,false);
				player:addItem(15204);
				player:messageSpecial(ITEM_OBTAINED, 15204);
			elseif(option == 41) then
				eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",9,false);
				player:addItem(16075);
				player:messageSpecial(ITEM_OBTAINED, 16075);
			elseif(option == 42) then
				eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",10,false);
				player:addItem(16076);
				player:messageSpecial(ITEM_OBTAINED, 16076);
			elseif(option == 43) then
				eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",11,false);
				player:addItem(16109);
				player:messageSpecial(ITEM_OBTAINED, 16109);
			elseif(option == 44) then
				eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",12,false);
				player:addItem(16118);
				player:messageSpecial(ITEM_OBTAINED, 16118);
			elseif(option == 45) then
				eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",13,false);
				player:addItem(16119);
				player:messageSpecial(ITEM_OBTAINED, 16119);
			elseif(option == 46) then
				eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",14,false);
				player:addItem(16120);
				player:messageSpecial(ITEM_OBTAINED, 16120);
			elseif(option == 47) then
				eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",15,false);
				player:addItem(16144);
				player:messageSpecial(ITEM_OBTAINED, 16144);
			elseif(option == 80) then
				eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",16,false);
				player:addItem(16145);
				player:messageSpecial(ITEM_OBTAINED, 16145);
			elseif(option == 81) then
				eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",17,false);
				player:addItem(11491);
				player:messageSpecial(ITEM_OBTAINED, 11419);
			elseif(option == 82) then
				eventItemsStored3 = player:setMaskBit(eventItemsStored3,"eventItemsStored3",18,false);
				player:addItem(11500);
				player:messageSpecial(ITEM_OBTAINED, 11500);
			elseif(option == 48) then
				eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",0,false);
				player:addItem(13819);
				player:messageSpecial(ITEM_OBTAINED, 13819);
			elseif(option == 49) then
				eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",1,false);
				player:addItem(13821);
				player:messageSpecial(ITEM_OBTAINED, 13821);
			elseif(option == 50) then
				eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",2,false);
				player:addItem(14450);
				player:messageSpecial(ITEM_OBTAINED, 14450);
			elseif(option == 51) then
				eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",3,false);
				player:addItem(14457);
				player:messageSpecial(ITEM_OBTAINED, 14457);
			elseif(option == 52) then
				eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",4,false);
				player:addItem(15408);
				player:messageSpecial(ITEM_OBTAINED, 15408);
			elseif(option == 53) then
				eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",5,false);
				player:addItem(15415);
				player:messageSpecial(ITEM_OBTAINED, 15415);
			elseif(option == 54) then
				eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",6,false);
				player:addItem(14519);
				player:messageSpecial(ITEM_OBTAINED, 14519);
			elseif(option == 55) then
				eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",7,false);
				player:addItem(14520);
				player:messageSpecial(ITEM_OBTAINED, 14520);
			elseif(option == 56) then
				eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",8,false);
				player:addItem(14532);
				player:messageSpecial(ITEM_OBTAINED, 14532);
			elseif(option == 57) then
				eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",9,false);
				player:addItem(14534);
				player:messageSpecial(ITEM_OBTAINED, 14534);
			elseif(option == 58) then
				eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",10,false);
				player:addItem(15752);
				player:messageSpecial(ITEM_OBTAINED, 15752);
			elseif(option == 59) then
				eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",11,false);
				player:addItem(15753);
				player:messageSpecial(ITEM_OBTAINED, 15753);
			elseif(option == 60) then
				eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",12,false);
				player:addItem(11265);
				player:messageSpecial(ITEM_OBTAINED, 11265);
			elseif(option == 61) then
				eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",13,false);
				player:addItem(11273);
				player:messageSpecial(ITEM_OBTAINED, 11273);
			elseif(option == 62) then
				eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",14,false);
				player:addItem(16321);
				player:messageSpecial(ITEM_OBTAINED, 16321);
			elseif(option == 63) then
				eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",15,false);
				player:addItem(16329);
				player:messageSpecial(ITEM_OBTAINED, 16329);
			elseif(option == 84) then
				eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",16,false);
				player:addItem(11300);
				player:messageSpecial(ITEM_OBTAINED, 11300);
			elseif(option == 85) then
				eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",17,false);
				player:addItem(11301);
				player:messageSpecial(ITEM_OBTAINED, 11301);
			elseif(option == 86) then
				eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",18,false);
				player:addItem(11290);
				player:messageSpecial(ITEM_OBTAINED, 11290);
			elseif(option == 87) then
				eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",19,false);
				player:addItem(11316);
				player:messageSpecial(ITEM_OBTAINED, 11316);
			elseif(option == 88) then
				eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",20,false);
				player:addItem(11318);
				player:messageSpecial(ITEM_OBTAINED, 11318);
			elseif(option == 126) then
				eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",21,false);
				player:addItem(11355);
				player:messageSpecial(ITEM_OBTAINED, 11355);
			elseif(option == 127) then
				eventItemsStored4 = player:setMaskBit(eventItemsStored4,"eventItemsStored4",22,false);
				player:addItem(16378);
				player:messageSpecial(ITEM_OBTAINED, 16378);
			elseif(option == 96) then
				eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",0,false);
				player:addItem(264);
				player:messageSpecial(ITEM_OBTAINED, 264);
			elseif(option == 97) then
				eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",1,false);
				player:addItem(455);
				player:messageSpecial(ITEM_OBTAINED, 455);
			elseif(option == 98) then
				eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",2,false);
				player:addItem(265);
				player:messageSpecial(ITEM_OBTAINED, 265);
			elseif(option == 99) then
				eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",3,false);
				player:addItem(266);
				player:messageSpecial(ITEM_OBTAINED, 266);
			elseif(option == 100) then
				eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",4,false);
				player:addItem(267);
				player:messageSpecial(ITEM_OBTAINED, 267);
			elseif(option == 101) then
				eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",5,false);
				player:addItem(456);
				player:messageSpecial(ITEM_OBTAINED, 456);
			elseif(option == 102) then
				eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",6,false);
				player:addItem(457);
				player:messageSpecial(ITEM_OBTAINED, 457);
			elseif(option == 103) then
				eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",7,false);
				player:addItem(458);
				player:messageSpecial(ITEM_OBTAINED, 458);
			elseif(option == 104) then
				eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",8,false);
				player:addItem(138);
				player:messageSpecial(ITEM_OBTAINED, 138);
			elseif(option == 105) then
				eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",9,false);
				player:addItem(269);
				player:messageSpecial(ITEM_OBTAINED, 269);
			elseif(option == 106) then
				eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",10,false);
				player:addItem(3641);
				player:messageSpecial(ITEM_OBTAINED, 3641);
			elseif(option == 107) then
				eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",11,false);
				player:addItem(3642);
				player:messageSpecial(ITEM_OBTAINED, 3642);
			elseif(option == 108) then
				eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",12,false);
				player:addItem(207);
				player:messageSpecial(ITEM_OBTAINED, 207);
			elseif(option == 109) then
				eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",13,false);
				player:addItem(271);
				player:messageSpecial(ITEM_OBTAINED, 271);
			elseif(option == 110) then
				eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",14,false);
				player:addItem(3643);
				player:messageSpecial(ITEM_OBTAINED, 3643);
			elseif(option == 111) then
				eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",15,false);
				player:addItem(3644);
				player:messageSpecial(ITEM_OBTAINED, 3644);
			elseif(option == 112) then
				eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",16,false);
				player:addItem(3645);
				player:messageSpecial(ITEM_OBTAINED, 3645);
			elseif(option == 113) then
				eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",17,false);
				player:addItem(181);
				player:messageSpecial(ITEM_OBTAINED, 181);
			elseif(option == 114) then
				eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",18,false);
				player:addItem(182);
				player:messageSpecial(ITEM_OBTAINED, 182);
			elseif(option == 115) then
				eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",19,false);
				player:addItem(183);
				player:messageSpecial(ITEM_OBTAINED, 183);
			elseif(option == 116) then
				eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",20,false);
				player:addItem(3622);
				player:messageSpecial(ITEM_OBTAINED, 3622);
			elseif(option == 117) then
				eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",21,false);
				player:addItem(3623);
				player:messageSpecial(ITEM_OBTAINED, 3623);
			elseif(option == 118) then
				eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",22,false);
				player:addItem(3624);
				player:messageSpecial(ITEM_OBTAINED, 3624);
			elseif(option == 119) then
				eventItemsStored5 = player:setMaskBit(eventItemsStored5,"eventItemsStored5",23,false);
				player:addItem(3646);
				player:messageSpecial(ITEM_OBTAINED, 3646);
			end
		end
	end
end;