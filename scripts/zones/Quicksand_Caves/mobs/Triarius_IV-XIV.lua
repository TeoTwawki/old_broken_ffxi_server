-----------------------------------	
-- Area: Quicksand Caves
-- NM: Tribunus_IV-XIV
-- Missions: Bastok 8-1 The Chains
-- that Bind Us
-- @zone 208
-- @pos -468.000 0.001 617.000
-----------------------------------	

require("scripts/globals/settings");
require("scripts/zones/Quicksand_Caves/TextIDs");

-----------------------------------
-- onMobSpawn Action
-----------------------------------
function onMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath Action
-----------------------------------
function onMobDeath(mob,killer)
	if(killer:getCurrentMission(BASTOK) == THE_CHAINS_THAT_BIND_US) and (killer:getVar("MissionStatus") == 1) then
		SetServerVariable("Bastok8-1LastClear", os.time());
	end
end;

-----------------------------------
-- onMobEngaged Action
-----------------------------------
function onMobEngaged(mob, target)
end;

-----------------------------------
-- onMobDisengage Action
-----------------------------------
function onMobDisengage(mob)
	-- printf("Disengaging Triarius");
	local self = mob:getID();
	DespawnMob(self, 120);
end;


-----------------------------------
-- onMobDespawn Action
-----------------------------------
function onMobDespawn(mob)
	-- printf("Despawning Triarius");
	local mobsup = GetServerVariable("BastokFight8_1");
	SetServerVariable("BastokFight8_1",mobsup - 1);

	if(GetServerVariable("BastokFight8_1") == 0) then
		local npc = GetNPCByID(17629729); -- qm6
		npc:setStatus(0); -- Reappear
	end
end;
