-----------------------------------
--	Area: Windurst Woods
--	NPC:  Tih Pikeh
-- Type: Mission Giver NPC
-- @zone 241
-- @pos 109.464-5.000-21.996
-- Notes: Changes dialogue based
-- on mission status.
-----------------------------------

package.loaded["scripts/globals/settings"] = nil;
require("scripts/globals/settings");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	local nation = player:getNation();

	--Mission Flags--
	local windymission = player:getVar("windurstMissions");
	local windyoneone = player:getVar("theHorutotoRuinsExperiment");
	local windyonetwo = player:getVar("theHeartOfTheMatter");
	local windyonetwoa = player:getVar("theHeartOf-ApururuCS");
	local windyonetwoc = player:getVar("theHeartOf-CardianCS");
	local windyonethree = player:getVar("thePriceOfPeace");
	-----------------------------------
	-- Dialogues,cutscenes,etc. go below.
	-----------------------------------
	--Standard,non-Windurst player dialogue--
	if (nation ~= 2) then
		player:startEvent(0x6c,0,0,nation);
	else --Windurst player dialogue (most cases)
		if (windymission == 12) then
			--Check status of Windurst Mission 1-2--
			if (windyonetwo == 0) then
				player:startEvent(0x83);
			elseif (windyonetwo == 1 or windyonetwo == 2) then
				player:startEvent(0x87);
			else
				player:startEvent(0x6a);
			end
		elseif (windymission == 13) then
			--Check status of Windurst Mission 1-3--
			if (windyonethree == 0) then
				player:startEvent(0x6a);
			elseif (windyonethree == 1 or windyonethree == 2) then
				player:startEvent(0x98);
			elseif (windyonethree == 3) then
				player:startEvent(0x9c);
			end
		else
			--Check status of Windurst mission 1-1--
			if (windyoneone == 0) then
				player:startEvent(0x6a);
			elseif (windyoneone == 1 or windyoneone == 2) then
				player:startEvent(0x7e); 
			elseif (windyoneone == 3) then
				player:startEvent(0x83);
			end
		end
	end
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;
