-----------------------------------
-- Area: Sacrificial Chamber
--  NM:  Cyaneous-toed Yallberry
-- BCNM: Jungle Boogymen
-- @zone 163
-- @pos -284.368, -32.000, 322.647
-----------------------------------

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobEngaged
-----------------------------------

function onMobEngaged(mob,target)
end;

-----------------------------------
-- onMobDeath Action
-----------------------------------

function onMobDeath(mob,killer)
	local kills = killer:getVar("EVERYONES_GRUDGE_KILLS");

	if(kills < 480) then
		killer:setVar("EVERYONES_GRUDGE_KILLS",kills + 1);
	end

end;