-- Area: Hall of Transference
-- Last updated: client ver. 30140910_0

-- Variable TextID   Description text

-- General Texts
ITEM_CANNOT_BE_OBTAINED = 6378; -- You cannot obtain the item <item> come back again after sorting your inventory
          ITEM_OBTAINED = 6381; -- Obtained: <item>
           GIL_OBTAINED = 6382; -- Obtained <number> gil
       KEYITEM_OBTAINED = 6384; -- Obtained key item: <keyitem>

-- Other Texts
NO_RESPONSE_OFFSET = 7219; -- There is no response.
    DOOR_IS_CLOSED = 7220; -- The door is firmly shut.
          REPAIRED = 7224; -- The device has been repaired.
   REGISTERED_DATA = 7225; -- The device appears to be functioning correctly.
            NODATA = 7226; -- You have no registered data.
     REGISTER_DATA = 7227; -- Register your Data in order to use the device.
