-----------------------------------
-- Area: Hall of Transference - Dem
-- Door: Cermet Gate - Dem
-- @zone 14
-- @pos -216.449 -48.635 -280.000
-----------------------------------
package.loaded["scripts/zones/Hall_of_Transference/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/missions");
require("scripts/zones/Hall_of_Transference/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	if(player:getCurrentMission(COP) == BELOW_THE_ARKS and (player:getVar("PH") <= 1 and 
	player:getVar("PD") <= 1 and player:getVar("PM") <= 1) and player:getVar("PromathiaStatus") == 2)then
		player:startEvent(0x00A0);
	elseif(player:getCurrentMission(COP) == BELOW_THE_ARKS and player:getVar("PD") <= 3
	and player:getVar("PH") ~= 2 and player:getVar("PM") ~= 2)then
		player:startEvent(0x0096);
	elseif(player:getCurrentMission(COP) > BELOW_THE_ARKS) then
		player:startEvent(0x0096);
	else
		player:messageSpecial(DOOR_IS_CLOSED);
	end

	return 1;

end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);

	if(csid == 0x00A0) then
		player:setVar("PD",2);
		player:setPos(152,0 ,-70 ,81 ,18); -- Teleport to Promyvion Dem
	end
	if(csid == 0x0096 and option == 1) then
		if(player:getCurrentMission(COP) > BELOW_THE_ARKS) then
			player:setPos(152,0 ,-70 ,81 ,18); -- Teleport to Promyvion Dem
		elseif(player:getVar("PD") ~= 3) then
			player:setVar("PD",2);
			player:setPos(152,0 ,-70 ,81 ,18); -- Teleport to Promyvion Dem
		end
	end
end;