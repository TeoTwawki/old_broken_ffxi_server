-----------------------------------
-- Area: Hall of Transference - Mea
-- Door: Cermet Gate - Mea
-- @zone 14
-- @pos 280.000 -88.659 -16.450
-----------------------------------
package.loaded["scripts/zones/Hall_of_Transference/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/missions");
require("scripts/zones/Hall_of_Transference/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	if(player:getCurrentMission(COP) == BELOW_THE_ARKS and (player:getVar("PH") <= 1 and 
	player:getVar("PD") <= 1 and player:getVar("PM") <= 1) and player:getVar("PromathiaStatus") == 2)then
		player:startEvent(0x00A0);
	elseif(player:getCurrentMission(COP) == BELOW_THE_ARKS and player:getVar("PM") <= 3
	and player:getVar("PD") ~= 2 and player:getVar("PH") ~= 2)then
		player:startEvent(0x0096);
	elseif(player:getCurrentMission(COP) > BELOW_THE_ARKS) then
		player:startEvent(0x0096);
	else
		player:messageSpecial(DOOR_IS_CLOSED);
	end

	return 1;

end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);

	if(csid == 0x00A0) then
		player:setVar("PM",2);
		player:setPos(-107 ,0 ,223 ,164 ,20); -- Teleport to Promyvion Mea
	end
	if(csid == 0x0096 and option == 1) then
		if(player:getCurrentMission(COP) > BELOW_THE_ARKS) then
			player:setPos(-107 ,0 ,223 ,164 ,20); -- Teleport to Promyvion Mea
		elseif(player:getVar("PM") ~= 3) then
			player:setVar("PM",2);
			player:setPos(-107 ,0 ,223 ,164 ,20); -- Teleport to Promyvion Mea
		end
	end

end;