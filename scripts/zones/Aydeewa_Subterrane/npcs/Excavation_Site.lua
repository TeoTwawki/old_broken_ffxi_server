-----------------------------------
-- Area: Aydeewa Subterrane
-- NPC: Excavation Site
-- Type: Mining Point
-- Quest: Olduum
-- @zone 68
-- @pos 361.691, 1.595, 362.892
-- @pos 374.905, 0.131, 333.510
-----------------------------------
package.loaded["scripts/zones/Aydeewa_Subterrane/TextIDs"] = nil;
-------------------------------------

require("scripts/globals/mining");
require("scripts/zones/Aydeewa_Subterrane/TextIDs");

-----------------------------------
-- onTrade
-----------------------------------

function onTrade(player,npc,trade)
	startMining(player,player:getZone(),npc,trade,0x00A5);
end;

-----------------------------------
-- onTrigger
-----------------------------------

function onTrigger(player,npc)
	player:messageSpecial(MINING_IS_POSSIBLE_HERE,605);
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
end;