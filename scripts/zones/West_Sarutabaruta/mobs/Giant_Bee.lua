-----------------------------------
-- Area: West Sarutabaruta
-- MOB:  Giant Bee
-- @zone 115
-- @pos many
-----------------------------------

require("/scripts/globals/fieldsofvalor");

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)
	checkRegime(killer,mob,61,2);
end;
