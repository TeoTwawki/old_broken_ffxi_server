----------------------------------
-- Area: Kuftal Tunnel
-- NM: Yowie
-- @zone 174
-- @pos 25.841 20.252 71.875
-----------------------------------

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob,killer)

	-- Set Yowie's Window Open Time
	wait = math.random((7200),(28800)); -- 2-8 hours
	SetServerVariable("[POP]Yowie", os.time(t) + (wait /NM_TIMER_MOD));
	DeterMob(mob:getID(), true);

	-- Set PH back to normal, then set to respawn spawn
    local PH = GetServerVariable("[PH]Yowie");
	SetServerVariable("[PH]Yowie", 0);
	DeterMob(PH, false);
	GetMobByID(PH):setRespawnTime(GetMobRespawnTime(PH));

end;