-----------------------------------	
-- Area: Kuftal Tunnel	
-- NM: Dervo's Ghost
-- Missions: Bastok 8-2
-- @zone 174
-- @pos -22.000 -11.000 -147.000
-----------------------------------	

require("scripts/globals/missions");

-----------------------------------	
-- onMobDeath	
-----------------------------------	
	
function onMobDeath(mob,killer)	

	if(killer:getCurrentMission(BASTOK) == ENTER_THE_TALEKEEPER and killer:getVar("MissionStatus") == 2) then
		killer:setVar("MissionStatus",3);
	end

end;	
