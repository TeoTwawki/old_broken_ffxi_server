-----------------------------------
-- Area: Upper Jeuno
-- NPC: Afdeen
-- Type: Abdhaljs Isle-Purgonorgo
-- Officer
-- @zone 244
-- @pos 1.462 0.000 21.627
-----------------------------------
package.loaded["scripts/zones/Upper_Jeuno/TextIDs"] = nil;
-----------------------------------

require("scripts/zones/Upper_Jeuno/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
	local count = trade:getItemCount();
	local Blackbook = trade:hasItemQty(5325,1);
	local Bluebook = trade:hasItemQty(5323,1);
	local BlackPage = trade:hasItemQty(5326,1);
	local BluePage = trade:hasItemQty(5324,1);

	if((Blackbook == true or Bluebook == true or BlackPage == true or BluePage == true) and count == 1) then
		player:startEvent(0x00B3);
	else
		player:startEvent(0x0053);
	end

end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	if(player:getVar("BrennerExplanation") == 0) then -- Initial CS tracking
		player:startEvent(0x0051); -- first diagloge explanation
	elseif(player:hasItem(5325)) then -- check for Blackbook
		player:startEvent(0x00CB, 5325);
	elseif(player:hasItem(5323)) then -- check for Bluebook
		player:startEvent(0x00CB, 5323);
	elseif(player:hasItem(5326)) then -- check for Blackbook page
		player:startEvent(0x00CB, 5326);
	elseif(player:hasItem(5324)) then -- check for Bluebook page
		player:startEvent(0x00CB, 5324);
	else
		player:startEvent(0x00B2);
	end

end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	printf("CSID: %u",csid);
	printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	printf("CSID: %u",csid);
	printf("RESULT: %u",option);
	local cost = 0;
	if (csid == 0x0051) then
		player:setVar("BrennerExplanation",1); -- set initial CS viewed	
	elseif (csid == 0x00B2) then
		if (option == 196608) then
			cost = 1250;
		elseif (option == 196609) then
			cost = 1100;
		elseif (option == 196610) then
			cost = 1000;
		elseif (option == 196611) then
			cost = 900;
		elseif (option == 196612) then
			cost = 800;
		elseif (option == 196613) then
			cost = 700;
		elseif (option == 196614) then
			cost = 600;
		end
		if(option == 196608 or option == 196609 or option == 196610 or option == 196611 or option == 196612 or option == 196613 or option == 196614) then
			if(player:getFreeSlotsCount() < 1) then
				player:messageSpecial(ITEM_CANNOT_BE_OBTAINED,5325);
			else
				player:addItem(5325);
				player:messageSpecial(ITEM_OBTAINED,5325);
				player:delGil(cost);
			end
		end
	elseif (csid == 0x00B3 and option == 1) then
		player:tradeComplete();
		player:setPos(0,0,0,0,44);
	end

end;
