-----------------------------------
-- Area: Knotted root
-- NPC: Moh gates
-- @zone 269
-- @pos many
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/missions");
require("scripts/globals/titles");
require("scripts/globals/status");
require("scripts/globals/magic");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function onMobSpawn(mob)
end;


-----------------------------------
-- onMobEngaged
-----------------------------------
function onMobEngaged(mob,target)
end;

-----------------------------------
-- onMobDeath Action
-----------------------------------

function onMobDeath(mob,killer)

	local A = GetMobAction(17879208);
	local B = GetMobAction(17879209);
	local C = GetMobAction(17879210);
	local E = GetMobAction(17879217);
	local F = GetMobAction(17879218);
	local G = GetMobAction(17879219);

	local D = A+B+C;
	local H = E+F+G;

	if(D >= 63 and D <= 75)then
		-- GetNPCByID(17879296):openDoor(3600);
		-- GetNPCByID(17879297):openDoor(3600);
		DespawnMob(17879211);
		DespawnMob(17879212);
		DespawnMob(17879213);
		DespawnMob(17879214);
		DespawnMob(17879215);
		DespawnMob(17879216);
	elseif(H >= 63 and H <= 75)then
		GetNPCByID(17879294):openDoor(3600);
		GetNPCByID(17879295):openDoor(3600);
		DespawnMob(17879220);
		DespawnMob(17879221);
		DespawnMob(17879222);
		DespawnMob(17879223);
		DespawnMob(17879224);
		DespawnMob(17879225);
	end
end;
