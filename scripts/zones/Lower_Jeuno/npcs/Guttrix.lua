-----------------------------------
-- Area: Lower Jeuno
--  NPC: Guttrix
-- Type: Standard NPC
-- Quests: The Goblin Tailor
--  @pos -36.010 4.499 -139.714 245
--
-- ZONE data:             pRACE data:
-- 0  = Ordelle's Caves    0  = Nobody
-- 1  = Gugsen Mines       1  = Hume guys
-- 2  = Maze of Shakrhami  2  = Hume gals
--                         3  = Elvaan guys
-- RSE_STATUS data:        4  = Elvaan gals
-- 0  = No RSE acquired    5  = Taru guys
-- 1  = Body               6  = Taru gals
-- 2  = Hands              7  = Mithra
-- 4  = Pants              8  = Galka
-- 8  = Feet               9+ = Nothing, invalid
-- 15 = All RSE acquired
--
-- Todo:
--  Store RSE completion in database somewhere other
--  than a player variable? The char_vars SQL table
--  was not designed for permanent storage.
--
--  Currently if player vars table gets reset,
--  then this could be repeated for another full set.
--
--  In retail you can only get each item ONCE EVER and
--  we can't use quest completed status to check that.
--
-----------------------------------
package.loaded["scripts/zones/Lower_Jeuno/TextIDs"] = nil;
package.loaded["scripts/globals/settings"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/titles");
require("scripts/globals/keyitems");
require("scripts/globals/quests");
require("scripts/zones/Lower_Jeuno/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------
function onTrigger(player,npc)
    local GobTailor = player:getQuestStatus(JEUNO,THE_GOBLIN_TAILOR);
    local ZONE = VanadielRSELocation();
    local RSE_RACE = VanadielRSERace();
    local pRACE = player:getRace();
    local RSE_STATUS = player:getVar("RSE_STATUS");

    if (player:getMainLvl() >= 10 and player:getFame(JEUNO) >= 3 and GobTailor == QUEST_AVAILABLE) then
        player:startEvent(0x2720, ZONE, RSE_RACE);
    elseif (RSE_RACE == pRACE and RSE_STATUS >= 1 and RSE_STATUS <= 14 and player:hasKeyItem(MAGICAL_PATTERN) == false) then
        player:startEvent(0x2721, ZONE, RSE_RACE);
    elseif (player:hasKeyItem(MAGICAL_PATTERN) == true) then
        player:startEvent(0x2722, RSE_STATUS);
    elseif (player:getMainLvl() <= 9 or player:getFame(JEUNO) <= 2) then
        player:startEvent(0x2724);
    else
        player:startEvent(0x2723);
    end

end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
    -- printf("CSID: %u",csid);
    -- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------
function onEventFinish(player,csid,option)
    -- printf("CSID: %u",csid);
    -- printf("RESULT: %u",option);
    local RACE = player:getRace();
    local RSE_STATUS = player:getVar("RSE_STATUS");
    local REWARD = 0;
    local SELECTION = 0;
    if (csid == 0x2720 or csid == 0x2721) then
        if (player:getQuestStatus(JEUNO,THE_GOBLIN_TAILOR) ~= QUEST_ACCEPTED) then
            player:delQuest(JEUNO,THE_GOBLIN_TAILOR);
            player:addQuest(JEUNO,THE_GOBLIN_TAILOR);
        end
    elseif (csid == 0x2722) then
        if (option == 1) then
            if (RACE == 1) then
                REWARD = 12654; -- Custom Tunic
                SELECTION = 1;
            elseif (RACE == 2) then
                REWARD = 12655; -- Custom Vest
                SELECTION = 1;
            elseif (RACE == 3) then
                REWARD = 12656; -- Magna Jerkin
                SELECTION = 1;
            elseif (RACE == 4) then
                REWARD = 12657; -- Magna Bodice
                SELECTION = 1;
            elseif (RACE == 5 or RACE == 6) then
                REWARD = 12658; -- Wonder Kaftan
                SELECTION = 1;
            elseif (RACE == 7) then
                REWARD = 12659; -- Savage Separates
                SELECTION = 1;
            elseif (RACE == 8) then
                REWARD = 12660; -- Elder's Surcoat
                SELECTION = 1;
            end
        elseif (option == 2) then
            if (RACE == 1) then
                REWARD = 12761; -- Custom M Gloves
                SELECTION = 2;
            elseif (RACE == 2) then
                REWARD = 12762; -- Custom F Gloves
                SELECTION = 2;
            elseif (RACE == 3) then
                REWARD = 12763; -- Magna Gauntlets
                SELECTION = 2;
            elseif (RACE == 4) then
                REWARD = 12764; -- Magna Gloves
                SELECTION = 2;
            elseif (RACE == 5 or RACE == 6) then
                REWARD = 12765; -- Wonder Mitts
                SELECTION = 2;
            elseif (RACE == 7) then
                REWARD = 12766; -- Savage Gauntlets
                SELECTION = 2;
            elseif (RACE == 8) then
                REWARD = 12767; -- Elder's Bracers
                SELECTION = 2;
            end
        elseif (option == 3) then
            if (RACE == 1) then
                REWARD = 12871; -- Custom Slacks
                SELECTION = 4;
            elseif (RACE == 4) then
                REWARD = 12872; -- Custom Pants
                SELECTION = 4;
            elseif (RACE == 3) then
                REWARD = 12873; -- 	Magna M Chausses
                SELECTION = 4;
            elseif (RACE == 4) then
                REWARD = 12874; -- 	Magna F Chausses
                SELECTION = 4;
            elseif (RACE == 5 or RACE == 6) then
                REWARD = 12875; -- Wonder Braccae
                SELECTION = 4;
            elseif (RACE == 7) then
                REWARD = 12876; -- Savage Loincloth
                SELECTION = 4;
            elseif (RACE == 8) then
                REWARD = 12877; -- Elder's Braguette
                SELECTION = 4;
            end
        elseif (option == 4) then
            if (RACE == 1) then
                REWARD = 13015; -- Custom M Boots
                SELECTION = 8;
            elseif (RACE == 4) then
                REWARD = 13016; -- Custom F Boots
                SELECTION = 8;
            elseif (RACE == 3) then
                REWARD = 13017; -- Magna M Ledelsens
                SELECTION = 8;
            elseif (RACE == 4) then
                REWARD = 13018; -- Magna F Ledelsens
                SELECTION = 8;
            elseif (RACE == 5 or RACE == 6) then
                REWARD = 13019; -- Wonder CLomps
                SELECTION = 8;
            elseif (RACE == 7) then
                REWARD = 13020; -- Savage Gaiters
                SELECTION = 8;
            elseif (RACE == 8) then
                REWARD = 13021; -- Elder's Sandals
                SELECTION = 8;
            end
        end

        if (player:getFreeSlotsCount() >= 1 and option ~= 1073741824) then
            player:addItem(REWARD);
            player:setVar("RSE_STATUS",RSE_STATUS+SELECTION);
            player:delKeyItem(MAGICAL_PATTERN);
            player:completeQuest(JEUNO,THE_GOBLIN_TAILOR);
            player:addFame(JEUNO, JEUNO_FAME*30);
            player:addTitle(GOBLINS_EXCLUSIVE_FASHION_MANNEQUIN);
            player:messageSpecial(ITEM_OBTAINED,REWARD);
        elseif (option ~= 1073741824) then
            player:messageSpecial(ITEM_CANNOT_BE_OBTAINED,REWARD);
        end
    end

end;
