-----------------------------------
-- Area: Lower Jeuno
-- Name: Game Table
-- Type: Gaming NPC
-- @zone 245
-- @pos -55.000, 4.999, -66.800 &
-- -59.000, 4.999, -73.700 &
-- -40.400, -1.000, -42.700
-----------------------------------

-----------------------------------
-- onTrigger Action
-----------------------------------
function onTrigger(player,npc)
	player:startEvent(0x2759);
end; 
 

-----------------------------------
-- onTrade Action
-----------------------------------
function onTrade(player,npc,trade)
end; 
 
  
-----------------------------------
-- onEventFinish Action
-----------------------------------
function onEventFinish(player,csid,option)
--print("CSID:",csid);
--print("RESULT:",option);
end;