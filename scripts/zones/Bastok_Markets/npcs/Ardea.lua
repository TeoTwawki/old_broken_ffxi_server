-----------------------------------
-- Area: Bastok Markets
-- NPC: Ardea
-- Type: Quest NPC
-- Quests: Chasing Quotas, Rock
-- Racketeer
-- @zone 235
-- @pos -197.963 -6.000 -69.026
-----------------------------------
package.loaded["scripts/zones/Bastok_Markets/TextIDs"] = nil;
-----------------------------------

require("scripts/zones/Bastok_Markets/TextIDs");
require("scripts/globals/keyitems");
require("scripts/globals/quests");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end; 

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	local RockRacketeer = player:getQuestStatus(WINDURST,ROCK_RACKETTER);
	local Quotas_Status = player:getVar("ChasingQuotas_Progress");

	-- Rock Racketeer
	if (RockRacketeer == QUEST_ACCEPTED and player:hasKeyItem(SHARP_GRAY_STONE)) then
		player:startEvent(0x0105);
	elseif(Quotas_Status == 3) then
		player:startEvent(264); -- Someone was just asking about that earring.
	elseif(Quotas_Status == 4) then
		player:startEvent(265); -- They'll be happy if you return it.
	-- Standard dialog
	else
		player:startEvent(0x104);
	end
end; 

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
--printf("CSID: %u",csid);
--printf("RESULT: %u",option);
	if(csid == 0x0108) then
		player:setVar("chasingQuotas_Event",6);

	-- Rock Racketeer
	elseif (csid == 0x0105 and option ~= 1) then
		player:delKeyItem(SHARP_GRAY_STONE);
		player:addGil(GIL_RATE*10);
		player:setVar("rockracketeer_sold",1);
	elseif (csid == 0x0105 and option ~= 2) then
		player:setVar("rockracketeer_sold",2);
	elseif (csid == 264) then
		player:setVar("ChasingQuotas_Progress",4);
	end

end;

--260 -- 0x0104 -- This is the Goldsmith's Guild.  Do not enter the premises unless you have business with the guild.
--261 -- 0x0105 -- Hmm... This sharp gray stone seems to have some traces of mythril in it.  Not enough to be worth much, unfortunately.  I'm willing to buy it off you, but I can't offer you more than ten gil for it. Y/N
--262 -- 0x0106 -- Are you going back to Windurst? I'm sorry I could not make your trip here worthwhile.  
--263 -- 0x0107 -- Yes? The stone you sold me the other day? You mean the one with the traces of mythril?  I'm sorry but ift has already been used by one of our guild members in his work.
--264 -- 0x0108 -- You want me to take a look at this earring? Hmmm... Why, that is strange.  I had a customer come in the other day and ask me to make an earring just like this one.  She said she had lost one of the pair.  I told her that it would be impossible for me to replace the lost earring, so I am sure she would be quite pleased if you returned it to her.  I think she said her name was Esca.
--265 -- 0x0109 -- Could you please take that earring to Esca? I am sure she would be quite pleased to have it returned.
