-----------------------------------
-- Area: Tahrongi Canyon
--  VNM: Yilbegan
-- @zone 117
-- @pos
-----------------------------------

require("scripts/globals/titles");

-----------------------------------
-- onMobSpawn Action
-----------------------------------

function OnMobSpawn(mob)
end;

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
	killer:addTitle(YILBEGAN_HIDEFLAYER);
	if (killer:getVar("Windy_Tutorial_NPC") == 8) then
		killer:setVar("Windy_Tutorial_NPC",9);
	end
end;