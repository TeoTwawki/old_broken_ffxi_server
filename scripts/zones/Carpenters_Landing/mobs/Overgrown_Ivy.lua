-----------------------------------
-- Area: Carpenters Landing
-- NM: Overgrown Ivy
-- @zone 2
-- @pos -120.000 -6.150 -469.000
-----------------------------------

-----------------------------------
-- onMobDeath
-----------------------------------

function onMobDeath(mob, killer)
	if(killer:getCurrentMission(COP) == THE_ROAD_FORKS and killer:getVar("EMERALD_WATERS_Status") == 4)then 
		killer:setVar("EMERALD_WATERS_Status",5);
	end
end;