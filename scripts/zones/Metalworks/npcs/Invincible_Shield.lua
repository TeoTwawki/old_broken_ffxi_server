-----------------------------------
-- Area: Metalworks
-- NPC: Invincible Shield
-- Type: Ballista NPC
-- Quests: Lure of the Wildcat
-- (Bastok)
-- @zone 237
-- @pos -51.083 -11.000 2.126
-----------------------------------
package.loaded["scripts/zones/Metalworks/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/quests");
require("scripts/zones/Metalworks/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)

	local WildcatBastok = player:getVar("WildcatBastok");

	if (player:getQuestStatus(BASTOK,LURE_OF_THE_WILDCAT_BASTOK) == QUEST_ACCEPTED and player:getMaskBit(WildcatBastok,6) == false) then
		player:startEvent(0x03a4);
	else
		player:startEvent(0x032a);
	end
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
	-- printf("CSID: %u",csid);
	-- printf("RESULT: %u",option);

	if (csid == 0x03a4) then
		player:setMaskBit(player:getVar("WildcatBastok"),"WildcatBastok",6,true);
	end

end;
