---------------------------------------------------------------------------------------------------
-- func: @setlanflag <flag value> <target>
-- auth: Forgottenandlost
-- desc: Toggles LAN flag
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "is"
};

function onTrigger(player, flag, target)
    if (flag == nil) then
        player:PrintToPlayer("@setlanflag <flag value> <target>");
        return
    elseif (flag >= 1) then
        flag = true;
    elseif (flag <= 0) then
        flag = false;
    else
        player:PrintToPlayer("Flag value must be 1 or 0 (on/off)");
        player:PrintToPlayer("@setlanflag <on/off> <target>");
        return
    end

    if (target == nil) then
        player:setLanFlag(flag)
    else
        local targ = GetPlayerByName(target);
        if (targ ~= nil) then
            targ:setLanFlag(flag)
        else
            player:PrintToPlayer( string.format( "Player named '%s' not found!", target ) );
        end
    end
end;