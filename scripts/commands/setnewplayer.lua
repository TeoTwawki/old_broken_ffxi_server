---------------------------------------------------------------------------------------------------
-- func: @setnewplayer <target>
-- auth: Forgottenandlost
-- desc: Toggles NewPlayer status for testing
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "ss"
};

function onTrigger(player, target)

    if (target == nil) then
		if (player:getNewPlayer() == false) then
			player:setNewPlayer(true);
		else
			player:setNewPlayer(false);
		end
    else
        local targ = GetPlayerByName(target);
        if (targ ~= nil) then
			if (targ:getNewPlayer() == false) then
				targ:setNewPlayer(true);
			else
				targ:setNewPlayer(false);
			end
        else
            player:PrintToPlayer( string.format( "Player named '%s' not found!", target ) );
        end
    end

end;