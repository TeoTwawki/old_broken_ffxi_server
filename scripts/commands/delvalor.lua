---------------------------------------------------------------------------------------------------
-- func: @delvalor <player> <amount>
-- auth: Compmike, modified by Forgottenandlost
-- desc: Deletes the specified amount of Valor Points to the player
---------------------------------------------------------------------------------------------------

cmdprops =
{
	permission = 1,
	parameters = "is"
};

function onTrigger(player,amount,target)

    if (amount == nil) then
        player:PrintToPlayer( "You must enter a valid point amount." );
        player:PrintToPlayer( "@delvalor <amount> <player>" );
        return;
    end

    if (target == nil) then
        player:delValorPoint(amount);
    else
        local targ = GetPlayerByName(target);
        if (targ ~= nil) then
            targ:delValorPoint(amount);
        else
            player:PrintToPlayer( string.format( "Player named '%s' not found!", target ) );
            player:PrintToPlayer( "@delvalor <amount> <player>" );
        end
    end
end;