---------------------------------------------------
-- Mobskill: Horrid Roar
--
-- Removes beneficial effects from a single target
-- and reduces enmity.
-- Type: Enfeebling
-- Utsusemi/Blink absorb: Wipes shadows
-- Range: Melee or radial
-- Notes: The effect varies slightly by which wyrm
-- used this ability:
-- Fafnir/Cynoprosopi/Smok - One buff removed,
-- minimal enmity reset 
---------------------------------------------------

require("/scripts/globals/settings");
require("/scripts/globals/status");
require("/scripts/globals/monstertpmoves");

---------------------------------------------------

function OnMobSkillCheck(target,mob,skill)
    if(target:isBehind(mob, 48) == true) then
        return 1;
    elseif (mob:AnimationSub() ~= 0) then
        return 1;
    end
    return 0;
end;

function OnMobWeaponSkill(target, mob, skill)

    local dispel =  target:dispelStatusEffect(bit.bor(EFFECTFLAG_DISPELABLE, EFFECTFLAG_FOOD));

    if(dispel == EFFECT_NONE) then
        skill:setMsg(MSG_NO_EFFECT); -- No effect
    else
        skill:setMsg(MSG_DISAPPEAR);
    end

    mob:lowerEnmity(target, 20);

    return dispel;
end;