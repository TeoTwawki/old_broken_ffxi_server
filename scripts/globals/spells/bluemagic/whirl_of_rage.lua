-----------------------------------------
-- Bluemagic: Whirl of Rage
-- Delivers an area attack that stuns enemies. 
-- Damage varies with TP.
-- STR+2  DEX+2 
-- Lvl.: 83 MP Cost: 73 Blue Points: 2
-----------------------------------------

require("scripts/globals/magic");
require("scripts/globals/status");
require("scripts/globals/bluemagic");

-----------------------------------------
-- OnMagicCastingCheck
-----------------------------------------

function OnMagicCastingCheck(caster,target,spell)
	return 0;
end;

-----------------------------------------
-- OnSpellCast
-----------------------------------------

function onSpellCast(caster,target,spell)
	local params = {};
	-- This data should match information on http://wiki.ffxiclopedia.org/wiki/Calculating_Blue_Magic_Damage
	params.tpmod = TPMOD_DAMAGE; params.dmgtype = DMGTYPE_SLASH; params.scattr = SC_DETONATION;
	params.numhits = 1;
	params.multiplier = 1.0; params.tp150 = 3.5; params.tp300 = 4.0; params.azuretp = 1.0; params.duppercap = 100; -- D upper >=69
	params.str_wsc = 0.3; params.dex_wsc = 0.0; params.vit_wsc = 0.0; params.agi_wsc = 0.0; params.int_wsc = 0.0; params.mnd_wsc = 0.30; params.chr_wsc = 0.0;
	damage = BluePhysicalSpell(caster, target, spell, params);
	damage = BlueFinalAdjustments(caster, target, spell, damage, params);

	if(target:hasStatusEffect(EFFECT_STUN)) then
		spell:setMsg(75); -- No effect
	else
	-- if(resist <= 0.125) then
			-- spell:setMsg(85);
		-- else
			-- target:addStatusEffect(EFFECT_STUN,0,0,math.random(1,5));
		-- end
	-- end
		target:addStatusEffect(EFFECT_STUN,0,0,math.random(1,5));
	end

	return damage;
end;