-----------------------------------------
-- Bluemagic: Head Butt
-- Damage varies with TP. Additional
-- effect: "Stun."
-- DEX +2
-- Lvl.: 12 MP Cost: 12 Blue Points: 3
-----------------------------------------

require("scripts/globals/magic");
require("scripts/globals/status");
require("scripts/globals/bluemagic");

-----------------------------------------
-- OnSpellCast
-----------------------------------------

function OnMagicCastingCheck(caster,target,spell)
	return 0;
end;

function onSpellCast(caster,target,spell)
	-- local bonus = AffinityBonus(caster, spell:getElement());
	-- local dINT = caster:getStat(MOD_INT) - target:getStat(MOD_INT);
	-- local resist = applyResistance(caster,spell,target,dINT,37,bonus);
	local params = {};
	-- This data should match information on http://wiki.ffxiclopedia.org/wiki/Calculating_Blue_Magic_Damage
	params.tpmod = TPMOD_DAMAGE; params.dmgtype = DMGTYPE_BLUNT; params.scattr = SC_IMPACTION;
	params.numhits = 1;
	params.multiplier = 1.75; params.tp150 = 2.125; params.tp300 = 2.25; params.azuretp = 2.375; params.duppercap = 17;
	params.str_wsc = 0.2; params.dex_wsc = 0.0; params.vit_wsc = 0.0; params.agi_wsc = 0.0; params.int_wsc = 0.2; params.mnd_wsc = 0.0; params.chr_wsc = 0.0;
	damage = BluePhysicalSpell(caster, target, spell, params);
	damage = BlueFinalAdjustments(caster, target, spell, damage, params);

	if(target:hasStatusEffect(EFFECT_STUN)) then
		spell:setMsg(75); -- No effect
	else
	-- if(resist <= 0.125) then
			-- spell:setMsg(85);
		-- else
			-- target:addStatusEffect(EFFECT_STUN,0,0,math.random(1,5));
		-- end
	-- end
		target:addStatusEffect(EFFECT_STUN,0,0,math.random(1,5));
	end

	return damage;
end;