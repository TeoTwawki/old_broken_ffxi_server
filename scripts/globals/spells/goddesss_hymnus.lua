-----------------------------------------
-- Spell: Goddess's Hymnus
-- Grants reraise to party members within
-- area of effect.
-----------------------------------------

require("scripts/globals/status");

-----------------------------------------
-- OnSpellCast
-----------------------------------------

function OnMagicCastingCheck(caster,target,spell)
	return 0;
end;

function onSpellCast(caster,target,spell)

	local sLvl = caster:getSkillLevel(SKILL_SNG); -- Gets skill level of Singing
	local iLvl = caster:getWeaponSkillLevel(SLOT_RANGED);

	local iBoost = (caster:getMod(MOD_HYMNUS) + caster:getMod(MOD_ALL_SONGS)) * 12;

	if (caster:hasStatusEffect(EFFECT_SOUL_VOICE)) then
		iBoost = iBoost * 2;
	elseif (caster:hasStatusEffect(EFFECT_MARCATO)) then
		iBoost = iBoost * 1.5;
	end
	caster:delStatusEffect(EFFECT_MARCATO);

	local duration = 120;
	duration = duration + iBoost + (caster:getMod(MOD_SONG_DURATION)/100) + 1);

	if (caster:hasStatusEffect(EFFECT_TROUBADOUR)) then
		duration = duration * 2;
	end

	if not (target:addBardSong(caster,EFFECT_HYMNUS,power,0,duration,caster:getID(), 0, 1)) then
		spell:setMsg(75);
	end

	return EFFECT_HYMNUS;
end;