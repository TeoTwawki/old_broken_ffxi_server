-----------------------------------------
-- Spell: Alexander
-- Summons Alexander to fight by your side
-- Notes: Astral Flow must be active to
-- summon Alexander.
-- When summoned, Alexander immediately
-- uses Perfect Defense. Alexander
-- lingers shortly after buffing and then
-- vanishes. Astral Flow is canceled once
-- Alexander is dismissed. 
-----------------------------------------

require("scripts/globals/pets");
require("scripts/globals/summon");
require("scripts/globals/bcnm");

-----------------------------------------
-- OnSpellCast
-----------------------------------------

function OnMagicCastingCheck(caster,target,spell)
	if (not caster:canUsePet()) then
		return MSGBASIC_CANT_BE_USED_IN_AREA;
	elseif (not caster:hasStatusEffect(EFFECT_ASTRAL_FLOW)) then
		return 581;
	elseif(caster:getObjType() == TYPE_PC) then
		return avatarMiniFightCheck(caster);
	end
	return 0;
end;

function onSpellCast(caster,target,spell)
	caster:spawnPet(PET_ALEXANDER);

	return 0;
end;