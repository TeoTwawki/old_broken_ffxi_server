-----------------------------------
--  EFFECT_ISSEKIGAN
-- Increases chance of parring and
-- gives an enmity bonus upon
-- successful parry.
-----------------------------------

-----------------------------------
-- onEffectGain Action
-----------------------------------

function onEffectGain(target,effect)
    -- Handled in core
    -- target:addMod(MOD_PARRY_RATE, effect:getPower());
end;

-----------------------------------
-- onEffectTick Action
-----------------------------------

function onEffectTick(target,effect)
end;

-----------------------------------
-- onEffectLose Action
-----------------------------------

function onEffectLose(target,effect)
    -- Handled in core
    -- target:delMod(MOD_PARRY_RATE, effect:getPower());
end;