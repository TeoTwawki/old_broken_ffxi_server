 -----------------------------------
--	EFFECT_HASTE_SAMBA
--  Inflicts the next target you
--  strike with Haste daze,
--  increasing the attack speed of
--  all those engaged in battle
--  with it. 
-----------------------------------

require("scripts/globals/status");

-----------------------------------
-- onEffectGain Action
-----------------------------------

function onEffectGain(target,effect)
end;

-----------------------------------
-- onEffectTick Action
-----------------------------------

function onEffectTick(target,effect)
end;

-----------------------------------
-- onEffectLose Action
-----------------------------------

function onEffectLose(target,effect)
end;