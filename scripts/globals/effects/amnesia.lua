-----------------------------------
--	EFFECT_AMNESIA
-- Harmful Fire based status effect.
-- Victim loses combat memory,
-- preventing use of JAs WSs & Pet
-- Commands
-----------------------------------

-----------------------------------
-- onEffectGain Action
-----------------------------------

function onEffectGain(target,effect)
end;

-----------------------------------
-- onEffectTick Action
-----------------------------------

function onEffectTick(target,effect)
end;

-----------------------------------
-- onEffectLose Action
-----------------------------------

function onEffectLose(target,effect)
end;