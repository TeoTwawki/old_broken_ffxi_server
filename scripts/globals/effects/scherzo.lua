-----------------------------------
-- EFFECT_SCHERZO
-- Mitigates the impact of severely
-- damaging attacks for party
-- members within area of effect. 
-----------------------------------

-----------------------------------
-- onEffectGain Action
-----------------------------------

function onEffectGain(target,effect)
	target:addMod(MOD_SCHERZO_DMG, effect:getPower());
end;

-----------------------------------
-- onEffectTick Action
-----------------------------------

function onEffectTick(target,effect)
end;

-----------------------------------
-- onEffectLose Action
-----------------------------------

function onEffectLose(target,effect)
	target:delMod(MOD_SCHERZO_DMG, effect:getPower());
end;