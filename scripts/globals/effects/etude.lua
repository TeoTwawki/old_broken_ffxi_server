-----------------------------------
-- EFFECT_ETUDE
-- Etude is a type of Song used by
-- a Bard. It has no Area of
-- Effect, and can only be cast on
-- one person at a time. Etudes
-- boost the primary stats of STR,
-- DEX, VIT, AGI, MND, INT, and
-- CHR. 
-----------------------------------

-----------------------------------
-- onEffectGain Action
-----------------------------------

function onEffectGain(target,effect)
	printf("Power: %d \n", effect:getPower());
	if(effect:getSubPower() == 1) then
		target:addMod(MOD_STR, effect:getPower());
	elseif(effect:getSubPower() == 2) then
		target:addMod(MOD_DEX, effect:getPower());
	elseif(effect:getSubPower() == 3) then
		target:addMod(MOD_VIT, effect:getPower());
	elseif(effect:getSubPower() == 4) then
		target:addMod(MOD_AGI, effect:getPower());
	elseif(effect:getSubPower() == 5) then
		target:addMod(MOD_INT, effect:getPower());
	elseif(effect:getSubPower() == 6) then
		target:addMod(MOD_MND, effect:getPower());
	elseif(effect:getSubPower() == 7) then
		target:addMod(MOD_CHR, effect:getPower());
	end
end;

-----------------------------------
-- onEffectTick Action
-----------------------------------

function onEffectTick(target,effect)
	if(effect:getTier() == 2) then
		if(effect:getPower() > 0) then
			effect:setPower(effect:getPower()-1);
			if(effect:getSubPower() == 1) then
				target:delMod(MOD_STR, 1);
			elseif(effect:getSubPower() == 2) then
				target:delMod(MOD_DEX, 1);
			elseif(effect:getSubPower() == 3) then
				target:delMod(MOD_VIT, 1);
			elseif(effect:getSubPower() == 4) then
				target:delMod(MOD_AGI, 1);
			elseif(effect:getSubPower() == 5) then
				target:delMod(MOD_INT, 1);
			elseif(effect:getSubPower() == 6) then
				target:delMod(MOD_MND, 1);
			elseif(effect:getSubPower() == 7) then
				target:delMod(MOD_CHR, 1);
			end
			printf("Power: %d \n", effect:getPower());
		end
	end
end;

-----------------------------------
-- onEffectLose Action
-----------------------------------

function onEffectLose(target,effect)
	if(effect:getPower() > 0) then
		if(effect:getSubPower() == 1) then
			target:delMod(MOD_STR, effect:getPower());
		elseif(effect:getSubPower() == 2) then
			target:delMod(MOD_DEX, effect:getPower());
		elseif(effect:getSubPower() == 3) then
			target:delMod(MOD_VIT, effect:getPower());
		elseif(effect:getSubPower() == 4) then
			target:delMod(MOD_AGI, effect:getPower());
		elseif(effect:getSubPower() == 5) then
			target:delMod(MOD_INT, effect:getPower());
		elseif(effect:getSubPower() == 6) then
			target:delMod(MOD_MND, effect:getPower());
		elseif(effect:getSubPower() == 7) then
			target:delMod(MOD_CHR, effect:getPower());
		end
	end
end;