-----------------------------------
--
-- EFFECT_SANCTION
--
-----------------------------------

-----------------------------------
-- onEffectGain Action
-----------------------------------

function onEffectGain(target,effect)
end;

-----------------------------------
-- onEffectTick Action
-----------------------------------

function onEffectTick(target,effect)
    -- Todo: Only works below a certain %
    if(effect:getPower() == 1) then
        target:addHP(1);
    elseif(effect:getPower() == 2) then
        target:addMP(1);
    end
end;

-----------------------------------
-- onEffectLose Action
-----------------------------------

function onEffectLose(target,effect)
end;