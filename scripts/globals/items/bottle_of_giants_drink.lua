-----------------------------------------
-- ID: 5241
-- Item: Bottle of Giant's Drink
-- Item Effect: +100% HP
-- Durration: 15 mins
-----------------------------------------

require("scripts/globals/status");

-----------------------------------------
-- OnItemCheck
-----------------------------------------

function onItemCheck(target)
	return 0;
end;

-----------------------------------------
-- OnItemUse
-----------------------------------------

function onItemUse(target)
	local duration = 900;
		target:delStatusEffect(EFFECT_MAX_HP_BOOST);
		target:addStatusEffect(EFFECT_MAX_HP_BOOST,100,0,duration);
end;
