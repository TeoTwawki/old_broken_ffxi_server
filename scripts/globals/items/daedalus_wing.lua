-----------------------------------------
-- ID: 4202
-- Item: Daedalus Wing
-- Effect: Increases TP of the user by 100
-----------------------------------------

-----------------------------------------
-- OnItemCheck
-----------------------------------------

function onItemCheck(target)
    return 0;
end;

-----------------------------------------
-- OnItemUse
-----------------------------------------

function onItemUse(target)
    target:addTP(100);
end;