-----------------------------------------
--	ID: 27727
--	Metal Slime Hat
--	DEF:2 Experience point bonus: +1% Dispense: Metal slime candy
-----------------------------------------

-----------------------------------------
-- OnItemCheck
-----------------------------------------

function onItemCheck(target)
local result = 0;
	if (target:getFreeSlotsCount() == 0) then
		result = 308;
	end
return result;
end;

-----------------------------------------
-- OnItemUse
-----------------------------------------

function onItemUse(target)
	target:addItem(6189,1);
end;