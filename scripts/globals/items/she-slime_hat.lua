-----------------------------------------
--	ID: 27726
--	She Slime Hat
--	DEF:1 While asleep: "Regen"+1 Dispense: She-slimeulation candy
-----------------------------------------

-----------------------------------------
-- OnItemCheck
-----------------------------------------

function onItemCheck(target)
local result = 0;
	if (target:getFreeSlotsCount() == 0) then
		result = 308;
	end
return result;
end;

-----------------------------------------
-- OnItemUse
-----------------------------------------

function onItemUse(target)
	target:addItem(6188,1);
end;