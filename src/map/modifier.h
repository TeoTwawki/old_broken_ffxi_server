﻿/*
===========================================================================

  Copyright (c) 2010-2014 Darkstar Dev Teams

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/

  This file is part of DarkStar-server source code.

===========================================================================
*/

#ifndef _CMODIFIER_H
#define _CMODIFIER_H

#include "../common/cbasetypes.h"

/************************************************************************
*																		*
*	Modifier Enumerators												*
*																		*
************************************************************************/

enum MODIFIER
{

/************************************************************************
*																		*
*	Basic Stats															*
*																		*
************************************************************************/

	MOD_NONE				= 0,

	MOD_DEF					= 1,		// Target's Defense
	MOD_HP					= 2,		// MP +/-
	MOD_HPP					= 3,		// HP Percentage
	MOD_CONVMPTOHP			= 4,		// MP -> HP (Cassie Earring)
	MOD_MP					= 5,		// MP +/-
	MOD_MPP					= 6,		// MP Percentage
	MOD_CONVHPTOMP			= 7,		// HP -> MP

	MOD_STR					= 8,		// Strength
	MOD_DEX					= 9,		// Dexterity
	MOD_VIT					= 10,		// Vitality
	MOD_AGI					= 11,		// Agility
	MOD_INT					= 12,		// Intelligence
	MOD_MND					= 13,		// Mind
	MOD_CHR					= 14,		// Charisma

/************************************************************************
*																		*
*	Elemental Defenses													*
*																		*
************************************************************************/

	MOD_FIREDEF				= 15,		// Fire Defense
	MOD_ICEDEF				= 16,		// Ice Defense
	MOD_WINDDEF				= 17,		// Wind Defense
	MOD_EARTHDEF			= 18,		// Earth Defense
	MOD_THUNDERDEF			= 19,		// Thunder Defense
	MOD_WATERDEF			= 20,		// Water Defense
	MOD_LIGHTDEF			= 21,		// Light Defense
	MOD_DARKDEF				= 22,		// Dark Defense

/************************************************************************
*																		*
*	Attack, Accuracy, Enmity, Magic Dmg									*
*																		*
************************************************************************/

	MOD_ATT					= 23,		// Attack
	MOD_RATT				= 24,		// Ranged Attack

	MOD_ACC					= 25,		// Accuracy
	MOD_RACC				= 26,		// Ranged Accuracy

	MOD_ENMITY				= 27,		// Enmity
    MOD_ENMITY_LOSS_REDUCTION = 648,    // Reduces Enmity lost when taking damage
	MOD_TRANQUIL_HEART		= 563,		// Reduces enmity bases on healing skill (only checks if > 0)

	MOD_MATT				= 28,		// Magic Attack
	MOD_MDEF				= 29,		// Magic Defense
	MOD_MACC				= 30,		// Magic Accuracy
	MOD_MEVA				= 31,		// Magic Evasion
	MOD_MAG_DMG_STAT		= 579,		// Magic Damage Stat from equip
	MOD_MAG_BURST_BONUS		= 414,		// Magic Burst Dmg % Bonus
	MOD_ELEM_MAG_DMG		= 418,		// Elemental Magic Dmg % Bonus
	MOD_ENFB_MAG_DMG		= 419,		// Enfeebling Magic Dmg % Bonus
	MOD_NIN_MAG_DMG			= 420,		// Ninjutsu Magic Dmg % Bonus

/************************************************************************
*																		*
*	Magical Attack and Elemental Accuracy								*
*																		*
************************************************************************/

	MOD_FIREATT				= 32,		// Fire Damage
	MOD_ICEATT				= 33,		// Ice Damage
	MOD_WINDATT				= 34,		// Wind Damage
	MOD_EARTHATT			= 35,		// Earth Damage
	MOD_THUNDERATT			= 36,		// Thunder Damage
	MOD_WATERATT			= 37,		// Water Damage
	MOD_LIGHTATT			= 38,		// Light Damage
	MOD_DARKATT				= 39,		// Dark Damage
	MOD_FIREACC				= 40,		// Fire Accuracy
	MOD_ICEACC				= 41,		// Ice Accuracy
	MOD_WINDACC				= 42,		// Wind Accuracy
	MOD_EARTHACC			= 43,		// Earth Accuracy
	MOD_THUNDERACC			= 44,		// Thunder Accuracy
	MOD_WATERACC			= 45,		// Water Accuracy
	MOD_LIGHTACC			= 46,		// Light Accuracy
	MOD_DARKACC				= 47,		// Dark Accuracy

/************************************************************************
*																		*
*	Weaponskill Modifiers												*
*																		*
************************************************************************/

	MOD_WSACC				= 48,		// Weaponskill Accuracy
	MOD_WS_DMG				= 457,		// Weaponskill Damage %
	MOD_ADDS_WEAPONSKILL	 = 355,		// Weaponskill ID to add
	MOD_ADDS_WEAPONSKILL_DYN = 356,		// Weaponskill ID to add while in Dynamis

/************************************************************************
*																		*
*	Resistance to damage type											*
* Value is stored as a percentage of damage reduction (to within 1000)	*
* Example: 1000 = 100%, 875 = 87.5%										*
*																		*
************************************************************************/

	MOD_SLASHRES			= 49,	// Slash Resistance
	MOD_PIERCERES			= 50,	// Piercing Resistance
	MOD_IMPACTRES			= 51,	// Impact Resistance
	MOD_HTHRES				= 52,	// Hand-To-Hand Resistance

/************************************************************************
*																		*
*	Damage Reduction to Elements										*
* Value is stored as a percentage of damage reduction (to within 1000)	*
* Example: 1000 = 100%, 875 = 87.5%										*
*																		*
************************************************************************/

	MOD_FIRERES				= 54,		// % Fire Resistance
	MOD_ICERES				= 55,		// % Ice Resistance
	MOD_WINDRES				= 56,		// % Wind Resistance
	MOD_EARTHRES			= 57,		// % Earth Resistance
	MOD_THUNDERRES			= 58,		// % Thunder Resistance
	MOD_WATERRES			= 59,		// % Water Resistance
	MOD_LIGHTRES			= 60,		// % Light Resistance
	MOD_DARKRES				= 61,		// % Dark Resistance

/************************************************************************
*																		*
*	Battle Skills		% Mods, Evasion, Healing Rate etc..				*
*																		*
************************************************************************/

	MOD_ATTP				= 62,		// % Attack
	MOD_DEFP				= 63,		// % Defense
	MOD_ACCP				= 64,		// % Accuracy
	MOD_EVAP				= 65,		// % Evasion
	MOD_RATTP				= 66,		// % Ranged Attack
	MOD_RACCP				= 67,		// % Ranged Attack Accuracy
	MOD_PARRYP				= 562,		// % Parry

	MOD_EVA					= 68,		// Evasion
	MOD_RDEF				= 69,		// Ranged Defense
	MOD_REVA				= 70,		// Ranged Evasion
	MOD_MPHEAL				= 71,		// MP Recovered while healing
	MOD_HPHEAL				= 72,		// HP Recovered while healing
	MOD_STORETP				= 73,		// Increases the rate at which TP is gained

/************************************************************************
*																		*
*	Working Skills		(weapon combat skills)							*
*																		*
************************************************************************/

	MOD_HTH					= 80,		// Hand To Hand
	MOD_DAGGER				= 81,		// Dagger
	MOD_SWORD				= 82,		// Sword
	MOD_GSWORD				= 83,		// Great Sword
	MOD_AXE					= 84,		// Axe
	MOD_GAXE				= 85,		// Great Axe
	MOD_SCYTHE				= 86,		// Scythe
	MOD_POLEARM				= 87,		// Polearm
	MOD_KATANA				= 88,		// Katana
	MOD_GKATANA				= 89,		// Great Katana
	MOD_CLUB				= 90,		// Club
	MOD_STAFF				= 91,		// Staff
	MOD_ARCHERY				= 104,		// Archery
	MOD_MARKSMAN			= 105,		// Marksman
	MOD_THROW				= 106,		// Throw
	MOD_GUARD				= 107,		// Guard
	MOD_EVASION				= 108,		// Evasion
	MOD_SHIELD				= 109,		// Shield
	MOD_PARRY				= 110,		// Parry

/************************************************************************
*																		*
*		Magic Skills													*
*																		*
************************************************************************/

	MOD_DIVINE				= 111,
	MOD_HEALING				= 112,
	MOD_ENHANCE				= 113,
	MOD_ENFEEBLE			= 114,
	MOD_ELEM				= 115,
	MOD_DARK				= 116,
	MOD_SUMMONING			= 117,
	MOD_NINJUTSU			= 118,
	MOD_SINGING				= 119,
	MOD_STRING				= 120,
	MOD_WIND				= 121,
	MOD_BLUE				= 122,
	MOD_GEOMANCY			= 580,

/************************************************************************
*																		*
*	Crafting Skills														*
*																		*
************************************************************************/

	MOD_FISH				= 127,
	MOD_WOOD				= 128,
	MOD_SMITH				= 129,
	MOD_GOLDSMITH			= 130,
	MOD_CLOTH				= 131,
	MOD_LEATHER				= 132,
	MOD_BONE				= 133,
	MOD_ALCHEMY				= 134,
	MOD_COOK				= 135,
	MOD_SYNERGY				= 136,

	MOD_SYNTH_SKILL_UP		= 626,		// Extra skillup % chance
	MOD_SYNTH_ITEM_LOSS		= 627,		// Extra % chance to not lose crafting items
	MOD_SYNTH_HQ_RATE		= 628,		// Extra % chance HQ craft an items

/************************************************************************
*																		*
*	Activity Skills														*
*																		*
************************************************************************/

	MOD_CHOCOBO_TIME		= 415,		// Chocobo Riding Time Increase in Mins
	MOD_GOLDFISHING			= 465,		// Bonus to Goldfishing

/************************************************************************
*																		*
*	Chance you will not make an HQ synth								*
*			Impossibility of HQ synth									*
*																		*
************************************************************************/

	MOD_ANTIHQ_WOOD			= 144,
	MOD_ANTIHQ_SMITH		= 145,
	MOD_ANTIHQ_GOLDSMITH	= 146,
	MOD_ANTIHQ_CLOTH		= 147,
	MOD_ANTIHQ_LEATHER		= 148,
	MOD_ANTIHQ_BONE			= 149,
	MOD_ANTIHQ_ALCHEMY		= 150,
	MOD_ANTIHQ_COOK			= 151,

/************************************************************************
*																		*
*	Damage / Crit Damage / Delay										*
*																		*
************************************************************************/

	MOD_DMG					= 160,				// Damage Multiplier
	MOD_DMGPHYS				= 161,				// Physical Damage Multiplier
	MOD_DMGBREATH			= 162,				// Breath Damage Multiplier
	MOD_DMGMAGIC			= 163,				// Magic Damage Multiplier - 256 base! (value of -24 means -24/256 magic damage taken)
	MOD_DMGRANGE			= 164,				// Range Damage Multiplier

	MOD_UDMGPHYS			= 387,				// Uncapped Damage Multipliers
	MOD_UDMGBREATH			= 388,				// Used in sentinal, invincible, physical shield etc
	MOD_UDMGMAGIC			= 389,
	MOD_UDMGRANGE			= 390,

	MOD_CRITHITRATE			= 165,				// Raises chance to crit
	MOD_ENEMYCRITRATE		= 166,				// Raises chance enemy will crit
	MOD_CRIT_DMG_INCREASE	= 459,				// Raises Crit Hit Dmg % divided by 100
	MOD_MAGIC_CRIT_RATE		= 559,				// Raises chance to Magic Crit
	MOD_MAG_CRIT_DMG		= 560,				// Raises Magic Crit Dmg (MAB)

	MOD_SHIELD_DEF			= 460,				// Shield Dmg Reduction % Bonus

	MOD_HASTE_MAGIC			= 167,				// Haste (and Slow) from magic - 1024 base! (448 cap)
	MOD_HASTE_ABILITY		= 383,				// Haste (and Slow) from abilities - 1024 base! (256 cap?)
	MOD_HASTE_GEAR			= 384,				// Haste (and Slow) from equipment - 1024 base! (256 cap)
	MOD_SPELLINTERRUPT		= 168,				// % Spell Interruption Rate
	MOD_MOVE				= 169,				// % Movement Speed
	MOD_FASTCAST			= 170,				// Increases Spell Cast Time (TRAIT)
	MOD_UFASTCAST			= 577,				// Increases Uncapped Spell Cast Time
	MOD_DELAY				= 171,				// Increase/Decrease Delay
	MOD_RANGED_DELAY		= 172,				// Increase/Decrease Ranged Delay
	MOD_MARTIAL_ARTS		= 173,				// The integer amount of delay to reduce from H2H weapons' base delay. (TRAIT)
    MOD_SKILLCHAINBONUS     = 174,				// Damage bonus applied to skill chain damage.  Modifier from effects/traits
    MOD_SKILLCHAINDMG       = 175,				// Damage bonus applied to skill chain damage.  Modifier from gear (multiplicative after effect/traits)

/************************************************************************
*																		*
*	Food!																*
*																		*
************************************************************************/

	MOD_FOOD_HPP			= 176,
	MOD_FOOD_HP_CAP			= 177,
	MOD_FOOD_MPP			= 178,
	MOD_FOOD_MP_CAP			= 179,
	MOD_FOOD_ATTP			= 180,
	MOD_FOOD_ATT_CAP		= 181,
	MOD_FOOD_DEFP			= 182,
	MOD_FOOD_DEF_CAP		= 183,
	MOD_FOOD_ACCP			= 184,
	MOD_FOOD_ACC_CAP		= 185,
	MOD_FOOD_RATTP			= 186,
	MOD_FOOD_RATT_CAP		= 187,
	MOD_FOOD_RACCP			= 188,
	MOD_FOOD_RACC_CAP		= 189,

/************************************************************************
*																		*
*	Killer-Effects - (Most by Traits/JobAbility)						*
*																		*
************************************************************************/

	MOD_VERMIN_KILLER		= 224,
	MOD_BIRD_KILLER			= 225,
	MOD_AMORPH_KILLER		= 226,
	MOD_LIZARD_KILLER		= 227,
	MOD_AQUAN_KILLER		= 228,
	MOD_PLANTOID_KILLER		= 229,
	MOD_BEAST_KILLER		= 230,
	MOD_UNDEAD_KILLER		= 231,
	MOD_ARCANA_KILLER		= 232,
	MOD_DRAGON_KILLER		= 233,
	MOD_DEMON_KILLER		= 234,
	MOD_EMPTY_KILLER		= 235,
	MOD_HUMANOID_KILLER		= 236,
    MOD_LUMORIAN_KILLER     = 237,
    MOD_LUMINION_KILLER     = 238,

/************************************************************************
*																		*
*	Resistances and Damage increase to Ecosystems						*
*			Job Ability Circles											*
*																		*
************************************************************************/

	MOD_DRAGON_DMG			= 569,
	MOD_ARCANE_DMG			= 570,
	MOD_UNDEAD_DMG			= 571,
	MOD_DEMON_DMG			= 572,

/************************************************************************
*																		*
*	Resistances to enfeebles - Traits/Job Ability						*
*																		*
************************************************************************/

	MOD_SLEEPRES			= 240,
	MOD_POISONRES			= 241,
	MOD_PARALYZERES			= 242,
	MOD_BLINDRES			= 243,
	MOD_SILENCERES			= 244,
	MOD_VIRUSRES			= 245,
	MOD_PETRIFYRES			= 246,
	MOD_BINDRES				= 247,
	MOD_CURSERES			= 248,
	MOD_GRAVITYRES			= 249,
	MOD_SLOWRES				= 250,
	MOD_STUNRES				= 251,
	MOD_CHARMRES			= 252,
	MOD_DEATHRES			= 253,
	MOD_TERRORRES			= 254,
	MOD_AMNESIARES			= 255,
	MOD_DOOMRES				= 412,
	MOD_CONFUSIONRES		= 137,

/************************************************************************
*																		*
*																		*
*																		*
************************************************************************/

	MOD_PARALYZE			= 257,				// Paralyze -- percent chance to proc
	MOD_MIJIN_GAKURE		= 258,				// Tracks whether or not you used this ability to die.
	MOD_DUAL_WIELD			= 259,				// Percent reduction in dual wield delay.
	MOD_TACTICAL_GUARD		= 462,				// Extra TP Gained from a Guard
	MOD_TACTICAL_PARRY		= 463,				// Extra TP Gained from a Parry
	MOD_OCCULT_ACCUMEN		= 464,				// TP Gained from Elemental or Dark Magic Casts divided by 1000

/************************************************************************
*																		*
*	Warrior																*
*																		*
************************************************************************/

	MOD_DOUBLE_ATTACK		= 288,				// Percent chance to proc
	MOD_WARCRY_DURATION		= 639,				// Warcy duration bonus from gear
	MOD_RETALIATION_DMG		= 475,				// % increase to Retaliation Dmg
	MOD_BLOOD_RAGE			= 552,				// Blood Rage duration

/************************************************************************
*																		*
*	Monk																*
*																		*
************************************************************************/

	MOD_SUBTLE_BLOW			= 289,				// How much TP to reduce.
	MOD_COUNTER				= 291,				// Percent chance to counter
	MOD_KICK_ATTACK			= 292,				// Percent chance to kick
	MOD_KICK_DMG			= 386,	 			// increases kick attack damage
	MOD_COUNTER_BASE_DMG	= 473,				// Increase Weapon Base DMG on Counters
	MOD_PFT_COUNTER_DMG		= 538,				// Increase Weapon Base DMG on Perfect Counter

/************************************************************************
*																		*
*	White Mage															*
*																		*
************************************************************************/

	MOD_AFFLATUS_SOLACE				= 293,		// Pool of HP accumulated during Afflatus Solace
	MOD_AFFLATUS_MISERY				= 294,		// Pool of HP accumulated during Afflatus Misery
	MOD_DIVINE_BENISON_FASTCAST		= 564,		// Divine Benison Fastcast Mod
	MOD_DIVINE_BENISON_ENMITY		= 565,		// Divine Benison Enmity Mod
	MOD_DIVINE_VEIL					= 632,		// Divine Veil chance without Divine Seal
	MOD_CURE_TO_MP					= 635,		// Converts % of Cure to MP

/************************************************************************
*																		*
*	Black Mage															*
*																		*
************************************************************************/

	MOD_CLEAR_MIND			= 295,				// Used in conjunction with MOD_HEALMP to increase amount between tics
	MOD_CONSERVE_MP			= 296,				// Percent chance
	MOD_MANA_WALL_DMG		= 539,				// Percent additional Dmg Reduction to Mana Wall

/************************************************************************
*																		*
*	Red Mage															*
*																		*
************************************************************************/

	MOD_BLINK				= 299,				// Tracks blink shadows
	MOD_STONESKIN			= 300,				// Tracks stoneskin HP pool
	MOD_PHALANX				= 301,				// Tracks direct damage reduction
	MOD_SABOTEUR			= 544,				// Increase Potency and Duration durring Saboteur
	MOD_AQUAVEIL_PWR		= 612,				// Increase Aquaveil Power

/************************************************************************
*																		*
*	Thief																*
*																		*
************************************************************************/

    MOD_STEAL               = 298,				// Increase/Decrease THF Steal chance
	MOD_TRIPLE_ATTACK		= 302,				// Percent chance
	MOD_TREASURE_HUNTER     = 303,				// Percent chance
	MOD_SNEAK_ATK_MOD		= 458,				// Modifier for DEX Divided by 10 (1 = .01)
	MOD_TRICK_ATK_MOD		= 470,				// Modifier for AGI %
	MOD_SATA_DMG			= 471,				// SA TA Dmg Modifier %
	MOD_GILFINDER			= 468,				// Gil dropped from mob +50% per mod
	MOD_DESPOIL				= 476,				// Increase/Decrease THF Despoil chance
	MOD_HIDE				= 633,				// Increase Hide duration (seconds)
	MOD_MUG_MULTIPLIER		= 637,				// Mug gil multiplier

/************************************************************************
*																		*
*	Paladin																*
*																		*
************************************************************************/

	MOD_SHIELD_BASH					= 385,		// Shield Bash Dmg Increase
	MOD_DIVINE_EMBLEM				= 540,		// Divine Emblem enmity modifier
	MOD_BLOCK_RATE					= 541,		// Increase chance to block with shield (Reprisal/Palisade)
	MOD_ENMITY_REDUCTION_PHYSICAL 	= 601,		// Reduces Enmity decrease when taking physical damage
	MOD_RAMPART_DURATION			= 634,		// Increase Rampart duration (seconds)
	MOD_SHIELD_MASTERY_TP			= 461,		// Shield mastery TP bonus when blocking with a shield (modId = 485)

/************************************************************************
*																		*
*	Dark Knight															*
*																		*
************************************************************************/

	MOD_WEAPON_BASH			= 433,				// Increase Weapon Bash Damage
	MOD_STALWART_SOUL		= 469,				// HP reduction from Souleater divided by 1000
	MOD_DARK_MAG_CAST		= 474,				// % Reduction to Dark Magic Cast Time
	MOD_ABSORB_POTENCY		= 542,				// % increase to Absorb potency
	MOD_ABSORB_DURATION		= 543,				// Increase to Absorb duration seconds
	MOD_SCARLET_DMG			= 553,				// Percent Bonus to Dmg during Scarlet Delirium
	MOD_SCARLET_MDMG		= 554,				// Percent Bonus to Magic Dmg during Scarlet Delirium

/************************************************************************
*																		*
*	Beastmaster															*
*																		*
************************************************************************/

	MOD_TAME				= 304,				// Additional percent chance to charm
	MOD_CHARM_TIME			= 360,				// Extends the charm time only, no effect of charm chance
	MOD_REWARD_HP_BONUS		= 364,				// Percent to add to reward HP healed. (364)
	MOD_CHARM_CHANCE		= 411,				// Extra chance to charm (light&apollo staff etc)
	MOD_REWARD_RECAST		= 396,				// Time reduction to Reward recast

/************************************************************************
*																		*
*	Bard																*
*																		*
************************************************************************/

	MOD_SONG_RECAST			= 391,				// Percent decrease to Bard Song Recast
	MOD_SONG_CAST			= 399,				// Percent decrease to Song Cast Time
	MOD_SONG_DURATION		= 435,				// Song Duration % Modifier
	MOD_SONG_EFFECTS		= 456,				// Additional # of Song Effects (Daurdabla)
	MOD_ALL_SONGS			= 436,				// Increase to all songs
	MOD_ETUDE				= 437,				// Song Modifiers
	MOD_REQUIEM				= 438,
	MOD_VIRELAI				= 439,
	MOD_MARCH				= 440,
	MOD_HYMNUS				= 441,
	MOD_MADRIGAL			= 442,
	MOD_MINUET				= 443,
	MOD_CAROL				= 444,
	MOD_PAEON				= 445,
	MOD_MAMBO				= 446,
	MOD_LULLABY				= 447,
	MOD_BALLAD				= 448,
	MOD_MAZURKA				= 449,
	MOD_THRENODY			= 450,
	MOD_FINALE				= 451,
	MOD_MINNE				= 452,
	MOD_SCHERZO				= 453,
	MOD_ELEGY				= 454,
	MOD_PRELUDE				= 455,

	MOD_FIRE_NULL			= 603,				// Annul modifiers for Carol II songs
	MOD_EARTH_NULL			= 604,
	MOD_WATER_NULL			= 605,
	MOD_WIND_NULL			= 606,
	MOD_ICE_NULL			= 607,
	MOD_LTNG_NULL			= 608,
	MOD_LIGHT_NULL			= 609,
	MOD_DARK_NULL			= 610,

	MOD_FIRE_ABSORB			= 616,				// Elemental Absorb Chance
	MOD_EARTH_ABSORB		= 617,
	MOD_WATER_ABSORB		= 618,
	MOD_WIND_ABSORB			= 619,
	MOD_ICE_ABSORB			= 620,
	MOD_LTNG_ABSORB			= 621,
	MOD_LIGHT_ABSORB		= 622,
	MOD_DARK_ABSORB			= 623,

	MOD_MAGIC_ABSORB		= 624,
	MOD_MAGIC_NULL			= 625,
	MOD_SCHERZO_DMG			= 611,				// Sentinel's Scherzo's damage multiplier

/************************************************************************
*																		*
*	Ranger																*
*																		*
************************************************************************/

	MOD_RECYCLE					= 305,				// Percent chance to recycle
	MOD_SNAP_SHOT				= 365,				// Percent reduction to range attack delay
	MOD_RAPID_SHOT				= 359,				// Percent chance to proc rapid shot
	MOD_WIDESCAN				= 340,				// Range to see enemies on the Map
	MOD_SHADOWBIND				= 433,				// Enables Shadowbind on next RA
	MOD_BARRAGE					= 467,				// Barrage Shot Count Increase
	MOD_DEAD_AIM				= 567,				// Critical Ranged Hit Dmg % Increase
	MOD_BARRAGE_ACC				= 596,				// Barrage accuracy
	MOD_DOUBLE_SHOT_RATE		= 597,				// The rate that double shot can proc. Without this, the default is 40%.
	MOD_DOUBLE_SHOT_DMG			= 602,				// Double Shot DMG % Increase
	MOD_VELOCITY_SNAPSHOT_BONUS	= 598,				// Increases Snapshot whilst Velocity Shot is up.
	MOD_VELOCITY_RATT_BONUS		= 599,				// Increases Ranged Attack whilst Velocity Shot is up.
	MOD_SHADOW_BIND_EXT			= 641,				// Extends the time of shadowbind

/************************************************************************
*																		*
*	Samurai																*
*																		*
************************************************************************/

	MOD_ZANSHIN				= 306,				// Zanshin percent chance
	MOD_MEDITATE_DUR		= 432,	 			// Extends Meditate duration in secs

/************************************************************************
*																		*
*	Ninja																*
*																		*
************************************************************************/

	MOD_UTSUSEMI			= 307,				// Everyone's favorite --tracks shadows.
	MOD_NINJA_TOOL			= 308,				// Percent chance to not use a tool.
	MOD_STEALTH             = 358,				// Chance to remain undetected ??
	MOD_UTSUSEMI_CAST		= 405,				// Percent decrease to Utsusemi Cast Time
	MOD_NINJUTSU_RECAST		= 394,				// Time in seconds decrease to Ninjutsu Spell Recast
	MOD_PARRY_RATE			= 557,				// Percent increase to parry rate.

/************************************************************************
*																		*
*	Dragoon																*
*																		*
************************************************************************/

	MOD_JUMP_TP_BONUS				= 361,		// Bonus tp player receives when using jump (must be divided by 10)
	MOD_JUMP_ATT_BONUS				= 362,		// ATT% bonus for jump + high jump
	MOD_HIGH_JUMP_ENMITY_REDUCTION	= 363,		// For gear that reduces more enmity from high jump
	MOD_SPIRIT_JUMP_TP				= 532,		// Bonus tp player receives when using spirit jump (must be divided by 10)
	MOD_SPIRIT_JUMP_ATT				= 533,		// ATT% bonus for spirit jump
	MOD_CONSERVE_TP					= 566,		// Proc rate for Conserve TP
	MOD_SPIRIT_LINK_HP				= 630,		// Spirit Link Recovery +x HP
	MOD_SPIRIT_LINK_HPP				= 631,		// Spirit Link Recovery HP * x%

/************************************************************************
*																		*
*	Summoner															*
*																		*
************************************************************************/

	MOD_AVATAR_PERPETUATION		= 371,			// Stores base cost of current avatar
	MOD_WEATHER_REDUCTION		= 372,			// Stores perpetuation reduction depending on weather
	MOD_DAY_REDUCTION			= 373,			// Stores perpetuation reduction depending on day
	MOD_PERPETUATION_REDUCTION	= 346,			// Stores the MP/tick reduction from gear
	MOD_BP_DELAY			 	= 357,			// Stores blood pact delay reduction
	MOD_SUMMON_CAST				= 402,			// Time in seconds decrease to Summoning Magic Cast Time
	MOD_ELE_AVATAR_RECAST		= 395,			// Percent decrease to Elemental Avatar Recast
	MOD_BLOOD_BOON				= 568,			// Proc chance for Blood Boon

/************************************************************************
*																		*
*	Blue Mage															*
*																		*
************************************************************************/

	MOD_BLUE_POINTS			= 309,				// Tracks extra blue points
	MOD_BLUE_RECAST			= 393,				// Percent decrease to Blue Spell Recast
	MOD_BLUE_CAST			= 406,				// Percent decrease to Blue Magic Cast Time
	MOD_BLUE_LEARN			= 561,				// Percent chance increase to learn Blue Magics

/************************************************************************
*																		*
*	Corsair																*
*																		*
************************************************************************/

	MOD_DMG_REFLECT			= 316,				// Tracks totals
	MOD_ROLL_ROGUES			= 317,				// Tracks totals
	MOD_ROLL_GALLANTS		= 318,				// Tracks totals
	MOD_ROLL_CHAOS			= 319,				// Tracks totals
	MOD_ROLL_BEAST			= 320,				// Tracks totals
	MOD_ROLL_CHORAL			= 321,				// Tracks totals
	MOD_ROLL_HUNTERS		= 322,				// Tracks totals
	MOD_ROLL_SAMURAI		= 323,				// Tracks totals
	MOD_ROLL_NINJA			= 324,				// Tracks totals
	MOD_ROLL_DRACHEN		= 325,				// Tracks totals
	MOD_ROLL_EVOKERS		= 326,				// Tracks totals
	MOD_ROLL_MAGUS			= 327,				// Tracks totals
	MOD_ROLL_CORSAIRS		= 328,				// Tracks totals
	MOD_ROLL_PUPPET			= 329,				// Tracks totals
	MOD_ROLL_DANCERS		= 330,				// Tracks totals
	MOD_ROLL_SCHOLARS		= 331,				// Tracks totals
	MOD_BUST				= 332,				// # of busts
	MOD_QUICK_DRAW_DMG      = 578,				// Additional damage to Quick Draw

/************************************************************************
*																		*
*	Puppetmaster														*
*																		*
************************************************************************/

    MOD_AUTO_MELEE_SKILL	= 581,
    MOD_AUTO_RANGED_SKILL	= 582,
    MOD_AUTO_MAGIC_SKILL	= 583,

/************************************************************************
*																		*
*	Dancer																*
*																		*
************************************************************************/

	MOD_FINISHING_MOVES		= 333,				// Tracks # of finishing moves
	MOD_JIG_TIME			= 422,				// Additional duration of Jigs in secs
	MOD_WALTZ_POTENCY		= 477,				// Additional potency of Waltzs
	MOD_WALTZ_RCVD			= 487,				// Additional potency of Waltzs Recieved
	MOD_STEP_ACCURACY		= 573,				// Bonus accuracy for Dancer's steps

/************************************************************************
*																		*
*	Scholar																*
*																		*
************************************************************************/

	MOD_SUBLIMATION_BONUS	= 334,				// MP drain Mod for Sublimation charge.
	MOD_LIGHT_ARTS_EFFECT	= 434,
	MOD_DARK_ARTS_EFFECT	= 335,
	MOD_LIGHT_ARTS_SKILL	= 336,
	MOD_DARK_ARTS_SKILL		= 337,
	MOD_REGEN_EFFECT		= 338,
	MOD_REGEN_DURATION		= 339,
	MOD_HELIX_EFFECT		= 555,
	MOD_HELIX_DURATION		= 556,
	MOD_STORMSURGE_EFFECT   = 558,
	MOD_BLACK_MAGIC_COST    = 545,	            // MP cost for black magic (light/dark arts)
    MOD_WHITE_MAGIC_COST    = 546,	            // MP cost for white magic (light/dark arts)
    MOD_BLACK_MAGIC_CAST    = 547,	            // Cast time for black magic (light/dark arts)
    MOD_WHITE_MAGIC_CAST    = 548,	            // Cast time for black magic (light/dark arts)
    MOD_BLACK_MAGIC_RECAST  = 549,	            // Recast time for black magic (light/dark arts)
    MOD_WHITE_MAGIC_RECAST  = 550,	            // Recast time for white magic (light/dark arts)
    MOD_ALACRITY_CELERITY_EFFECT = 551,			// Bonus for celerity/alacrity effect
    MOD_GRIMOIRE_SPELLCASTING    = 647,         // (modID = 647) "Grimoire: Reduces spellcasting time" bonus
	MOD_GRIMOIRE_RECAST		= 410,				// Percent decrease to Spell Recasts under Light Arts and Dark Arts
	MOD_GRIMOIRE_CAST		= 400,				// Percent decrease to Grimoire Cast Time

/************************************************************************
*																		*
*	Pets / Avatars / Automatons / Wyverns								*
*																		*
************************************************************************/

	MOD_PET_MABB			= 312,				// Tracks totals
	MOD_PET_MACC			= 313,				// Stat Modifiers for Pets
	MOD_PET_MDEF			= 488,

	MOD_PET_ATTP			= 312,
	MOD_PET_DEFP			= 489,
	MOD_PET_ACC				= 315,
	MOD_PET_RACC			= 490,
	MOD_PET_ATT				= 491,
	MOD_PET_RATT			= 492,
	MOD_PET_DEF				= 493,
	MOD_PET_EVA				= 494,

	MOD_PET_HP				= 495,
	MOD_PET_HPP				= 496,
	MOD_PET_MP				= 497,

	MOD_PET_HPHEAL			= 498,
	MOD_PET_MPHEAL			= 499,

	MOD_PET_ENMITY			= 500,
	MOD_PET_BLD_PACT_DMG	= 501,
	MOD_PET_REGAIN			= 502,
	MOD_PET_REGEN			= 503,
	MOD_PET_REFRESH			= 504,
	MOD_PET_HASTE			= 505,
	MOD_PET_STORETP			= 506,
	MOD_PET_TPBONUS			= 507,

	MOD_PET_DMG				= 508,
	MOD_PET_PPHYS_DMG		= 509,
	MOD_PET_MAG_DMG			= 510,
	MOD_PET_CRITRATE		= 511,

	MOD_PET_BREATH_ACC		= 512,
	MOD_WYVERN_BREATH		= 513,
	MOD_PET_BREATH_DMG		= 514,
	MOD_WYVERN_SUBJOB		= 534,

	MOD_PET_BMAG_SKL		= 515,
	MOD_PET_COMBAT_SKL		= 516,
	MOD_PET_MELEE_SKL		= 517,
	MOD_PET_WMAG_SKL		= 518,

	MOD_PET_DBL_ATK			= 519,
	MOD_PET_COUNTER			= 520,
	MOD_PET_SPELL_TIME		= 521,
	MOD_PET_CONSERVE_MP		= 522,
	MOD_PET_MP_COST			= 523,
	MOD_PET_CURE_PTNCY		= 524,
	MOD_PET_SUBTLE_BLOW		= 535,
	MOD_PET_FASTCAST		= 536,
	MOD_PET_SPELL_INT		= 537,

	MOD_PET_STR				= 525,
	MOD_PET_DEX				= 526,
	MOD_PET_VIT				= 527,
	MOD_PET_AGI				= 528,
	MOD_PET_INT				= 529,
	MOD_PET_MND				= 530,
	MOD_PET_CHR				= 531,

/************************************************************************
*																		*
*	Fellows																*
*																		*
************************************************************************/

	MOD_FELLOW_HP			= 479,				// Tracks Totals
	MOD_FELLOW_MP			= 480,				// Stat Modifiers for Fellows
	MOD_FELLOW_STR			= 481,
	MOD_FELLOW_DEX			= 482,
	MOD_FELLOW_VIT			= 483,
	MOD_FELLOW_AGI			= 484,
	MOD_FELLOW_INT			= 485,
	MOD_FELLOW_MND			= 486,
	MOD_FELLOW_CHR			= 487,

/************************************************************************
*																		*
*	Spikes and Enspell													*
*																		*
************************************************************************/

	MOD_ENSPELL				= 341,				// Stores the type of enspell active (0 if nothing)
	MOD_ENSPELL_DMG			= 343,				// Stores the base damage of the enspell before reductions
	MOD_ENSPELL_DMG_BONUS	= 642,				//
	MOD_ENSPELL_DURATION	= 413,				// Stores the additional duration time for enspell
	MOD_SPIKES				= 342,				// Store the type of spike spell active (0 if nothing)
	MOD_SPIKES_DMG			= 344,				// Stores the base damage of the spikes before reductions

	MOD_ADDITIONAL_EFFECT	= 638,				// Allow scripted "Additional Effect" to process

	MOD_ITEM_SPIKES_TYPE	= 644,				// Type spikes an item has
	MOD_ITEM_SPIKES_DMG		= 645,				// Damage of an items spikes
	MOD_ITEM_SPIKES_CHANCE	= 646,				// Chance of an items spike

	MOD_TP_BONUS			= 345,

/************************************************************************
*																		*
*	Experience															*
*																		*
************************************************************************/

	// MOD_DEDICATION			= 310,				// % exp gain
	// MOD_DEDICATION_CAP		= 311,				// Cap of dedication effect
	MOD_EXP_BONUS			= 382,

/************************************************************************
*																		*
*	Stores the amount of elemental affinity (elemental staves mostly)	*
*																		*
************************************************************************/

	MOD_FIRE_AFFINITY		= 347,
	MOD_EARTH_AFFINITY		= 348,
	MOD_WATER_AFFINITY		= 349,
	MOD_ICE_AFFINITY		= 350,
	MOD_THUNDER_AFFINITY	= 351,
	MOD_WIND_AFFINITY		= 352,
	MOD_LIGHT_AFFINITY		= 353,
	MOD_DARK_AFFINITY		= 354,

/************************************************************************
*																		*
*		Special Modifiers+												*
*																		*
************************************************************************/

	MOD_CRYSTAL_DROP		 = 472,				// Increase drop rate of Crystals (20 Max)

	MOD_MAIN_DMG_RATING		 = 366,				// Adds damage rating to main hand weapon (maneater/blau dolch etc hidden effects)
	MOD_SUB_DMG_RATING		 = 367,				// Adds damage rating to off hand weapon
	MOD_REGAIN				 = 368,				// Auto regain TP (from items) | this is multiplied by 10 e.g. 20 is 2% TP
	MOD_REGAIN_DOWN			 = 574,				// Plague, reduce tp
	MOD_REFRESH				 = 369,				// Auto refresh from equipment
	MOD_REFRESH_DOWN		 = 575,				// Plague, reduce mp
	MOD_REGEN				 = 370,				// Auto regen from equipment
	MOD_REGEN_DOWN      	 = 576,				// Poison
	MOD_CURE_POTENCY		 = 374,				// % cure potency | bonus from gear is capped at 50
	MOD_CURE_POTENCY_RCVD	 = 375,				// % potency of received cure | healer's roll, some items have this
	MOD_RANGED_DMG_RATING	 = 376,				// Adds damage rating to ranged weapon
	MOD_MAIN_DMG_RANK		 = 377,				// Adds weapon rank to main weapon (http://wiki.bluegartr.com/bg/Weapon_Rank)
	MOD_SUB_DMG_RANK		 = 378,				// Adds weapon rank to sub weapon
	MOD_RANGED_DMG_RANK		 = 379,				// Adds weapon rank to ranged weapon
	MOD_DELAYP				 = 380,				// Delay addition percent (does not affect tp gain)
	MOD_RANGED_DELAYP		 = 381,				// Ranged delay addition percent (does not affect tp gain)

	MOD_EAT_RAW_FISH		= 416,				// Allows the User to eat Raw Fish like a Mithra
	MOD_EAT_RAW_MEAT		= 417,				// Allows the User to eat Raw Meat like a Galka

	MOD_QUAD_ATTACK			= 421,				// Percent chance
	MOD_INHIBIT_TP			= 636,				// Reduces TP earned by %

/************************************************************************
*																		*
*		Magic Casts / Recasts											*
*																		*
************************************************************************/

	MOD_ELEMENTAL_RECAST	= 392,				// Percent decrease to Elemental Spell Recast

	MOD_BLACK_CAST			= 397,				// Percent decrease to Black Magic Cast Time
	MOD_CURE_CAST			= 398,				// Percent decrease to Cure Magic Cast Time
	MOD_STONESKIN_CAST		= 401,				// Percent decrease to Stoneskin Cast Time
	MOD_ELEMENTAL_CAST		= 403,				// Percent decrease to Elemental Magic Cast Time
	MOD_HEALING_CAST		= 404,				// Percent decrease to Healing Magic Cast Time
	MOD_ENHANCING_CAST		= 407,				// Percent decrease to Enhancing Magic Cast Time
	MOD_ENFEEBLING_CAST		= 408,				// Percent decrease to Enfeebling Magic Cast Time
	MOD_QUICK_MAGIC			= 409,				// Occasionally causes spells to cast instantly and 0 recast

/************************************************************************
*																		*
*		Magic Effects													*
*																		*
************************************************************************/

	MOD_SNEAK_DUR			= 423,				// Percent Increase
	MOD_INVIS_DUR			= 424,				// Percent Increase
	MOD_ENHANCING_DUR		= 425,				// Percent Increase
	MOD_BARSPELL_DUR		= 427,				// Percent Increase duration Elemental Resistance Spells
	MOD_BARSPELL_PWR		= 428,				// Power Increase Elemental Resistance Spells
	MOD_DRAIN_ASPIR			= 429,				// Percent Increase
	MOD_CURSNA_EFFECT		= 430,				// Percent Increase chance to remove DooM
	MOD_CURSNA_RCVD			= 431,				// Percent Increase chance to remove DooM

	MOD_STONESKIN_HP		= 426,				// Stoneskin Amount

/************************************************************************
*																		*
*		Gear Set Modifiers												*
*																		*
************************************************************************/

	MOD_DA_DOUBLE_DAMAGE		 = 584,	// Double attack's double damage chance %.
	MOD_TA_TRIPLE_DAMAGE		 = 585,	// Triple attack's triple damage chance %.
	MOD_ZANSHIN_DOUBLE_DAMAGE	 = 586,	// Zanshin's double damage chance %.
	MOD_RAPID_SHOT_DOUBLE_DAMAGE = 587,	// Rapid shot's double damage chance %.
	MOD_ABSORB_DMG_CHANCE		 = 588,	// Chance to absorb damage %
	MOD_ABSORB_DMG_TO_MP		 = 600,	// Absorbs a percentage of damage taken to MP.
	MOD_EXTRA_DUAL_WIELD_ATTACK  = 589,	// Chance to land an extra attack when dual wielding
	MOD_EXTRA_KICK_ATTACK		 = 590,	// Occasionally allows a second Kick Attack during an attack round without the use of Footwork.
	MOD_SAMBA_DOUBLE_DAMAGE		 = 591,	// Double damage chance when samba is up.
	MOD_NULL_PHYSICAL_DAMAGE	 = 592,	// Chance to null physical damage.
	MOD_QUICK_DRAW_TRIPLE_DAMAGE = 593,	// Chance to do triple damage with quick draw.
	MOD_BAR_ELEMENT_NULL_CHANCE	 = 594,	// Bar Elemental spells will occasionally nullify damage of the same element.
	MOD_GRIMOIRE_INSTANT_CAST	 = 595,	// Spells that match your current Arts will occasionally cast instantly, without recast.
	MOD_ABSORB_DMG_TO_TP		 = 629,	// Absorbs a percentage of damage taken to TP. x / 1000

/************************************************************************
*																		*
*		Reraise															*
*																		*
************************************************************************/

	MOD_RERAISE_I			= 613,		// Auto-Reraise
	MOD_RERAISE_II			= 614,		// Auto-Reraise II
	MOD_RERAISE_III			= 615,		// Auto-Reraise III

/************************************************************************
*																		*
*		GoV Modifiers													*
*																		*
************************************************************************/

    MOD_GOV_CLEARS        = 643, // 4% bonus per Grounds of Valor Page clear (modId = 496)

/************************************************************************
*																		*
*		MAX Modifiers													*
*																		*
************************************************************************/

	// To save time finding the next mod to use..
	// MOD_SPARE				= 649,
	// MOD_SPARE				= 650,
	// MOD_SPARE				= 651,
	// MOD_SPARE				= 652,
	// MOD_SPARE				= 653,
	// MOD_SPARE				= 654,

// Need to refactor entire set...
// Sort out which mods are in DSP trunk and which exist only in ours
// make DSP's match their trunks ID's and place ours 1000+
// while eliminating any two that have identical usage.

};

#define MAX_MODIFIER 1024

/************************************************************************
*																		*
*  Modifier Class														*
*																		*
************************************************************************/

class CModifier
{
public:

	uint16	getModID();						// Return modifier ID
	int16	getModAmount();					// Return modifier value

	void	setModAmount(int16 amount);		// Set modifier value

	 CModifier(uint16 type, int16 amount = 0);
	~CModifier();

private:

	uint16	m_id;							// Modifier ID
	int16	m_amount;						// Modifier value
};

#endif