﻿/*
===========================================================================

  Copyright (c) 2010-2014 Darkstar Dev Teams

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/

  This file is part of DarkStar-server source code.

===========================================================================
*/

#ifndef _LATENTEFFECT_H
#define _LATENTEFFECT_H

#include "../common/cbasetypes.h"
#include "../common/mmo.h"

enum LATENT
{
	LATENT_HP_UNDER_PERCENT			= 0,  // HP less than or equal to % - PARAM: HP PERCENT
	LATENT_HP_OVER_PERCENT			= 1,  // HP more than % - PARAM: HP PERCENT
	LATENT_HP_UNDER_TP_UNDER_100	= 2,  // HP less than or equal to %, tp under 100 - PARAM: HP PERCENT
	LATENT_HP_OVER_TP_UNDER_100		= 3,  // HP more than %, tp over 100 - PARAM: HP PERCENT
	LATENT_MP_UNDER_PERCENT			= 4,  // MP less than or equal to % - PARAM: MP PERCENT
	LATENT_MP_UNDER					= 5,  // MP less than # - PARAM: MP #
	LATENT_TP_UNDER					= 6,  // TP under # and during WS - PARAM: TP VALUE
	LATENT_TP_OVER					= 7,  // TP over # - PARAM: TP VALUE
	LATENT_SUBJOB					= 8,  // Subjob - PARAM: JOBTYPE
	LATENT_PET_ID					= 9,  // Pettype - PARAM: PETID
	LATENT_WEAPON_DRAWN				= 10, // Weapon drawn
	LATENT_WEAPON_SHEATHED			= 11, // Weapon sheathed
	LATENT_MP_OVER_PERCENT			= 12, // MP more than % - PARAM: MP PERCENT
	LATENT_STATUS_EFFECT_ACTIVE		= 13, // Status effect on player - PARAM: EFFECTID
	LATENT_NO_FOOD_ACTIVE			= 14, // No food effects active on player
	LATENT_PARTY_MEMBERS			= 15, // Party has members other than self - PARAM: # OF MEMBERS
	LATENT_PARTY_MEMBERS_IN_ZONE	= 16, // Party size # and members in zone - PARAM: # OF MEMBERS
	LATENT_AVATAR_IN_PARTY			= 21, // Party has a specific avatar - PARAM: same as globals/pets.lua (21 for any avatar)
	LATENT_JOB_IN_PARTY				= 22, // Party has job - PARAM: JOBTYPE
	LATENT_ZONE						= 23, // In zone - PARAM: zoneid
	LATENT_SYNTH_TRAINEE			= 24, // Synth skill under 40 + no support
	LATENT_SONG_ROLL_ACTIVE			= 25, // Any song or roll active
	LATENT_TIME_OF_DAY				= 26, // PARAM: 0: DAYTIME 1: NIGHTTIME 2: DUSK-DAWN
	LATENT_HOUR_OF_DAY				= 27, // PARAM: 1: NEW DAY, 2: DAWN, 3: DAY, 4: DUSK, 5: EVENING, 6: DEAD OF NIGHT
	LATENT_FIRESDAY					= 28, // Vanadiel day is Firesday
	LATENT_EARTHSDAY				= 29, // Vanadiel day is Earthsday
	LATENT_WATERSDAY				= 30, // Vanadiel day is Watersday
	LATENT_WINDSDAY					= 31, // Vanadiel day is Windsday
	LATENT_DARKSDAY					= 32, // Vanadiel day is Darksday
	LATENT_NATION_CITIZEN			= 33, // Citizen of the Nation - PARAM: 0: San d'Oria, 1: Bastok, 2: Windurst
	LATENT_ICEDAY					= 34, // Vanadiel day is Iceday
	LATENT_LIGHTNINGSDAY			= 35, // Vanadiel day is Lightningsday
	LATENT_LIGHTSDAY				= 36, // Vanadiel day is Lightsday
	LATENT_MOON_FIRST_QUARTER		= 37, // Moon is in the 1st quarter
	LATENT_JOB_MULTIPLE_5			= 38, // Job a multiple of 5 (5,10,15 etc)
	LATENT_JOB_MULTIPLE_10			= 39, // Job a multiple of 10 (10,20,30 etc)
	LATENT_JOB_MULTIPLE_13_NIGHT	= 40, // Job a multiple of 13 (13,26,39 etc)
	LATENT_JOB_LEVEL_ODD			= 41, // Job level is Odd
	LATENT_JOB_LEVEL_EVEN			= 42, // Job level is Even
	LATENT_WEAPON_DRAWN_HP_UNDER	= 43, // PARAM: HP PERCENT
	LATENT_MP_OVER					= 44, // MP greater than # - PARAM: MP #
	LATENT_MP_UNDER_VISIBLE_GEAR	= 45, // MP less than or equal to %, calculated using MP bonuses from visible gear only
	LATENT_HP_OVER_VISIBLE_GEAR		= 46, // HP more than or equal to %, calculated using HP bonuses from visible gear only 
	LATENT_WEAPON_BROKEN			= 47, //
	LATENT_IN_DYNAMIS				= 48, // You are in a Dynamis zone
	LATENT_FOOD_ACTIVE				= 49, // Food effect (foodId) active - PARAM: FOOD ITEMID
	LATENT_JOB_LEVEL_BELOW			= 50, // PARAM: level
	LATENT_JOB_LEVEL_ABOVE			= 51, // PARAM: level
	LATENT_WEATHER_ELEMENT			= 52, // PARAM: 0: NONE, 1: FIRE, 2: EARTH, 3: WATER, 4: WIND, 5: ICE, 6: THUNDER, 7: LIGHT, 8: DARK
	LATENT_NATION_CONTROL			= 53, // PARAM: 0: Under own nation's control, 1: Outside own nation's control 2: San d'Oria, 3: Bastok, 4: Windurst
    LATENT_ZONE_HOME_NATION         = 54, // In zone and citizen of nation (aketons)
	//Placeholder					= 55,
	//Placeholder					= 56,
	//Placeholder					= 57,
	//Placeholder					= 58,
	LATENT_MOON_NEW					= 59,	// Moon is New
	LATENT_MOON_WAXING_CRESCENT		= 60,	// Moon is in the Waxing Crescent
	LATENT_MOON_WANING_CRESCENT		= 61,	// Moon is in the Waning Crescent
	LATENT_MOON_SECOND_QUARTER		= 62,	// Moon is in the 2nd quarter
	LATENT_MOON_WAXING_GIBBIOUS		= 63,	// Moon is in the Waxing Gibbious
	LATENT_MOON_WANING_GIBBIOUS		= 64,	// Moon is in the Waning Gibbious
	LATENT_MOON_FULL				= 65,	// Moon is Full
	LATENT_MULTI					= 66	// Multiple conditions (Horror Head, Hooro Head II, Pyracmon Cap) PARAM: 0: Nitetime + Darksday + Full Moon, 1: Daytime + Lightssday + New Moon
};

#define MAX_LATENTEFFECTID		67

/************************************************************************
*																		*
*  Нерешенные задачи:													*
*																		*
*  - сохранение ID сущности, добавившей эффект							*
*  - обновление эффекта (например перезапись protect 1 на protect 2)    *
*																		*
*  Pending problem:                                                     *
*                                                                       *
*  - Save ID essence, add effects                                       *
*  - Update effect (eg overwrite protect 1 to protect 2)                *
*                                                                       *
************************************************************************/

class CBattleEntity;

class CLatentEffect
{
public:

	LATENT		GetConditionsID();            // Return the ID of the latent
	uint16		GetConditionsValue();         // Return the values of the latent
    uint8		GetSlot();                    // Return the latent's slot
	uint16		GetModValue();                // Return the Modifiers ID
    int16		GetModPower();                // Return the Modifiers value
    bool		IsActivated();                // Check if the latent is active
	uint16		GetFlag();                    // Return the flag of the latent

	CBattleEntity* GetOwner();

    void    SetConditionsId(LATENT id);       // Set the latent ID
    void    SetConditionsValue(uint16 value); // Set the latent value
	void	SetSlot(uint8 slot);              // Set the latent slot
    void    SetModValue(uint16 value);        // Set the latent modifier ID
	void	SetModPower(int16 power);         // Set the latent modifier value
    void    Activate();                       // Set the latent to active
	void	Deactivate();                     // Set the latent to deactive
    void    SetOwner(CBattleEntity* Owner);   // Set the character the latent is on

	CLatentEffect
	(
		LATENT conditionsId,
		uint16 conditionsValue,
		uint8 slot, 
		uint16 modValue, 
		int16 modPower
	);

    ~CLatentEffect();

private:

    CBattleEntity* m_POwner;

	LATENT		m_ConditionsID;			// Condition type to be true
	uint16		m_ConditionsValue;		// Condition parameter to be met
	uint8		m_SlotID;				// Slot associated with latent
    uint16      m_ModValue;             // Mod ID to be applied when active
	uint16		m_ModPower;				// Power of mod to be applied when active
	bool		m_Activated;			// Active or not active
};

#endif

