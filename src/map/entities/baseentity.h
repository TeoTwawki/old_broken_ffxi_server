﻿/*
===========================================================================

  Copyright (c) 2010-2014 Darkstar Dev Teams

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/

  This file is part of DarkStar-server source code.

===========================================================================
*/

#ifndef _BASEENTITY_H
#define _BASEENTITY_H

#include "../../common/cbasetypes.h"
#include "../../common/mmo.h"

#include "../ai/ai_general.h"
#include "../instance.h"

/************************************************************************
*                                                                       *
*  Entity Type Enumerators                                              *
*                                                                       *
************************************************************************/

enum ENTITYTYPE
{
	TYPE_PC		= 0x01,
	TYPE_NPC	= 0x02,
	TYPE_MOB	= 0x04,
	TYPE_PET	= 0x08,
    TYPE_SHIP   = 0x10
};

/************************************************************************
*                                                                       *
*  Entity Status Type Enumerators                                       *
*                                                                       *
************************************************************************/

enum STATUSTYPE
{
	STATUS_NORMAL			= 0,
	STATUS_UPDATE			= 1,
	STATUS_DISAPPEAR		= 2,
	STATUS_3				= 3,
	STATUS_4				= 4,
	STATUS_CUTSCENE_ONLY	= 6,
	STATUS_18				= 18,
	STATUS_SHUTDOWN			= 20
};

/************************************************************************
*                                                                       *
*  Entity Animation Type Enumerators                                    *
*                                                                       *
************************************************************************/

enum ANIMATIONTYPE
{
	ANIMATION_NONE					= 0,
	ANIMATION_ATTACK				= 1,
	ANIMATION_2						= 2,
	// 2 =  Also Death ?
	ANIMATION_DEATH					= 3,
	ANIMATION_4						= 4,
	// 4 = ?
    ANIMATION_CHOCOBO               = 5,
	ANIMATION_FISHING   			= 6,
	ANIMATION_7						= 7,
	// 7= Also Healing ?
	ANIMATION_OPEN_DOOR				= 8,
	ANIMATION_CLOSE_DOOR			= 9,
	ANIMATION_ELEVATOR_UP			= 10,
	ANIMATION_ELEVATOR_DOWN			= 11,
	ANIMATION_12					= 12,
	ANIMATION_13					= 13,
	ANIMATION_14					= 14,
	ANIMATION_15					= 15,
	ANIMATION_16					= 16,
	ANIMATION_17					= 17,
	ANIMATION_18					= 18,
	ANIMATION_19					= 19,
	ANIMATION_20					= 20,
	ANIMATION_21					= 21,
	ANIMATION_22					= 22,
	ANIMATION_23					= 23,
	ANIMATION_24					= 24,
	ANIMATION_25					= 25,
	ANIMATION_26					= 26,
	ANIMATION_27					= 27,
	ANIMATION_28					= 28,
	ANIMATION_29					= 29,
	ANIMATION_30					= 30,
	ANIMATION_31					= 31,
	ANIMATION_32					= 32,
	// 12-32 = ?
	ANIMATION_HEALING				= 33,
	ANIMATION_34					= 34,
	ANIMATION_35					= 35,
	ANIMATION_36					= 36,
	ANIMATION_37					= 37,
	// 34-37 = ?
	ANIMATION_FISHING_FISH			= 38,
	ANIMATION_FISHING_CAUGHT		= 39,
	ANIMATION_FISHING_ROD_BREAK		= 40,
	ANIMATION_FISHING_LINE_BREAK	= 41,
	ANIMATION_FISHING_MONSTER		= 42,
	ANIMATION_FISHING_STOP			= 43,
	ANIMATION_SYNTH					= 44,
	ANIMATION_SIT					= 47,
	ANIMATION_RANGED				= 48,
	ANIMATION_FISHING_START			= 50,
	ANIMATION_51					= 51,
	ANIMATION_52					= 52,
	ANIMATION_53					= 53,
	ANIMATION_54					= 54
	// 51+ = Aura or Sphere effects?
};

enum ALLEGIANCETYPE
{
	ALLEGIANCE_MOB			= 0,
	ALLEGIANCE_PLAYER		= 1,
	ALLEGIANCE_SAN_DORIA	= 2,
	ALLEGIANCE_BASTOK		= 3,
	ALLEGIANCE_WINDURST		= 4
};

/************************************************************************
*                                                                       *
*  Entity Structure                                                     *
*                                                                       *
************************************************************************/
// TODO: возможо стоит сделать эту структуру частью класса, взамен нынешних id и targid, но уже без метода clean
// TODO: Should make possible the structure of the class, instead of the current id and targid, but without clean method
struct EntityID_t
{
    void clean()
    {
        id = 0;
        targid = 0;
    }

    uint32 id;
    uint16 targid;
};

/************************************************************************
*                                                                       *
*  Zone Class                                                           *
*                                                                       *
************************************************************************/

class CZone;

struct location_t
{
	position_t	p;				// Position essentially
	uint16		destination;	// Current zone
	CZone*		zone;			// Current zone
	uint16		prevzone;		// Previous zone (for monsters and npc not used)
	bool		zoning;			// Flag is reset every time you access a new area. needed to implement the logic of the game problems ("quests")
	uint16		boundary;		// Specific area in the zone in which the entity (using the characters and vehicles)
};

/************************************************************************
*																		*
*  Базовый класс для всех сущностей в игре								*
*  The base class for all entities in the game                          *
*																		*
************************************************************************/

class CBaseEntity
{
public:

	uint32			id;						// Global identifier that is unique to the server
	uint16			targid;					// Local identifier that is unique in the area
	ENTITYTYPE		objtype;				// Entity Type
	STATUSTYPE		status;					// Status of the entity (separate entities - different statuses)
	uint16			m_TargID;				// TargID object that looks the essence
	string_t		name;					// The entity name
	look_t			look;					// Appearance of all the entities
	look_t			mainlook;				// Only used if mob use changeSkin() or player /lockstyle
	location_t		loc;					// Location entities
	uint8			animation;				// Animation
	uint8			animationsub;			// Additional animation options
	uint8			speed;					// Speed
	uint8			speedsub;				// Optional parameter to the speed of movement
	uint8			namevis;				// Name visibility
	bool			untargetable;
	bool			hpvis;
	uint8			allegiance;				// what types of targets the entity can fight

    string_t        ObjectName;             // Name String for spoofed chat packets
    virtual const int8* GetObjectName();    // Fetch entity name for spoofed chat packets

	virtual const int8* GetName();          // The entity name

	uint16			getZone();				// Current zone
	float			GetXPos();				// Position coordinate X
	float			GetYPos();				// Position coordinate Y
	float			GetZPos();				// Position coordinate Z
	uint8			GetRotPos();
	void			HideName(bool hide);    // Hide / show name
	bool			IsNameHidden();			// Checks if name is hidden

    CAIGeneral*     PBattleAI;              // интеллект любой сущности Intelligence of any nature
	CBattlefield*	PBCNM;					// pointer to bcnm (if in one)
	CInstance*		PInstance;

	CBaseEntity*	GetEntity(uint16 targid, uint8 filter = -1);

    CBaseEntity();							// конструктор Designer
    virtual ~CBaseEntity();					// деструктор Destructor

private:
};

#endif

