﻿/*
===========================================================================

  Copyright (c) 2010-2014 Darkstar Dev Teams

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/

  This file is part of DarkStar-server source code.

===========================================================================
*/

#include <string.h>

#include "entities/baseentity.h"
#include "utils/itemutils.h"
#include "universal_container.h"


/************************************************************************
*																		*
*  Конструктор															*
*  Designer                                                             *
*																		*
************************************************************************/

CUContainer::CUContainer()
{
    m_ContainerType = UCONTAINER_EMPTY;
	Clean();
}

/************************************************************************
*																		*
*  Отчищаем контейнер													*
*  Cleans container                                                     *
*																		*
************************************************************************/

void CUContainer::Clean()
{
    if (m_ContainerType == UCONTAINER_DELIVERYBOX)
    {
        for (uint8 i = 0; i < UCONTAINER_SIZE; ++i)
        {
            delete m_PItem[i];
        }
    }
    m_ContainerType = UCONTAINER_EMPTY;

    m_lock   = 0;
    m_count  = 0;
    m_target = 0;

    m_PItem.clear();
    m_PItem.resize(UCONTAINER_SIZE, NULL);
}

/************************************************************************
*                                                                       *
*  Узнаем цель обмена                                                   *
*  Know the purpose of sharing                                          *
*                                                                       *
************************************************************************/

uint16 CUContainer::GetTarget()
{
    return m_target;
}

/************************************************************************
*                                                                       *
*  Устанавливаем цель обмена                                            *
*  Set the target Exchange                                              *
*                                                                       *
************************************************************************/

void CUContainer::SetTarget(uint16 Target)
{
    m_target = Target;
}

/************************************************************************
*																		*
*  Узнаем текущий тип контейнера										*
*  Know the current type of container                                   *
*																		*
************************************************************************/

UCONTAINERTYPE CUContainer::GetType()
{
	return m_ContainerType;
}

/************************************************************************
*																		*
*  Устанавливаем текущий тип контейнера									*
*  Set the current type of container                                    *
*																		*
************************************************************************/

void CUContainer::SetType(UCONTAINERTYPE Type)
{
	DSP_DEBUG_BREAK_IF(m_ContainerType != UCONTAINER_EMPTY);

	m_ContainerType = Type;
}

/************************************************************************
*                                                                       *
*  Запрещаем изменение содержимого контейнера                           *
*  Do not modify the contents of the container                          *
*                                                                       *
************************************************************************/

void CUContainer::SetLock()
{
    m_lock = true;
}

/************************************************************************
*                                                                       *
*  Проверяем, заблокирован ли контейнер                                 *
*  Check whether the container is locked                                *
*                                                                       *
************************************************************************/

bool CUContainer::IsLocked()
{
	return m_lock;
}

/************************************************************************
*																		*
*  Проверяем, пуст ли контейнер											*
*  Check whether the container is empty                                 *
*																		*
************************************************************************/

bool CUContainer::IsContainerEmpty()
{
	return (m_ContainerType == UCONTAINER_EMPTY);
}

/************************************************************************
*                                                                       *
*  Проверяем, пуста ли ячейка                                           *
*  Check whether the cell is empty                                      *
*                                                                       *
************************************************************************/

bool CUContainer::IsSlotEmpty(uint8 slotID)
{
    if (slotID < m_PItem.size())
	{
        return m_PItem[slotID] == NULL;
    }
    return true;
}

/************************************************************************
*																		*
*  Добавляем предмет в указанныю ячейку контейнера						*
*  Add item to ukazannyyu cell container                                *
*																		*
************************************************************************/

bool CUContainer::SetItem(uint8 slotID, CItem* PItem)
{
	if (slotID < m_PItem.size() && !m_lock)
	{
        if (PItem != NULL && m_PItem[slotID] == NULL) m_count++;
        if (PItem == NULL && m_PItem[slotID] != NULL) m_count--;

		m_PItem[slotID] = PItem;
        return true;
	}
    return false;
}

void CUContainer::SetSize(uint8 size)
{
    m_PItem.resize(size, NULL);
}

/************************************************************************
*                                                                       *
*  Узнаем количество предметов, находящихся в контейнере                *
*  Get the number of objects in the container                           *
*                                                                       *
************************************************************************/

uint8 CUContainer::GetItemsCount()
{
    return m_count;
}

/************************************************************************
*																		*
*  Получаем предмет из указанной ячейки контейнера						*
*  Get an object from a specified cell container                        *
*																		*
************************************************************************/

CItem* CUContainer::GetItem(uint8 slotID)
{
	if (slotID < m_PItem.size())
	{
		return m_PItem[slotID];
	}
	return NULL;
}